﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillsPC
{
  public class LevelTable
  {
    public Dictionary<int, float> LevelAmounts { get; private set; }


    public float this[int i]
    {
      get
      {
        if (LevelAmounts.Keys.Contains(i))
        {
          return LevelAmounts[i];
        }

        int LastLevel = 0;

        List<int> keys = LevelAmounts.Keys.ToList();
        keys.Sort();

        foreach (int lvl in keys)
        {
          if (lvl < i)
            continue;

          if (lvl >= i)
            break;

          LastLevel = lvl;
        }

        return LevelAmounts[LastLevel];
      }
    }


  }
}
