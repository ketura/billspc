﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillsPC
{
  public class ExamplePokemonCollection
  {
    public Dictionary<string, SpeciesDefinition> Examples { get; set; }
    public Dictionary<Stat, SortedDictionary<int, SpeciesDefinition>> Thresholds { get; protected set; }
    private string CurrentPokemon;

    public ExamplePokemonCollection(string currentPokemon, Dictionary<string, SpeciesDefinition> pokemon=null)
    {
      CurrentPokemon = currentPokemon;

      if (pokemon == null)
        pokemon = new Dictionary<string, SpeciesDefinition>();
      
      Examples = new Dictionary<string, SpeciesDefinition>();

      RefreshExamples(pokemon);

      Thresholds = new Dictionary<Stat, SortedDictionary<int, SpeciesDefinition>>();
      RecalculateThresholds();
    }

    public void RefreshExamples(Dictionary<string, SpeciesDefinition> pokemon)
    {
      List<string> Names = new List<string>() { "Arceus", "Magikarp", "Rattata", "Snorlax", "Mewtwo", "Alakazam", "Machamp", "Steelix", "Scyther",
        "Cloyster", "Jolteon", "Electrode", "Starmie", "Gengar", "Ampharos", "Diglett", "Chansey", "Tyranitar", "Dragonite", "Marill", "Zapdos",
        "Articuno", "Moltres", "Espeon", "Umbreon", "Tentacruel", "Aerodactyl", "Pidgeotto", "Charizard", "Blastoise", "Venusaur"};

      foreach(string name in Names)
      {
        SpeciesDefinition def = pokemon.ContainsKey(name) ? pokemon[name] : DefaultValue(name);

        if (def != null)
          Examples.Add(name, def);
      }
    }

    public void RecalculateThresholds()
    {
      Thresholds.Clear();
      foreach (Stat stat in new Stat().GetValues())
      {
        var ordered = new SortedDictionary<int, SpeciesDefinition>(new DescendingComparer<int>());

        foreach (var pokemon in Examples.Values)
        {
          if (pokemon.Name != CurrentPokemon)
            ordered[pokemon.BaseStats[stat]] = pokemon;
        }

        Thresholds[stat] = ordered;
      }
    }


    private SpeciesDefinition DefaultValue(string name)
    {
      switch(name)
      {
        default:
          return null;

        case "Arceus":
          return new SpeciesDefinition()
          {
            Name = "Arceus",
            BaseStats = new StatPage(false)
            {
              { Stat.HP, 999 },
              { Stat.Endurance, 999 },
              { Stat.Attack, 999 },
              { Stat.Defense, 999 },
              { Stat.SpecialAttack, 999 },
              { Stat.SpecialDefense, 999 },
              { Stat.Resistance, 999 },
              { Stat.Weakness, 999 },
              { Stat.Speed, 999 },
              { Stat.Constitution, 999 },
              { Stat.Initiative, 999 },
              { Stat.Accuracy, 999 },
              { Stat.Evasion, 999 },
              { Stat.Critical, 999 },
              { Stat.Height, 999 },
              { Stat.Weight, 999 },
              { Stat.Obedience, 999 },
              { Stat.Attitude, 999 },
              { Stat.Intelligence, 999 },
              { Stat.Gender, 999 },
              { Stat.Temperament, 999 },
            }
          };


      }
    }
  }

  public class DescendingComparer<T> : IComparer<T> where T : IComparable<T>
  {
    public int Compare(T x, T y)
    {
      return y.CompareTo(x);
    }
  }
}
