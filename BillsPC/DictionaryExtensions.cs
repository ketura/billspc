﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillsPC
{
  //http://stackoverflow.com/questions/23565974/how-to-add-multiple-values-to-dictionary-in-c-sharp
  public static class DictionaryExtensions
  {
    public static void Add<TKey, TValue>(this IDictionary<TKey, TValue> target, IDictionary<TKey, TValue> source)
    {
      if (source == null) throw new ArgumentNullException("source");
      if (target == null) throw new ArgumentNullException("target");

      foreach (var keyValuePair in source)
      {
        target.Add(keyValuePair.Key, keyValuePair.Value);
      }
    }

    public static void RemoveRange<T>(this SortedDictionary<int, T> target, int start, int count)
    {
      if (start < 0) throw new ArgumentOutOfRangeException("start", "Start cannot be less than 0.");

      for (int i = start; i < Math.Min(count, target.Count); i++)
      {
        int key = target.ElementAt(i).Key;
        target.Remove(key);
      }
    }
  }
}
