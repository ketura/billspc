﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BillsPC.Controls
{
  public class AbilityTagHandler : TagHandler<SpeciesAbility>
  {
    public override SpeciesAbility Data
    {
      get
      {
        return new SpeciesAbility()
        {
          Name = this.Name,
          Details = txtDetails.Text,
          Chance = txtChance.Value
        };
      }

      set
      {
        this.Name = value.Name;
        txtDetails.Text = value.Details;
        txtChance.Value = value.Chance;
      }
    }

    public override string Name { get; set; }

    private AutosizedTextLabel txtDetails { get; set; }
    private PercentTextLabel txtChance { get; set; }

    public override void Initialize(DockPanel panel)
    {
      txtDetails = new AutosizedTextLabel()
      {
        LabelName = "Details:",
        MinWidth = 300,
        MaxWidth = 700,
        MaxHeight = 50,
        VerticalAlignment = VerticalAlignment.Center,
        VerticalContentAlignment = VerticalAlignment.Center,
        HorizontalAlignment = HorizontalAlignment.Left,
        IsTabStop = true
      };

      txtChance = new PercentTextLabel() { LabelName = "Chance:" };

      panel.Children.Add(txtDetails);
      panel.Children.Add(txtChance);

      Data = new SpeciesAbility() { Name = this.Name, Details = "" };
    }

    public AbilityTagHandler(string name) { Name = name; }
    public AbilityTagHandler(SpeciesAbility ability) { Data = ability; }
  }

  public class AbilityTag : Tag<SpeciesAbility>
  {
    public AbilityTag(string name) : base(new AbilityTagHandler(name)) { }
    public AbilityTag(TagHandler<SpeciesAbility> manager) : base(manager) { }
    public AbilityTag(SpeciesAbility move) : base(new AbilityTagHandler(move)) { }

  }
}
