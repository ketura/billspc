﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{

  public abstract class TagHandler<T>
  {
    public abstract void Initialize(DockPanel panel);
    public abstract T Data { get; set; }
    public abstract string Name { get; set; }
  }

  public abstract class Tag<T> : TagWrapper
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public TagHandler<T> Handler { get; protected set; }

    public T Data
    {
      get { return Handler.Data; }
      set { Handler.Data = value; }
    }

    public Tag(TagHandler<T> manager) : base()
    {
      Handler = manager;
      Init();
    }

    private void Init()
    {
      Handler.Initialize(TagContent);
      LabelName = Handler.Name;
      TagName = Handler.Name;
    }

    public virtual void TagClosed()
    {
      InvokeClick(TagName);
    }

    public virtual void TagDoubleClicked()
    {
      InvokeDoubleClick(TagName);
    }

    protected override void button_Click(object sender, RoutedEventArgs e)
    {
      TagClosed();
    }

    protected override void DockPanel_MouseDown(object sender, MouseButtonEventArgs e)
    {
      if (e.ClickCount >= 2)
      {
        TagDoubleClicked();
      }
    }
  }


  //modify the xaml to utilize this: http://stackoverflow.com/questions/9094486/adding-children-to-usercontrol
  public partial class TagWrapper : UserControl
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public event Action<string> CloseClick;
    public event Action<string> DoubleClick;

    protected void InvokeClick(string s)
    {
      CloseClick?.Invoke(LabelName);
    }

    protected void InvokeDoubleClick(string s)
    {
      DoubleClick?.Invoke(LabelName);
    }

    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(MoveTag), new PropertyMetadata("Label Text"));

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }

    public string TagName { get; protected set; }

    public TagWrapper()
    {
      Initialize();
    }

    protected virtual void Initialize()
    {
      InitializeComponent();
    }

    protected virtual void button_Click(object sender, RoutedEventArgs e) { }

    protected virtual void DockPanel_MouseDown(object sender, MouseButtonEventArgs e) { }
  }
}
