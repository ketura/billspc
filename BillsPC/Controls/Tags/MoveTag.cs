﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace BillsPC.Controls
{
  public class MoveTagHandler : TagHandler<SpeciesMove>
  {
    public override SpeciesMove Data
    {
      get
      {
        return new SpeciesMove()
        {
          Name = this.Name,
          Level = txtLevel.Value,
          Affinity = txtAffinity.Value,
          IV = txtIV.Value
        };
      }

      set
      {
        this.Name = value.Name;
        txtLevel.Value = value.Level;
        txtAffinity.Value = value.Affinity;
        txtIV.Value = value.IV;
      }
    }

    public override string Name { get; set; }

    private IntegerTextLabel txtLevel { get; set; }
    private PercentTextLabel txtAffinity { get; set; }
    private PercentTextLabel txtIV { get; set; }

    public override void Initialize(DockPanel panel)
    {
      txtLevel = new IntegerTextLabel() { LabelName = "Level:" };
      txtAffinity = new PercentTextLabel() { LabelName = "Affinity:" };
      txtAffinity.MaxValue = 999;
      txtIV = new PercentTextLabel() { LabelName = "IV:" };

      panel.Children.Add(txtLevel);
      panel.Children.Add(txtAffinity);
      panel.Children.Add(txtIV);

      Data = new SpeciesMove() { Name = this.Name, Affinity = 1.0f, IV = 0.2f, Level = 0};
    }

    public MoveTagHandler(string name) { Name = name; }
    public MoveTagHandler(SpeciesMove move) { Data = move; }
  }

  public class MoveTag : Tag<SpeciesMove>
  {
    public MoveTag(string name) : base(new MoveTagHandler(name)) { }
    public MoveTag(TagHandler<SpeciesMove> manager) : base(manager) { }
    public MoveTag(SpeciesMove move) : base(new MoveTagHandler(move)) { }

  }
}
