﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BillsPC.Controls
{

  public class EvolutionTagHandler : TagHandler<EvolutionCondition>
  {
    public override EvolutionCondition Data
    {
      get
      {
        return new EvolutionCondition()
        {
          Pokemon = Name,
          Type = (EvolutionMethod)cbType.SelectedItem,
          Amount = txtAmount.Value,
          Variance = txtVariance.Value,
          Details = txtDetails.Text
        };
      }

      set
      {
        Name = value.Pokemon;
        cbType.SelectedItem = value.Type;
        txtAmount.Value = value.Amount;
        txtVariance.Value = value.Variance;
        txtDetails.Text = value.Details;
      }
    }

    public override string Name { get; set; }

    private PickerLabel cbType { get; set; }
    private IntegerTextLabel txtAmount { get; set; }
    private PercentTextLabel txtVariance { get; set; }
    private AutosizedTextLabel txtDetails { get; set; }

    public override void Initialize(DockPanel panel)
    {
      cbType = new PickerLabel()
      {
        PickerWidth = 80,
        LabelName = "Type:",
        VerticalAlignment = VerticalAlignment.Center,
        HorizontalAlignment = HorizontalAlignment.Left,
        VerticalContentAlignment = VerticalAlignment.Center,
        IsTabStop = true
      };

      List<EvolutionMethod> eTypes = new List<EvolutionMethod>(new EvolutionMethod().GetValues());
      eTypes.Remove(EvolutionMethod.None);

      cbType.cbPicker.ItemsSource = eTypes;
      cbType.cbPicker.SelectedIndex = 0;

      txtAmount = new IntegerTextLabel()
      {
        LabelName = "#:",
        VerticalAlignment = VerticalAlignment.Center,
        VerticalContentAlignment = VerticalAlignment.Center,
        HorizontalAlignment = HorizontalAlignment.Left,
        IsTabStop = true
      };

      txtVariance = new PercentTextLabel()
      {
        LabelName = "Variance:",
        VerticalAlignment = VerticalAlignment.Center,
        VerticalContentAlignment = VerticalAlignment.Center,
        HorizontalAlignment = HorizontalAlignment.Left,
        IsTabStop = true
      };

      txtDetails = new AutosizedTextLabel()
      {
        LabelName = "Details:",
        Height = 32,
        Width = 185,
        VerticalAlignment = VerticalAlignment.Center,
        VerticalContentAlignment = VerticalAlignment.Center,
        HorizontalAlignment = HorizontalAlignment.Left,
        IsTabStop = true
      };

      panel.Children.Add(cbType);
      panel.Children.Add(txtAmount);
      panel.Children.Add(txtVariance);
      panel.Children.Add(txtDetails);

      Data = new EvolutionCondition() { Pokemon = Name, Type = EvolutionMethod.Level, Amount = 1, Variance = 0.2f };
    }

    public EvolutionTagHandler(string name) { Name = name; }
    public EvolutionTagHandler(EvolutionCondition cond) { Data = cond; }
  }

  public class EvolutionTag : Tag<EvolutionCondition>
  {
    public EvolutionTag(string name) : base(new EvolutionTagHandler(name)) { }
    public EvolutionTag(TagHandler<EvolutionCondition> manager) : base(manager) { }
    public EvolutionTag(EvolutionCondition cond) : base(new EvolutionTagHandler(cond)) { }

    //delete this
    public EvolutionTag() :base(new EvolutionTagHandler("poop")) { }
  }
}
