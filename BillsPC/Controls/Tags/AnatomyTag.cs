﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BillsPC.Controls
{
  

  public class AnatomyTagHandler : TagHandler<Anatomy>
  {
    public override Anatomy Data
    {
      get
      {
        return new Anatomy()
        {
          Name = this.Name,
          Stats = txtStats.Text,
          Count = txtCount.Value,
          Attached = txtAttached.Text,
          Attributes = txtAttributes.Text
        };
      }

      set
      {
        this.Name = value.Name;
        txtStats.Text = value.Stats;
        txtCount.Value = value.Count;
        txtAttached.Text = value.Attached;
        txtAttributes.Text = value.Attributes;
      }
    }

    public override string Name { get; set; }

    private AutosizedTextLabel txtStats { get; set; }
    private IntegerTextLabel txtCount { get; set; }
    private AutosizedTextLabel txtAttached { get; set; }
    private AutosizedTextLabel txtAttributes { get; set; }

    public override void Initialize(DockPanel panel)
    {
      txtStats = new AutosizedTextLabel()
      {
        LabelName = "Stats:",
        MinWidth = 100,
        MaxWidth = 500,
        MaxHeight = 50
        
      };

      txtCount = new IntegerTextLabel()
      {
        LabelName = "Count:",
      };

      txtAttributes = new AutosizedTextLabel()
      {
        LabelName = "Attributes:",
        MinWidth = 100,
        MaxWidth = 500,
        MaxHeight = 50
      };

      txtAttached = new AutosizedTextLabel()
      {
        LabelName = "Attached:",
        MinWidth = 100,
        MaxWidth = 500,
        MaxHeight = 50
      };

      panel.Children.Add(txtStats);
      panel.Children.Add(txtCount);
      panel.Children.Add(txtAttributes);
      panel.Children.Add(txtAttached);

      Data = new Anatomy() { Name = this.Name, Count = 1, Attributes = "", Attached = "", Stats = ""};
    }

    public AnatomyTagHandler(string name) { Name = name; }
    public AnatomyTagHandler(Anatomy part) { Data = part; }
  }

  public class AnatomyTag : Tag<Anatomy>
  {
    public AnatomyTag(string name) : base(new AnatomyTagHandler(name)) { }
    public AnatomyTag(TagHandler<Anatomy> manager) : base(manager) { }
    public AnatomyTag(Anatomy part) : base(new AnatomyTagHandler(part.Name)) { Handler.Data = part; }

  }
}
