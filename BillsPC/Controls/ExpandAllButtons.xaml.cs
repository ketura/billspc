﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for ExpandAllButtons.xaml
  /// </summary>
  public partial class ExpandAllButtons : UserControl
  {
    public bool Recursive { get; set; }

    public new UIElement Content { get; set; }
    public ExpandAllButtons()
    {
      InitializeComponent();
      Recursive = true;
    }

    private void btnExpandAll_Click(object sender, RoutedEventArgs e)
    {
      if(Content != null)
      {
        var children = Content.GetControls(Recursive);
        foreach(var child in children)
        {
          if(child is CollapsibleHeader)
          {
            ((CollapsibleHeader)child).Expand();
          }
        }
      }
    }

    private void btnCollapseAll_Click(object sender, RoutedEventArgs e)
    {
      if (Content != null)
      {
        var children = Content.GetControls(Recursive);
        foreach (var child in children)
        {
          if (child is CollapsibleHeader)
          {
            ((CollapsibleHeader)child).Collapse();
          }
        }
      }
    }
  }
}
