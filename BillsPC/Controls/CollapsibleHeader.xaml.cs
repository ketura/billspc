﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for CollapsibleHeader.xaml
  /// </summary>
  public partial class CollapsibleHeader : UserControl
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(CollapsibleHeader), new PropertyMetadata("Header Text"));

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }

    public new UIElement Content { get; protected set; }

    public void SetContent(UIElement c)
    {
      Content = c;
      ShowState = true;
    }

    public void ClearContent()
    {
      SetContent(null);
    }

    public bool ShowState
    {
      get { return Content?.Visibility == Visibility.Visible; }
      set
      {
        if (Content == null)
          return;

        if(value)
        {
          btnCollapse.Content = "-";
          Content.Visibility = Visibility.Visible;
        }
        else
        {
          btnCollapse.Content = "+";
          Content.Visibility = Visibility.Collapsed;
        }
      }
    }

    public void Collapse()
    {
      ShowState = false;
    }

    public void Expand()
    {
      ShowState = true;
    }

    public CollapsibleHeader()
    {
      InitializeComponent();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      ShowState = !ShowState;
    }

    private void Header_MouseDown(object sender, MouseButtonEventArgs e)
    {
      if (e.ClickCount >= 2)
      {
        ShowState = !ShowState;
      }
    }
  }
}
