﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for GridSample.xaml
  /// </summary>
  public partial class GridSample : UserControl
  {

    public double Size
    {
      get { return Highlight.Size; }
      set { Highlight.Size = value; }
    }

    public Brush HighlightBrush
    {
      get { return Highlight.Color; }
      set { Highlight.Color = value; }
    }
    public GridSample()
    {
      InitializeComponent();
    }
  }
}
