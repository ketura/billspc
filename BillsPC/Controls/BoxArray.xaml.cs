﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using System.Collections;

namespace BillsPC.Controls
{
  public class EnumControlArray<TKey, TControl> : ControlArray<TKey, TControl>
    where TKey : struct, IConvertible, IComparable
    where TControl : UserControl, new()
  {
    
    protected EnumControlArray()
    { }

    //bool for autoheader
    public EnumControlArray(object value, object maxDigits, object maxValue, bool autopopulate, bool unsigned, List<TKey> ignore=null) : base(value, maxDigits, maxValue, unsigned)
    {
      if (ignore == null)
        ignore = new List<TKey>();

      IgnoreList = ignore;

      if (autopopulate)
      {
        foreach (var t in new TKey().GetValues())
        {
          if (IgnoreList.Contains(t))
            continue;

          AddNewControlAt(t);
        }
      }
    }

    public override void AddNewControlAt(TKey key)
    {
      var newbox = new TControl();

      TrySetProperty(newbox, "LabelName", key.GetDescription());
      TrySetProperty(newbox, "Value", DefaultValue);
      TrySetProperty(newbox, "MaxValue", DefaultMaxValue);
      TrySetProperty(newbox, "MaxDigits", DefaultMaxDigits);
      TrySetProperty(newbox, "Unsigned", DefaultUnsigned);

      Boxes[key] = newbox;
      Fields.Add(newbox);
    }
  }


  public class ControlArray<TKey, TControl> : BoxArray, IDictionary
    where TControl : UserControl, new()
  {
    public List<TKey> IgnoreList { get; protected set; }
    public Dictionary<TKey, TControl> Boxes { get; private set; }
    protected ObservableCollection<TControl> Fields { get; set; }

    protected Type tkeyType;
    protected Type tcontrolType;

    public object DefaultValue { get; set; }
    public object DefaultMaxValue { get; set; }
    public object DefaultMaxDigits { get; set; }
    public object DefaultUnsigned { get; set; }

    public IEnumerable<TKey> Keys
    {
      get { return Boxes.Keys; }
    }

    public IEnumerable<TControl> Values
    {
      get { return Boxes.Values; }
    }

    public TControl this[TKey key]
    {
      get
      {
        if (IgnoreList.Contains(key))
          return null;

        return Boxes[key];
      }
      set
      {
        if (IgnoreList.Contains(key))
          return;

        if (value == null)
        {
          RemoveControlAt(key);
        }
        else
        {
          if(!Boxes.ContainsKey(key))
          {
            Fields.Add(value);
          }

          Boxes[key] = value;
        }
      }
    }

    public TKey GetKey(TControl control)
    {
      return Boxes.FirstOrDefault(x => x.Value == control).Key;
    }
   
    protected ControlArray()
    {  }

    public ControlArray(object value, object maxDigits, object maxValue, bool unsigned) : base()
    {
      tkeyType = typeof(TKey);
      tcontrolType = typeof(TControl);

      DefaultValue = value;
      DefaultMaxValue = maxValue;
      DefaultMaxDigits = maxDigits;
      DefaultUnsigned = unsigned;

      Boxes = new Dictionary<TKey, TControl>();
      Fields = new ObservableCollection<TControl>();

      ItemPanel.ItemsSource = Fields;
    }

    public virtual void AddNewControlAt(TKey key)
    {
      var newbox = new TControl();

      TrySetProperty(newbox, "LabelName", key.ToString());
      TrySetProperty(newbox, "Value", DefaultValue);
      TrySetProperty(newbox, "MaxDigits", DefaultMaxDigits);
      TrySetProperty(newbox, "Unsigned", DefaultUnsigned);

      Boxes[key] = newbox;
      Fields.Add(newbox);
    }

    public void RemoveControlAt(TKey key)
    {
      if (Boxes.ContainsKey(key))
      {
        Fields.Remove(Boxes[key]);
        Boxes.Remove(key);
      }
    }

    protected bool TrySetProperty(TControl control, string name, object value)
    {
      PropertyInfo prop = tcontrolType.GetProperty(name);
      if(prop != null)
      {
        prop.SetValue(control, value);
        return true;
      }

      return false;
    }

    #region Interface Stuff
    ICollection IDictionary.Keys
    {
      get { return (ICollection)Keys; }
    }

    ICollection IDictionary.Values
    {
      get { return (ICollection)Values; }
    }

    public bool IsReadOnly
    {
      get { return false; }
    }

    public bool IsFixedSize
    {
      get { return false; }
    }

    public int Count
    {
      get { return Boxes.Count; }
    }

    public bool Contains(object key)
    {
      if (key is TKey)
        return Boxes.Keys.Contains((TKey)key);

      return false;
    }

    public object SyncRoot
    {
      get { throw new NotImplementedException(); }
    }

    public bool IsSynchronized
    {
      get { throw new NotImplementedException(); }
    }

    public object this[object key]
    {
      get { throw new NotImplementedException(); }
      set { throw new NotImplementedException(); }
    }

    public void Add(object key, object value)
    {
      throw new NotImplementedException();
    }

    public void Clear()
    {
      Boxes.Clear();
      Fields.Clear();
    }

    public IDictionaryEnumerator GetEnumerator()
    {
      return Boxes.GetEnumerator();
    }

    public void Remove(object key)
    {
      if(key is TKey)
        RemoveControlAt((TKey)key);
    }

    public void CopyTo(Array array, int index)
    {
      throw new NotImplementedException();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      throw new NotImplementedException();
    }
    #endregion
  }

  /// <summary>
  /// Interaction logic for BoxArray.xaml
  /// </summary>
  public partial class BoxArray : UserControl
  {
    #region XAML Properties

    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
                typeof(string), typeof(BoxArray), new PropertyMetadata("Label Text"));

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set
      {
        SetValue(LabelNameProperty, value);
        if (string.IsNullOrEmpty(value))
        {
          header.ShowState = true;
          header.Visibility = Visibility.Collapsed;
          header.SetContent(null);
        }
        else
        {
          header.Visibility = Visibility.Visible;
        }

      }
    }

    public static readonly DependencyProperty ItemWidthProperty = DependencyProperty.RegisterAttached("ItemWidth",
                typeof(int), typeof(BoxArray), new PropertyMetadata(100));

    public int ItemWidth
    {
      get { return (int)GetValue(ItemWidthProperty); }
      set { SetValue(ItemWidthProperty, value); }
    }

    #endregion

    public BoxArray()
    {
      InitializeComponent();
      header.SetContent(ItemPanel);
    }
  }
}
