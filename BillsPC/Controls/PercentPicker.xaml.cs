﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for PercentPicker.xaml
  /// </summary>
  public partial class PercentPicker : UserControl
  {
    public PercentPicker()
    {
      InitializeComponent();

      DataObject.AddCopyingHandler(this, (sender, e) => { if (e.IsDragDrop) e.CancelCommand(); });
    }

    private void Picker_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      //SS.SwitchType();
    }

    private void txtPercent_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box == null)
      {
        return;
      }

      string regex = @"^1?\d\d?$";
      if (box.Text == "" || Regex.Match(box.Text, regex).Success)
      {
        box.Background = SystemColors.WindowBrush;
        SpeciesTypeUpdate(sender, null);
      }
      else
      {
        box.Background = new SolidColorBrush(Colors.Crimson);
      }
    }

    private void IntegerFilter(object sender, TextCompositionEventArgs e)
    {
      Regex regex = new Regex(@"\d");
      e.Handled = !regex.IsMatch(e.Text);
    }

    private void txtPercent_PreviewKeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter)
        SpeciesTypeUpdate(sender, null);
    }

    private void SpeciesTypeUpdate(object sender, KeyboardFocusChangedEventArgs e)
    {
      string text = txtPercent.Text;
      float value = 0;

      if (float.TryParse(text, out value) && cbPicker.SelectedItem != null)
      {
        //SS.UpdateTypePercentage(new PokeType().Parse(cbPicker.SelectedItem.ToString()), value / 100);
      }
    }

    private void AutoSelectOnTab(object sender, RoutedEventArgs e)
    {
      (sender as TextBox).SelectAll();
    }

    public static readonly DependencyProperty PickerSwitchProperty = DependencyProperty.RegisterAttached("PickerSwitch",
            typeof(string), typeof(PercentPicker), new FrameworkPropertyMetadata(null));

    public static string GetPickerSwitch(UIElement element)
    {
      if (element == null)
        throw new ArgumentNullException("element");
      return (string)element.GetValue(PickerSwitchProperty);
    }

    public static void SetPickerSwitch(UIElement element, string value)
    {
      if (element == null)
        throw new ArgumentNullException("element");
      element.SetValue(PickerSwitchProperty, value);
    }


    public static readonly DependencyProperty UpdatePercentageProperty = DependencyProperty.RegisterAttached("UpdatePercentage",
            typeof(string), typeof(PercentPicker), new FrameworkPropertyMetadata(null));


    public static string GetUpdatePercentage(UIElement element)
    {
      if (element == null)
        throw new ArgumentNullException("element");
      return (string)element.GetValue(UpdatePercentageProperty);
    }

    public static void SetUpdatePercentage(UIElement element, string value)
    {
      if (element == null)
        throw new ArgumentNullException("element");
      element.SetValue(UpdatePercentageProperty, value);
    }
  }
}
