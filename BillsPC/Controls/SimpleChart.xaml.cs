﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for SimpleChart.xaml
  /// </summary>
  public partial class SimpleChart : UserControl
  {


    public SeriesCollection SeriesCollection { get; set; }

    private StatGrowth _curve;
    public StatGrowth CurrentCurve
    {
      get { return _curve; }
      set
      {
        _curve = value;
        RecalculateCurve();
      }
    }

    private void RecalculateCurve()
    {
      int count = 20;
      int i = 0;

      foreach (var g in _curve.GetValues())
      {
        var list = StatGrowthFormulas.GetGrowthCurve(g, 50, count);
        for (int j = 0; j < count; j++)
        {
          if (SeriesCollection[i].Values.Count > j)
            SeriesCollection[i].Values.RemoveAt(j);
          SeriesCollection[i].Values.Insert(j, list[j]);
        }
        if (g == _curve)
        {
          ((LineSeries)SeriesCollection[i]).Stroke = new SolidColorBrush(Colors.Crimson);
          ((LineSeries)SeriesCollection[i]).StrokeThickness = 3;
          Panel.SetZIndex(((LineSeries)SeriesCollection[i]), 1);
          Chart.AxisX[0].Title = g.GetDescription();
        }

        i++;
      }
      YAxis.MaxValue = 25;
    }

    public SimpleChart(StatGrowth growth)
    {
      InitializeComponent();

      SeriesCollection = new SeriesCollection();

      foreach(var g in growth.GetValues())
      {
        var series = new LineSeries
        {
          PointGeometry = null,
          Values = new ChartValues<ObservablePoint>(),
          FontFamily = new FontFamily("Segoe UI"),
          DataLabels = false,
          Stroke = new SolidColorBrush(Colors.DimGray),
          Fill = new SolidColorBrush(Colors.Transparent)
        };
        series.Stroke.Opacity = 0.75;
        Panel.SetZIndex(series, 0);
        SeriesCollection.Add(series);
      }

      Chart.DisableAnimations = true;
      Chart.DataTooltip = null;

      CurrentCurve = growth;
      DataContext = this;
    }
  }
}
