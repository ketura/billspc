﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Delegate used to check whether the tab is currently dirty.
  /// </summary>
  /// <returns>
  /// tab.Dirty == true.
  /// </returns>
  public delegate bool ClickHandler();

  /// <summary>
  /// A pair of buttons, one used for Import and the other for Export.  Whether these export all or groups or
  /// individual chunks of data is up to the tab configuration.
  /// </summary>
  public partial class ImportExportButtons : UserControl
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    /// <summary>
    /// Before calling the appropriate import, this will check to see if the user should be prompted to save
    /// changes first.
    /// </summary>
    public event ClickHandler ImportDirtyCheck;

    /// <summary>
    /// The function to call when import is requested.
    /// </summary>
    public event Action<string> OnImportFileSelected;

    /// <summary>
    /// The function to call when export is requested.
    /// </summary>
    public event Action<string> OnExportFileSelected;

    /// <summary>
    /// If this is set, the import process will check ImportDirtyCheck and prompt the user to save if true.
    /// Exposed to XAML.
    /// </summary>
    public bool AutoSaveCheck
    {
      get { return (bool)GetValue(AutoSaveCheckProperty); }
      set { SetValue(AutoSaveCheckProperty, value); }
    }
    public static readonly DependencyProperty AutoSaveCheckProperty = DependencyProperty.RegisterAttached("AutoSaveCheck",
            typeof(bool), typeof(ImportExportButtons), new FrameworkPropertyMetadata(true));

    public bool Multiselect
    {
      get { return (bool)GetValue(MultiselectProperty); }
      set { SetValue(MultiselectProperty, value); }
    }
    public static readonly DependencyProperty MultiselectProperty = DependencyProperty.RegisterAttached("Multiselect",
            typeof(bool), typeof(ImportExportButtons), new FrameworkPropertyMetadata(true));

    /// <summary>
    /// The text to put on the import button's face.
    /// Exposed to XAML.
    /// </summary>
    public string ImportLabel
    {
      get { return (string)GetValue(ImportLabelProperty); }
      set { SetValue(ImportLabelProperty, value); }
    }
    public static readonly DependencyProperty ImportLabelProperty = DependencyProperty.RegisterAttached("ImportLabel",
            typeof(string), typeof(ImportExportButtons), new FrameworkPropertyMetadata("Import"));

    /// <summary>
    /// The text to put on the export button's face.
    /// Exposed to XAML.
    /// </summary>
    public string ExportLabel
    {
      get { return (string)GetValue(ExportLabelProperty); }
      set { SetValue(ExportLabelProperty, value); }
    }
    public static readonly DependencyProperty ExportLabelProperty = DependencyProperty.RegisterAttached("ExportLabel",
            typeof(string), typeof(ImportExportButtons), new FrameworkPropertyMetadata("Export"));

    /// <summary>
    /// The filename filter to pass to the appropriate file save/open dialogs.
    /// Exposed to XAML.
    /// </summary>
    public string FilenameFilter
    {
      get { return (string)GetValue(FilenameFilterProperty); }
      set { SetValue(FilenameFilterProperty, value); }
    }
    public static readonly DependencyProperty FilenameFilterProperty = DependencyProperty.RegisterAttached("FilenameFilter",
            typeof(string), typeof(ImportExportButtons), new FrameworkPropertyMetadata(null));


    public ImportExportButtons()
    {
      InitializeComponent();
      btnImport.Click += this.btnImport_Click;
      btnExport.Click += this.btnExport_Click;
    }

    /// <summary>
    /// The user has clicked the import button.  If AutoSaveCheck is set, the user will be prompted to save his
    /// data if the tab is detected to be dirty (based on the return value of ImportDirtyCheck).
    /// </summary>
    private void btnImport_Click(object sender, RoutedEventArgs e)
    {
      bool dirty = false;

      if (ImportDirtyCheck != null)
        dirty = ImportDirtyCheck();

      if (AutoSaveCheck && dirty)
      {
        var result = MessageBox.Show("Warning! Unsaved data will be lost.  Would you like to save your data first?", "Unsaved data", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);
        switch (result)
        {
          //Note that this only really works if the buttons are intended to be used as import/export all data.
          // For individual exports, there's no guarantee that saving the current widget data will save the widget
          // that triggered the dirty state.
          case MessageBoxResult.Yes:
            btnExport_Click(sender, e);
            break;

          case MessageBoxResult.Cancel:
            return;
        }
      }

      OpenFileDialog dialog = new OpenFileDialog() { Multiselect = Multiselect };
      dialog.Filter = FilenameFilter ?? "JSON file (*.json)|*.json|All Files (*.*)|*.*";
      if (dialog.ShowDialog() == true && OnImportFileSelected != null)
      {
        foreach (String file in dialog.FileNames)
        {
          OnImportFileSelected(file);
        }
      }
    }

    /// <summary>
    /// The user has clicked the export button.  Passes the selected file path to the registered export function.
    /// </summary>
    private void btnExport_Click(object sender, RoutedEventArgs e)
    {
      SaveFileDialog dialog = new SaveFileDialog();
      dialog.Filter = FilenameFilter ?? "JSON file (*.json)|*.json|All Files (*.*)|*.*";
      if (dialog.ShowDialog() == true && OnExportFileSelected != null)
      {
        OnExportFileSelected(dialog.FileName);
      }
    }
  }
}
