﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  public partial class Behavior : UserControl
  {
    public GroupType GroupType
    {
      get
      {
        if (!cbHerd.IsChecked.Value)
          return GroupType.None;
        if (rbFamily.IsChecked.Value)
          return GroupType.Family;
        if (rbPack.IsChecked.Value)
          return GroupType.Pack;
        if (rbHerd.IsChecked.Value)
          return GroupType.Herd;
        //if (rbPairs.IsChecked.Value)
          return GroupType.Pairs;
      }
      set
      {
        switch (value)
        {
          default:
          case GroupType.None:
            cbHerd.IsChecked = false;
            break;
          case GroupType.Pairs:
            cbHerd.IsChecked = true;
            rbPairs.IsChecked = true;
            break;
          case GroupType.Family:
            cbHerd.IsChecked = true;
            rbFamily.IsChecked = true;
            break;
          case GroupType.Pack:
            cbHerd.IsChecked = true;
            rbPack.IsChecked = true;
            break;
          case GroupType.Herd:
            cbHerd.IsChecked = true;
            rbHerd.IsChecked = true;
            break;
        }
      }
    }

    public int MaxGroupSize
    {
      get { return txtMaxGroupSize.Value; }
      set { txtMaxGroupSize.Value = value; }
    }

    public bool HasNests
    {
      get { return cbHasNests.IsChecked.Value; }
      set { cbHasNests.IsChecked = value; }
    }

    public bool Nocturnal
    {
      get { return cbNocturnal.IsChecked.Value; }
      set { cbNocturnal.IsChecked = value; }
    }

    public bool Territorial
    {
      get { return cbTerritorial.IsChecked.Value; }
      set { cbTerritorial.IsChecked = value; }
    }

    public string TerritorialList
    {
      get { return txtTerritorial.Text; }
      set { txtTerritorial.Text = value; }
    }
    public int BreedingLevel
    {
      get { return txtBreedLevel.Value; }
      set { txtBreedLevel.Value = value; }
    }

    public string PreferredFood
    {
      get { return txtPreferredFood.Text; }
      set { txtPreferredFood.Text = value; }
    }

    public bool Nomadic
    {
      get { return cbNomadic.IsChecked.Value; }
      set { cbNomadic.IsChecked = value; }
    }

    public bool Aggressive
    {
      get { return cbAggressive.IsChecked.Value; }
      set { cbAggressive.IsChecked = value; }
    }

    public bool Carnivore
    {
      get { return cbEats.IsChecked == true || cbEats.IsChecked == null; }
      set
      {
        if (value)
        {
          if (cbEats.IsChecked == false)
            cbEats.IsChecked = null;
        }
        else
        {
          cbEats.IsChecked = false;
        }
      }
    }

    public bool Herbivore
    {
      get { return cbEats.IsChecked == false || cbEats.IsChecked == null; }
      set
      {
        if (value)
        {
          if (cbEats.IsChecked == true)
            cbEats.IsChecked = null;
        }
        else
        {
          cbEats.IsChecked = true;
        }
      }
    }

    public void SetFoodType(bool carnivore, bool herbivore)
    {
      cbEats.IsChecked = null;

      if (carnivore && !herbivore)
      {
        cbEats.IsChecked = true;
      }
      else
      {
        cbEats.IsChecked = false;
      }
    }

    public Behavior()
    {
      InitializeComponent();
    }
  }
}
