﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for LabelPicker.xaml
  /// </summary>
  public partial class PickerLabel : UserControl
  {
    public event SelectionChangedEventHandler SelectionChanged;

    public ComboBox Picker { get { return cbPicker; } }

    public object SelectedItem
    {
      get { return cbPicker.SelectedItem; }
      set { cbPicker.SelectedItem = value; }
    }

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }
    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(PickerLabel), new PropertyMetadata("Label Text"));

    


    public double PickerWidth
    {
      get { return (double)GetValue(PickerWidthProperty); }
      set { SetValue(PickerWidthProperty, value); }
    }
    public static readonly DependencyProperty PickerWidthProperty = DependencyProperty.RegisterAttached("PickerWidth",
            typeof(double), typeof(PickerLabel), new PropertyMetadata(50.0));

    public PickerLabel()
    {
      InitializeComponent();
    }

    private void cbPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      SelectionChanged?.Invoke(sender, e);
    }
  }
}
