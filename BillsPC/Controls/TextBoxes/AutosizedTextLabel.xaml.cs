﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for AutosizedTextLabel.xaml
  /// </summary>
  public partial class AutosizedTextLabel : UserControl
  {
    public event TextChangedEventHandler TextChanged;

    public static readonly DependencyProperty TextProperty = TextBox.TextProperty.AddOwner(typeof(AutosizedTextLabel));

    public string Text
    {
      get { return (string)GetValue(TextProperty); }
      set { SetValue(TextProperty, value); }

    }

    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(AutosizedTextLabel), new PropertyMetadata("Description:"));

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }

    public AutosizedTextLabel()
    {
      InitializeComponent();
      DataObject.AddCopyingHandler(this, (sender, e) => { if (e.IsDragDrop) e.CancelCommand(); });
    }

    private void txtDescription_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextChanged?.Invoke(sender, e);
    }
  }
}
