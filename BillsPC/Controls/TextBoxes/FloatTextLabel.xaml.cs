﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for FloatTextLabel.xaml
  /// </summary>
  public partial class FloatTextLabel : UserControl
  {
    protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public event TextChangedEventHandler TextChanged;

    protected string FormatString = "{0:0.0#}";
    protected string SignedCharacterRegex = @"[\d-\.]";
    protected string SignedCompleteRegex = @"^-?\d*\.?\d*$";
    protected string UnsignedCharacterRegex = @"[\d\.]";
    protected string UnsignedCompleteRegex = @"^\d*\.?\d*$";

    public bool Valid { get; protected set; }

    private float? _value;

    public float Value
    {
      get
      {
        int value = 0;
        if (Valid && int.TryParse(txtFloat.Text, out value))
        {
          return value;
        }
        else
        {
          return 0;
        }
      }

      set
      {
        _value = value;
        if (_value == null)
        {
          txtFloat.Text = "";
        }
        else
        {
          Text = _value.ToString();
        }
        Valid = true;
      }
    }

    public string Text
    {
      get { return txtFloat.Text; }
      set
      {
        txtFloat.Text = value;
        ReformatText();
      }
    }


    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(FloatTextLabel), new PropertyMetadata("Label Text"));

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }

    public static readonly DependencyProperty MaxDigitsProperty = DependencyProperty.RegisterAttached("MaxDigits",
            typeof(int), typeof(FloatTextLabel), new PropertyMetadata(4));

    public int MaxDigits
    {
      get { return (int)GetValue(MaxDigitsProperty); }
      set { SetValue(MaxDigitsProperty, value); }
    }

    public static readonly DependencyProperty UnsignedProperty = DependencyProperty.RegisterAttached("Unsigned",
            typeof(bool), typeof(FloatTextLabel), new PropertyMetadata(false));

    public bool Unsigned
    {
      get { return (bool)GetValue(UnsignedProperty); }
      set { SetValue(UnsignedProperty, value); }
    }


    public FloatTextLabel()
    {
      InitializeComponent();
      txtFloat.GotFocus += AutoSelectOnTab;
      txtFloat.LostKeyboardFocus += TypeTabAway;
      txtFloat.PreviewKeyDown += TypeEnterListen;
      txtFloat.TextChanged += FloatTextChangedCheck;
      txtFloat.PreviewTextInput += InputFilter;

      DataObject.AddCopyingHandler(this, (sender, e) => { if (e.IsDragDrop) e.CancelCommand(); });
    }

    private void AutoSelectOnTab(object sender, RoutedEventArgs e)
    {
      (sender as TextBox).SelectAll();
    }

    private void TypeTabAway(object sender, KeyboardFocusChangedEventArgs e)
    {
      ReformatText();
    }

    private void ReformatText()
    {
      float temp = 0;
      if (!float.TryParse(txtFloat.Text, out temp))
        return;

      txtFloat.Text = string.Format(FormatString, temp);
      txtFloat.CaretIndex = txtFloat.Text.Length;
    }

    private void TypeEnterListen(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter)
        ReformatText();
    }

    private void FloatTextChangedCheck(object sender, TextChangedEventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box == null)
      {
        return;
      }

      if (box.Text == "" || Regex.Match(box.Text, Unsigned ? UnsignedCompleteRegex : SignedCompleteRegex).Success)
      {
        box.Background = SystemColors.WindowBrush;
        Valid = true;
      }
      else
      {
        box.Background = new SolidColorBrush(Colors.Crimson);
        Valid = false;
        e.Handled = true;
      }
    }

    private void InputFilter(object sender, TextCompositionEventArgs e)
    {
      Regex regex = new Regex(Unsigned ? UnsignedCharacterRegex : SignedCharacterRegex);
      e.Handled = !regex.IsMatch(e.Text);
    }

    private void txtFloat_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextChanged?.Invoke(this, e);
    }
  }
}
