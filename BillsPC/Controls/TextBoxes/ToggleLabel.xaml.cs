﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for ToggleLabel.xaml
  /// </summary>
  public partial class ToggleLabel : UserControl
  {
    public ToggleLabel()
    {
      InitializeComponent();
    }

    public event Action<bool?> ToggleChanged;

    public bool? IsChecked
    {
      get { return Toggle.IsChecked; }
      set { Toggle.IsChecked = value; }
    }

    public static readonly DependencyProperty ThreeStateProperty = DependencyProperty.RegisterAttached("ThreeState",
            typeof(bool), typeof(ToggleLabel), new PropertyMetadata(false));

    public bool ThreeState
    {
      get { return (bool)GetValue(ThreeStateProperty); }
      set { SetValue(ThreeStateProperty, value); }
    }

    public static readonly DependencyProperty TrueLabelProperty = DependencyProperty.RegisterAttached("TrueLabel",
            typeof(string), typeof(ToggleLabel), new PropertyMetadata("True"));

    public string TrueLabel
    {
      get { return (string)GetValue(TrueLabelProperty); }
      set { SetValue(TrueLabelProperty, value); }
    }

    public static readonly DependencyProperty FalseLabelProperty = DependencyProperty.RegisterAttached("FalseLabel",
            typeof(string), typeof(ToggleLabel), new PropertyMetadata("False"));

    public string FalseLabel
    {
      get { return (string)GetValue(FalseLabelProperty); }
      set { SetValue(FalseLabelProperty, value); }
    }

    public static readonly DependencyProperty ToggleWidthProperty = DependencyProperty.RegisterAttached("ToggleWidth",
            typeof(double), typeof(ToggleLabel), new PropertyMetadata(100.0));

    public double ToggleWidth
    {
      get { return (double)GetValue(ToggleWidthProperty); }
      set { SetValue(ToggleWidthProperty, value); }
    }

    public static readonly DependencyProperty TextPaddingProperty = DependencyProperty.RegisterAttached("TextPadding",
            typeof(double), typeof(ToggleLabel), new PropertyMetadata(100.0));

    public double TextPadding
    {
      get { return (double)GetValue(TextPaddingProperty); }
      set { SetValue(TextPaddingProperty, value); }
    }

    private void Toggle_CheckedChanged(object sender, RoutedEventArgs e)
    {
      ToggleChanged?.Invoke(IsChecked);
    }
  }
}
