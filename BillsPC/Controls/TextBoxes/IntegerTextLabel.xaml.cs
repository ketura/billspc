﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for IntegerTextLabel.xaml
  /// </summary>

  public partial class IntegerTextLabel : UserControl
  {
    protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public event TextChangedEventHandler TextChanged;
    public event EventHandler GotFocus;

    protected string FormatString = "{0:0}";
    protected string SignedCharacterRegex = @"[\d-]";
    protected string SignedCompleteRegex = @"^-?\d*\d*$";
    protected string UnsignedCharacterRegex = @"[\d]";
    protected string UnsignedCompleteRegex = @"^\d*$";

    public bool Valid { get; protected set; }

    private int? _value;

    public int Value
    {
      get
      {
        int value = 0;
        if (Valid && int.TryParse(txtInt.Text, out value))
        {
          return value;
        }
        else
        {
          return 0;
        }
      }

      set
      {
        _value = Math.Max(0, value);
        _value = Math.Min(999, (int)_value);
        if (_value == null)
        {
          txtInt.Text = "";
        }
        else
        {
          Text = _value.ToString();
        }
        Valid = true;
      }
    }

    public static readonly DependencyProperty TextProperty = TextBox.TextProperty.AddOwner(typeof(IntegerTextLabel));

    public string Text
    {
      get { return (string)GetValue(TextProperty); }
      set
      {
        SetValue(TextProperty, value);
        txtInt.Text = Text;
        ReformatText();
      }
    }


    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(IntegerTextLabel), new PropertyMetadata("Label Text"));

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }

    public static readonly DependencyProperty BackLabelProperty = DependencyProperty.RegisterAttached("BackLabel",
            typeof(string), typeof(IntegerTextLabel), new PropertyMetadata(""));

    public string BackLabel
    {
      get { return (string)GetValue(BackLabelProperty); }
      set { SetValue(BackLabelProperty, value); }
    }

    public static readonly DependencyProperty MaxDigitsProperty = DependencyProperty.RegisterAttached("MaxDigits",
            typeof(int), typeof(IntegerTextLabel), new PropertyMetadata(4));

    public int MaxDigits
    {
      get { return (int)GetValue(MaxDigitsProperty); }
      set { SetValue(MaxDigitsProperty, value); }
    }

    public static readonly DependencyProperty UnsignedProperty = DependencyProperty.RegisterAttached("Unsigned",
            typeof(bool), typeof(IntegerTextLabel), new PropertyMetadata(false));

    public bool Unsigned
    {
      get { return (bool)GetValue(UnsignedProperty); }
      set { SetValue(UnsignedProperty, value); }
    }




    public IntegerTextLabel()
    {
      InitializeComponent();
      txtInt.GotFocus += AutoSelectOnTab;
      txtInt.LostKeyboardFocus += TypeTabAway;
      txtInt.PreviewKeyDown += TypeEnterListen;
      txtInt.TextChanged += IntTextChangedCheck;
      txtInt.TextChanged += TxtInt_TextChanged;
      txtInt.PreviewTextInput += InputFilter;

      DataObject.AddCopyingHandler(this, (sender, e) => { if (e.IsDragDrop) e.CancelCommand(); });
    }

    private void TxtInt_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextChanged?.Invoke(this, e);
    }

    private void AutoSelectOnTab(object sender, RoutedEventArgs e)
    {
      (sender as TextBox).SelectAll();
      GotFocus?.Invoke(this, e);
    }

    private void TypeTabAway(object sender, KeyboardFocusChangedEventArgs e)
    {
        ReformatText();
    }

    private void ReformatText()
    {
      int temp = 0;
      if (!int.TryParse(txtInt.Text, out temp))
        return;

      txtInt.Text = string.Format(FormatString, temp);
      txtInt.CaretIndex = txtInt.Text.Length;
    }

    private void TypeEnterListen(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter)
        ReformatText();
    }

    private void IntTextChangedCheck(object sender, TextChangedEventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box == null)
      {
        return;
      }

      if (box.Text == "" || Regex.Match(box.Text, Unsigned ? UnsignedCompleteRegex : SignedCompleteRegex).Success)
      {
        box.Background = SystemColors.WindowBrush;
        Valid = true;
      }
      else
      {
        box.Background = new SolidColorBrush(Colors.Crimson);
        Valid = false;
      }
    }

    private void InputFilter(object sender, TextCompositionEventArgs e)
    {
      Regex regex = new Regex(Unsigned ? UnsignedCharacterRegex : SignedCharacterRegex);
      e.Handled = !regex.IsMatch(e.Text);
    }
  }
}

