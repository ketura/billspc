﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for PercentTextLabel.xaml
  /// </summary>
  public partial class PercentTextLabel : UserControl
  {
    protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public event TextChangedEventHandler TextChanged;

    protected string FormatString = "{0:#}";
    protected string UnsignedCharacterRegex = @"[\d]";
    protected string UnsignedCompleteRegex = @"^\d*$";

    public bool Valid { get; protected set; }

    private float? _value;

    //Hmm.  figure this out.
    public float Value
    {
      get
      {
        float value = 0;
        if (Valid && float.TryParse(txtPercent.Text, out value))
        {
          return value / 100;
        }
        else
        {
          return 0;
        }
      }

      set
      {
        _value = Math.Max(0f, value);
        _value = Math.Min(MaxValue * 100, (float)_value);
        if (_value == null)
        {
          txtPercent.Text = "";
        }
        else
        {
          Text = (_value * 100).ToString();
        }
        Valid = true;
      }
    }

    public static readonly DependencyProperty TextProperty = TextBox.TextProperty.AddOwner(typeof(PercentTextLabel));

    public string Text
    {
      get { return (string)GetValue(TextProperty); }
      set
      {
        SetValue(TextProperty, value);
        txtPercent.Text = Text;
        ReformatText();
      }
    }


    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(PercentTextLabel), new PropertyMetadata("Label Text"));

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }

    public static readonly DependencyProperty MaxDigitsProperty = DependencyProperty.RegisterAttached("MaxDigits",
            typeof(int), typeof(PercentTextLabel), new PropertyMetadata(4));

    public int MaxDigits
    {
      get { return (int)GetValue(MaxDigitsProperty); }
      set { SetValue(MaxDigitsProperty, value); }
    }

    public static readonly DependencyProperty MaxValueProperty = DependencyProperty.RegisterAttached("MaxValue",
            typeof(float), typeof(PercentTextLabel), new PropertyMetadata(1.0f));

    public float MaxValue
    {
      get { return (float)GetValue(MaxValueProperty); }
      set { SetValue(MaxValueProperty, value); }
    }

    public PercentTextLabel()
    {
      InitializeComponent();
      txtPercent.GotFocus += AutoSelectOnTab;
      txtPercent.LostKeyboardFocus += TypeTabAway;
      txtPercent.PreviewKeyDown += TypeEnterListen;
      txtPercent.TextChanged += TextChangedCheck;
      txtPercent.PreviewTextInput += InputFilter;

      DataObject.AddCopyingHandler(this, (sender, e) => { if (e.IsDragDrop) e.CancelCommand(); });
    }

    private void AutoSelectOnTab(object sender, RoutedEventArgs e)
    {
      (sender as TextBox).SelectAll();
    }

    private void TypeTabAway(object sender, KeyboardFocusChangedEventArgs e)
    {
      ReformatText();
    }

    private void ReformatText()
    {
      float temp = 0;
      if (!float.TryParse(txtPercent.Text, out temp))
        return;

      txtPercent.Text = string.Format(FormatString, temp);
      txtPercent.CaretIndex = txtPercent.Text.Length;
    }

    private void TypeEnterListen(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter)
        ReformatText();
    }

    private void TextChangedCheck(object sender, TextChangedEventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box == null)
      {
        return;
      }

      if (box.Text == "" || Regex.Match(box.Text, UnsignedCompleteRegex).Success)
      {
        float value = 0;
        float.TryParse(box.Text, out value);

        _value = Math.Max(0f, value);
        _value = Math.Min(MaxValue * 100, (float)_value);

        box.Text = ((float)_value).ToString("0");

        box.Background = SystemColors.WindowBrush;
        Valid = true;
      }
      else
      {
        box.Background = new SolidColorBrush(Colors.Crimson);
        Valid = false;
      }
    }

    private void InputFilter(object sender, TextCompositionEventArgs e)
    {
      Regex regex = new Regex(UnsignedCharacterRegex);
      e.Handled = !regex.IsMatch(e.Text);
    }

    private void txtPercent_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextChanged?.Invoke(this, e);
    }
  }
}
