﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for PercentMultiPicker.xaml
  /// </summary>
  public partial class PercentMultiPicker : UserControl
  {

    public event SelectionChangedEventHandler SelectionChanged;

    public static readonly DependencyProperty TextProperty = TextBox.TextProperty.AddOwner(typeof(PercentMultiPicker));

    public string Text
    {
      get { return (string)GetValue(TextProperty); }
      set { SetValue(TextProperty, value); }
    }

    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(PercentMultiPicker), new PropertyMetadata("Label Text"));

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }

    public static readonly DependencyProperty ComboSourceProperty = DependencyProperty.RegisterAttached("ComboSource",
            typeof(IEnumerable), typeof(PercentMultiPicker));

    public IEnumerable ComboSource
    {
      get { return (IEnumerable)GetValue(ComboSourceProperty); }
      set { SetValue(ComboSourceProperty, value); }
    }

    public object SelectedItem
    {
      get { return cbPicker.SelectedItem; }
      set { cbPicker.SelectedItem = value; }
    }

    public float Value
    {
      get { return txtPercent.Value; }
      set { txtPercent.Value = value; }
    }

    public PercentMultiPicker()
    {
      InitializeComponent();
      cbPicker.SelectionChanged += cbPicker_SelectionChanged;
    }

    private void cbPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (SelectionChanged != null)
        SelectionChanged(null, e);
    }
  }
}
