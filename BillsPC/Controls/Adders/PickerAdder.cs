﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BillsPC.Controls.Adders
{
  public class PickerAdder2<T> : PickerAdderBase
    where T : struct, IConvertible, IComparable
  {
    public PickerAdder2() : base()
    {
      AutoCycle = false;
    }

    protected override void btnAdd_Click2(object sender, RoutedEventArgs e)
    {
      //AddElement?.Invoke((T)Selector.SelectedItem);
      if (AutoCycle && Selector.SelectedIndex < Selector.Items.Count - 1)
        Selector.SelectedIndex += 1;
      else
        Selector_SelectionChanged(null, null);
    }

    public void Delete(T type)
    {
      //DeleteElement?.Invoke(type);
      if (AutoCycle && Selector.SelectedIndex > 0)
        Selector.SelectedIndex -= 1;
      else
        Selector_SelectionChanged(null, null);
    }

    protected override void btnDelete_Click(object sender, RoutedEventArgs e)
    {
      if (ConfirmDelete)
      {
        if (MessageBoxResult.No == MessageBox.Show(DeletionMessage, "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation))
        {
          return;
        }
      }

      Delete((T)Selector.SelectedItem);
    }
  }


  public class PickerAdderBase : AdderControlPanel<ComboBox>
  {
    protected override void SelectorWidthChanged(int? i)
    {
      Selector.Width = i ?? 100;
    }

    public bool AutoCycle { get; set; }

    public IEnumerable ComboSource
    {
      get { return Selector.ItemsSource; }
      set
      {
        Selector.ItemsSource = value;
        Selector_SelectionChanged(null, null);
        if (Selector.Items.Count > 0)
          Selector.SelectedIndex = 0;
      }
    }

    public PickerAdderBase() : base(new ComboBox()
    {
      HorizontalAlignment = HorizontalAlignment.Center,
      VerticalAlignment = VerticalAlignment.Center,
      VerticalContentAlignment = VerticalAlignment.Center,
      Width = 120,
      Height = 23, 
      Margin = new Thickness(5),
      Text = ""
    })
    { Init(); }

    public PickerAdderBase(ComboBox c) : base(c) { Init(); }

    private void Init()
    {
      Selector.SelectionChanged += Selector_SelectionChanged;
    }

    protected virtual void Selector_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (Contains == null)
        return;

      if (string.IsNullOrEmpty(Selector.SelectedItem?.ToString()))
      {
        btnAdd.IsEnabled = false;
        btnDelete.IsEnabled = false;
        return;
      }

      if (Contains(Selector.SelectedItem?.ToString()))
      {
        btnDelete.IsEnabled = true;
        btnAdd.IsEnabled = false;
      }
      else
      {
        btnDelete.IsEnabled = false;
        btnAdd.IsEnabled = true;
      }
    }

    protected void ValidateButtons()
    {

    }

    protected virtual void btnAdd_Click2(object sender, RoutedEventArgs e)
    {
      //AddElement?.Invoke(Selector.SelectedValue.ToString());
      if (AutoCycle && Selector.SelectedIndex < Selector.Items.Count - 1)
        Selector.SelectedIndex += 1;
      else
        Selector_SelectionChanged(null, null);
    }

    public void Delete2(string name)
    {
      //DeleteElement?.Invoke(name);
      if (AutoCycle && Selector.SelectedIndex > 0)
        Selector.SelectedIndex -= 1;
      else
        Selector_SelectionChanged(null, null);
    }

    public override void AddItem()
    {
      //throw new NotImplementedException();
    }

    public override void RemoveItem()
    {
      //throw new NotImplementedException();
    }

    public override void Display(string s)
    {
      Selector.Text = s;
    }
  }
}
