﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{

  public class PickerAdder<T> : PickerAdderBase
    where T : struct, IConvertible, IComparable
  {
    public new event Action<T> AddElement;
    public new event Action<T> DeleteElement;

    public PickerAdder() : base()
    {
      AutoCycle = false;
    }

    protected override void btnAdd_Click(object sender, RoutedEventArgs e)
    {
      AddElement?.Invoke((T)cbNew.SelectedItem);
      if (AutoCycle && cbNew.SelectedIndex < cbNew.Items.Count - 1)
        cbNew.SelectedIndex += 1;
      else
        cbNew_SelectionChanged(null, null);
    }

    public void Delete(T type)
    {
      DeleteElement?.Invoke(type);
      if (AutoCycle && cbNew.SelectedIndex > 0)
        cbNew.SelectedIndex -= 1;
      else
        cbNew_SelectionChanged(null, null);
    }

    protected override void btnDelete_Click(object sender, RoutedEventArgs e)
    {
      if (ConfirmDelete)
      {
        if (MessageBoxResult.No == MessageBox.Show(DeletionMessage, "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation))
        {
          return;
        }
      }

      Delete((T)cbNew.SelectedItem);
    }
  }


  public partial class PickerAdderBase : UserControl
  {

    public CheckContent Contains;

    public virtual event Action<string> AddElement;
    public virtual event Action<string> DeleteElement;

    public bool ConfirmDelete { get; set; }

    public string DeletionMessage { get; set; }

    public bool AutoCycle { get; set; }

    /// <summary>
    /// The text to put on the add button's face.
    /// Exposed to XAML.
    /// </summary>
    public string AddLabel
    {
      get { return (string)GetValue(AddLabelProperty); }
      set { SetValue(AddLabelProperty, value); }
    }
    public static readonly DependencyProperty AddLabelProperty = DependencyProperty.RegisterAttached("AddLabel",
            typeof(string), typeof(PickerAdderBase), new FrameworkPropertyMetadata("Add"));

    /// <summary>
    /// The text to put on the delete button's face.
    /// Exposed to XAML.
    /// </summary>
    public string DeleteLabel
    {
      get { return (string)GetValue(DeleteLabelProperty); }
      set { SetValue(DeleteLabelProperty, value); }
    }
    public static readonly DependencyProperty DeleteLabelProperty = DependencyProperty.RegisterAttached("DeleteLabel",
            typeof(string), typeof(PickerAdderBase), new FrameworkPropertyMetadata("Delete"));

    /// <summary>
    /// The Width of the combo box.
    /// Exposed to XAML.
    /// </summary>
    public int PickerWidth
    {
      get { return (int)GetValue(PickerWidthProperty); }
      set { SetValue(PickerWidthProperty, value); }
    }
    public static readonly DependencyProperty PickerWidthProperty = DependencyProperty.RegisterAttached("PickerWidth",
            typeof(int), typeof(PickerAdderBase), new FrameworkPropertyMetadata(120));

    /// <summary>
    /// The Width of the picker label.
    /// Exposed to XAML.
    /// </summary>
    public int LabelWidth
    {
      get { return (int)GetValue(LabelWidthProperty); }
      set { SetValue(LabelWidthProperty, value); }
    }
    public static readonly DependencyProperty LabelWidthProperty = DependencyProperty.RegisterAttached("LabelWidth",
            typeof(int), typeof(PickerAdderBase), new FrameworkPropertyMetadata(0));

    /// <summary>
    /// The text on the label.
    /// Exposed to XAML.
    /// </summary>
    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }
    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(PickerAdderBase), new FrameworkPropertyMetadata("Label Name"));

    public IEnumerable ComboSource
    {
      get { return cbNew.ItemsSource; }
      set
      {
        cbNew.ItemsSource = value;
        cbNew_SelectionChanged(null, null);
        if (cbNew.Items.Count > 0)
          cbNew.SelectedIndex = 0;
      }
    }

    public PickerAdderBase()
    {
      InitializeComponent();
      cbNew_SelectionChanged(null, null);
    }

    public void AdjustSelection()
    {
      cbNew_SelectionChanged(null, null);
    }

    protected virtual void cbNew_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (Contains == null)
        return;

      if (string.IsNullOrEmpty(cbNew.SelectedItem?.ToString()))
      {
        btnAdd.IsEnabled = false;
        btnDelete.IsEnabled = false;
        return;
      }

      if (Contains(cbNew.SelectedItem?.ToString()))
      {
        btnDelete.IsEnabled = true;
        btnAdd.IsEnabled = false;
      }
      else
      {
        btnDelete.IsEnabled = false;
        btnAdd.IsEnabled = true;
      }
    }

    protected virtual void btnAdd_Click(object sender, RoutedEventArgs e)
    {
      AddElement?.Invoke(cbNew.SelectedValue.ToString());
      if (AutoCycle && cbNew.SelectedIndex < cbNew.Items.Count - 1)
        cbNew.SelectedIndex += 1;
      else
        cbNew_SelectionChanged(null, null);
    }

    public virtual void Delete(string name)
    {
      DeleteElement?.Invoke(name);
      if (AutoCycle && cbNew.SelectedIndex > 0)
        cbNew.SelectedIndex -= 1;
      else
        cbNew_SelectionChanged(null, null);
    }

    protected virtual void btnDelete_Click(object sender, RoutedEventArgs e)
    {
      if (ConfirmDelete)
      {
        if (MessageBoxResult.No == MessageBox.Show(DeletionMessage, "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation))
        {
          return;
        }
      }

      Delete(cbNew.SelectedItem.ToString());
    }
  }
}
