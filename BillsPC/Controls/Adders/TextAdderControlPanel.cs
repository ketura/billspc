﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;

namespace BillsPC.Controls
{
  public class TextAdderControlPanel : AdderControlPanel<TextBox>
  {
    protected override void SelectorWidthChanged(int? i)
    {
      Selector.Width = i ?? 100;
    }

    public TextAdderControlPanel() : base(new TextBox()
    {
      HorizontalAlignment = HorizontalAlignment.Center,
      Height = 23,
      Margin = new Thickness(5),
      Text = "",
      VerticalAlignment = VerticalAlignment.Center,
      Width = 120,
      MaxLines = 1,
      VerticalContentAlignment = VerticalAlignment.Center
    })
    { Init(); }

    public TextAdderControlPanel(TextBox t) : base(t) { Init(); }

    private void Init()
    {
      Selector.PreviewKeyDown += Selector_PreviewKeyDown;
      Selector.TextChanged += Selector_TextChanged;
    }

    public override void AddItem()
    {
      InvokeAdd(Selector.Text);
      Selector.Text = "";
    }
    public override void RemoveItem()
    {
      InvokeDelete(Selector.Text);
      Selector.Text = "";
    }

    public override void Display(string s)
    {
      Selector.Text = s;
    }

    private void Selector_PreviewKeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter && btnAdd.IsEnabled)
        btnAdd_Click(null, null);
    }

    private void Selector_TextChanged(object sender, TextChangedEventArgs e)
    {
      if (string.IsNullOrEmpty(Selector.Text))
      {
        btnAdd.IsEnabled = false;
        btnDelete.IsEnabled = false;
        return;
      }

      if (Contains?.Invoke(Selector.Text) == true)
      {
        btnDelete.IsEnabled = true;
        btnAdd.IsEnabled = false;
      }
      else
      {
        btnDelete.IsEnabled = false;
        btnAdd.IsEnabled = true;
      }
    }
  }
}
