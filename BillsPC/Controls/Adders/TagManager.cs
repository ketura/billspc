﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillsPC.Controls
{
  public abstract class TagManager<T>
    where T : TagWrapper
  {
    public Dictionary<string, T> Tags { get; protected set; }

    public virtual void AddItem(T item)
    {
      if(!Tags.ContainsKey(item.TagName))
      {
        Tags[item.TagName] = item;
      }
    }
    public virtual void RemoveItem(T item)
    {
      if (Tags.ContainsKey(item.TagName))
      {
        Tags.Remove(item.TagName);
      }
    }

    public abstract void AddItem(string name);
    public abstract void RemoveItem(string name);

    //public override void AddItem(string name)
    //{
    //  AddItem(new MoveTag(new SpeciesMove() { Name = name }));
    //}

    //public override void AddItem(MoveTag item)
    //{
    //  Tags[item.Name] = item;
    //}
    //public override void RemoveItem(string name)
    //{
    //  if (Tags.ContainsKey(name))
    //  {
    //    Tags.Remove(name);
    //  }
    //}

    //public override void RemoveItem(MoveTag item)
    //{
    //  if (Tags.ContainsKey(item.Name))
    //  {
    //    Tags.Remove(item.Name);
    //  }
    //}
  }
}
