﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BillsPC.Controls
{
  public class EvolutionAdderControlPanel : TextAdderControlPanel
  {
    public TextBox EvolvesFrom { get; protected set; }
    public EvolutionAdderControlPanel() : base()
    {
      Label l = new Label() { Content = "Evolves From:" };
      ControlPanel.AddChildAt(l, 0);

      EvolvesFrom = new TextBox()
      {
        Margin = new Thickness(5),
        Width = 150
      };
      ControlPanel.AddChildAt(EvolvesFrom, 1);

      LabelName = "Evolves To:";
    }
  }

  public class EvolutionAdder : GroupAdder<WrapPanel, EvolutionAdderControlPanel, EvolutionTag>
  {
    public EvolutionAdder() : base()
    {
      ControlPanel = new EvolutionAdderControlPanel();
      ControlPanelStack.AddChildAt(ControlPanel, 0);

      ControlPanel.Add += AddItem;
      ControlPanel.Delete += DeleteItem;
      ControlPanel.Contains = Tags.ContainsKey;
    }

    public override void AddItem(string s)
    {
      if (string.IsNullOrEmpty(s))
        return;

      if (!Tags.ContainsKey(s))
      {
        Tags[s] = new EvolutionTag(s);
        Tags[s].CloseClick += DeleteItem;
        ItemPanel.Children.Add(Tags[s]);

        InvokeAdded(s);
      }
    }
    public override void DeleteItem(string s)
    {
      if (string.IsNullOrEmpty(s))
        return;

      if (Tags.ContainsKey(s))
      {
        ItemPanel.Children.Remove(Tags[s]);
        Tags.Remove(s);

        InvokeDeleted(s);
      }
    }
    public override void ResetAll(IEnumerable<string> list)
    {
      ItemPanel.Children.Clear();
      Tags.Clear();
      if (list != null)
      {
        foreach (string s in list)
        {
          Tags[s] = new EvolutionTag(s);
          Tags[s].CloseClick += DeleteItem;
          ItemPanel.Children.Add(Tags[s]);
        }
      }
    }
    public void ResetAll(Dictionary<string, EvolutionCondition> dict)
    {
      ItemPanel.Children.Clear();
      Tags.Clear();
      if (dict != null)
      {
        foreach (string s in dict.Keys)
        {
          Tags[s] = new EvolutionTag(s);
          Tags[s].Data = dict[s];
          Tags[s].CloseClick += DeleteItem;
          ItemPanel.Children.Add(Tags[s]);
        }
      }
    }
  }
}
