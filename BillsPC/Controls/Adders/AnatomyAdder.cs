﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace BillsPC.Controls
{

  public enum BodyType {
    [Description("Clear")]
    None,
    [Description("Floating Head")]
    Head,
    [Description("Serpent")]
    Serpent,
    [Description("Fish")]
    Fish,
    [Description("Head+Arms")]
    Geodude,
    [Description("Monoped")]
    Monoped,
    [Description("Biped")]
    Biped,
    [Description("Armless")]
    Armless,
    [Description("Quadruped")]
    Quadruped,
    [Description("Winged")]
    Winged,
    [Description("Tentacled")]
    Tentacled,
    [Description("Multi-body")]
    MultiBody,
    [Description("Bug")]
    Bug,
    [Description("Dragon")]
    Dragon,
    [Description("Plant")]
    Plant,
    [Description("Water")]
    Water,
    [Description("Electric")]
    Electric,
    [Description("Fire")]
    Fire,
    [Description("Poison")]
    Poison,
    [Description("Mouth Parts")]
    MouthParts,
  }

  
  public class AnatomyAdderControlPanel : TextAdderControlPanel
  {
    public Button ClearButton
    {
      get { return Buttons[BodyType.None]; }
    }

    public Button CopyButton;
    public Button PasteButton;

    public Dictionary<BodyType, Button> Buttons;

    public WrapPanel ButtonWrap;
    public WrapPanel CopyWrap;

    public AnatomyAdderControlPanel() : base()
    {
      CopyWrap = new WrapPanel() { HorizontalAlignment = HorizontalAlignment.Left, VerticalAlignment = VerticalAlignment.Center };

      CopyButton = new Button() { Content = "Copy", Margin = new Thickness(3), Padding = new Thickness(5, 3, 5, 3) };
      PasteButton = new Button() { Content = "Paste", Margin = new Thickness(3), Padding = new Thickness(5, 3, 5, 3) };
      CopyWrap.Children.Add(CopyButton);
      CopyWrap.Children.Add(PasteButton);

      ButtonWrap = new WrapPanel() { HorizontalAlignment = HorizontalAlignment.Left };
      
      Buttons = new Dictionary<BodyType, Button>();

      foreach(BodyType type in new BodyType().GetValues())
      {
        Button b = new Button() { Content = type.GetDescription(), Margin = new Thickness(3), Padding = new Thickness(5, 3, 5, 3) };
        Buttons[type] = b;
        ButtonWrap.Children.Add(b);
      }

      Buttons[BodyType.None].Foreground = Brushes.Crimson;
      Buttons[BodyType.None].FontWeight = FontWeights.Bold;

      ControlPanel.Children.Add(CopyWrap);
      ControlPanel.Children.Add(ButtonWrap);

      LabelName = "New Body Part:";
    }
  }

  public class AnatomyAdder : GroupAdder<StackPanel, AnatomyAdderControlPanel, AnatomyTag>
  {
    protected static Dictionary<string, AnatomyTag> Clipboard = new Dictionary<string, AnatomyTag>();

    private Dictionary<BodyType, List<Anatomy>> AnatomyLookup;

    public AnatomyAdder() : base()
    {
      ControlPanel = new AnatomyAdderControlPanel();
      ControlPanelStack.AddChildAt(ControlPanel, 0);

      ControlPanel.Add += AddItem;
      ControlPanel.Delete += DeleteItem;
      ControlPanel.Contains = Tags.ContainsKey;

      ControlPanel.CopyButton.Click += CopyClick;
      ControlPanel.PasteButton.Click += PasteClick;

      ControlPanel.ClearButton.Click += PresetClick;

      foreach (BodyType type in new BodyType().GetValues())
      {
        if (type == BodyType.None)
          continue;

        ControlPanel.Buttons[type].Click += PresetClick;
      }

      AnatomyLookup = new Dictionary<BodyType, List<Anatomy>>();

      #region Anatomy group definitions

      AnatomyLookup[BodyType.None] = new List<Anatomy>();
      AnatomyLookup[BodyType.Head] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,ATK, SPATK,HP,END,WEAK", Count = 1, Attributes = "Brain,Center", Attached = "Mouth,Shell" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Shell", Stats = "DEF,SPDEF,HP+,SPD,RES", Count = 1, Attributes = "Move,Stance,Clamp,Durable", Attached = "" }
      };

      AnatomyLookup[BodyType.Serpent] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,SPATK,HP+", Count = 1, Attributes = "Brain", Attached = "Mouth,Horn" },
        new Anatomy() { Name = "Horn", Stats = "ATK,CRIT", Count = 1, Attributes = "Durable", Attached = "" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST", Count = 1, Attributes = "Center,Segmented:6,Grapple,Move", Attached = "Head,Tail" },
        new Anatomy() { Name = "Tail", Stats = "HP,END,ATK,SPD", Count = 1, Attributes = "Grapple,Segmented:3,Move", Attached = "" },
      };

      AnatomyLookup[BodyType.Fish] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,SPATK,HP", Count = 1, Attributes = "Brain", Attached = "Mouth,Horn" },
        new Anatomy() { Name = "Horn", Stats = "ATK,CRIT", Count = 1, Attributes = "Durable", Attached = "" },
        new Anatomy() { Name = "Mouth", Stats = "ATK+,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST", Count = 1, Attributes = "Center,Move", Attached = "Head,Tail,Fin:6" },
        new Anatomy() { Name = "Tail", Stats = "HP,END,ATK,SPD+", Count = 1, Attributes = "Swim,Move", Attached = "Fin:2" },
        new Anatomy() { Name = "Fin", Stats = "SPD,EVA", Count = 8, Attributes = "Swim", Attached = "" }
      };

      AnatomyLookup[BodyType.Geodude] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,ATK, SPATK,HP+,END+,WEAK", Count = 1, Attributes = "Brain,Center,Stance", Attached = "Mouth,Arm:2,Carapace" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Arm", Stats = "HP,END,ATK,DEF", Count = 2, Attributes = "Grasp,Grapple,Segmented:2,Move", Attached = "" },
        new Anatomy() { Name = "Carapace", Stats = "DEF,SPDEF,HP+,SPD,RES", Count = 1, Attributes = "Durable", Attached = "" }
      };

      AnatomyLookup[BodyType.Monoped] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,ATK,SPATK,HP,END", Count = 1, Attributes = "Brain,Center", Attached = "Mouth,Shell" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST,SPD", Count = 1, Attributes = "Center,Stance,Move", Attached = "Head" }
      };

      AnatomyLookup[BodyType.Biped] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,SPATK,HP", Count = 1, Attributes = "Brain", Attached = "Mouth" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST", Count = 1, Attributes = "Center", Attached = "Arm:2,Leg:2,Head,Tail" },
        new Anatomy() { Name = "Leg", Stats = "HP,END,DEF,EVA,SPD", Count = 2, Attributes = "Stance,Move,Grapple,Segmented:2", Attached = "Claw:3" },
        new Anatomy() { Name = "Arm", Stats = "HP,END,ATK,DEF", Count = 2, Attributes = "Grasp,Grapple,Segmented:2", Attached = "Claw:3" },
        new Anatomy() { Name = "Tail", Stats = "HP,END,SPD", Count = 1, Attributes = "Grapple,Segmented:3", Attached = "" },
        new Anatomy() { Name = "Claw", Stats = "ATK,CRIT", Count = 12, Attributes = "Durable", Attached = "" }
      };

      AnatomyLookup[BodyType.Armless] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,SPATK,HP", Count = 1, Attributes = "Brain", Attached = "Mouth" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST", Count = 1, Attributes = "Center", Attached = "Leg:2,Head" },
        new Anatomy() { Name = "Leg", Stats = "HP,END,DEF,EVA,SPD", Count = 2, Attributes = "Stance,Move,Segmented:2", Attached = "" },
      };
      AnatomyLookup[BodyType.Quadruped] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,SPATK,HP", Count = 1, Attributes = "Brain", Attached = "Mouth" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST", Count = 1, Attributes = "Center", Attached = "Leg:4,Head,Tail" },
        new Anatomy() { Name = "Leg", Stats = "HP,END,DEF,EVA,SPD", Count = 4, Attributes = "Move,Stance,Segmented:2", Attached = "Hoof" },
        new Anatomy() { Name = "Tail", Stats = "HP,END,SPD", Count = 1, Attributes = "Grapple,Segmented:3", Attached = "" },
        new Anatomy() { Name = "Hoof", Stats = "ATK,CRIT,SPD", Count = 4, Attributes = "Durable", Attached = "" }
      };

      AnatomyLookup[BodyType.Winged] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,SPATK,HP", Count = 1, Attributes = "Brain", Attached = "Mouth" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "Beak" },
        new Anatomy() { Name = "Beak", Stats = "ATK,CRIT", Count = 1, Attributes = "Durable", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST", Count = 1, Attributes = "Center", Attached = "Wing:2,Leg:2,Head,Tail" },
        new Anatomy() { Name = "Leg", Stats = "HP,END,DEF,EVA,SPD", Count = 2, Attributes = "Stance,Move,Grapple,Segmented:2", Attached = "Talon:4" },
        new Anatomy() { Name = "Wing", Stats = "END,EVA+,SPD+", Count = 2, Attributes = "Flight,Move,Segmented:2", Attached = "" },
        new Anatomy() { Name = "Tail", Stats = "EVA,SPD", Count = 1, Attributes = "", Attached = "" },
        new Anatomy() { Name = "Talon", Stats = "ATK,CRIT", Count = 8, Attributes = "Durable", Attached = "" }
      };

      AnatomyLookup[BodyType.Tentacled] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,SPATK,HP", Count = 1, Attributes = "Brain", Attached = "Mouth" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "Beak" },
        new Anatomy() { Name = "Beak", Stats = "ATK,CRIT", Count = 1, Attributes = "Durable", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST", Count = 1, Attributes = "Center", Attached = "Arm:10,Tentacle:4,Head" },
        new Anatomy() { Name = "Arm", Stats = "HP,END,DEF,EVA,SPD,ATK", Count = 10, Attributes = "Stance,Move,Swim,Grasp,Grapple,Segmented:5", Attached = "" },
        new Anatomy() { Name = "Tentacle", Stats = "HP,END,ATK,DEF,SPD", Count = 4, Attributes = "Move,Swim,Grasp,Grapple,Segmented:8,Regenerates", Attached = "" },
      };

      AnatomyLookup[BodyType.MultiBody] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,ATK,SPATK,HP,END", Count = 1, Attributes = "Brain,Center", Attached = "Mouth,Shell" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST,SPD", Count = 3, Attributes = "Center,Stance,Move", Attached = "Head,Body:2" }
      };

      AnatomyLookup[BodyType.Bug] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,SPATK,HP", Count = 1, Attributes = "Brain", Attached = "Mouth" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "Fang:2" },
        new Anatomy() { Name = "Body", Stats = "HP+,END+,DEF, SPDEF,RES,CONST", Count = 1, Attributes = "Center", Attached = "Leg:6,Head,Spinneret" },
        new Anatomy() { Name = "Leg", Stats = "HP,END,EVA,SPD", Count = 6, Attributes = "Stance,Move,Grapple,Segmented:3", Attached = "" },
        new Anatomy() { Name = "Fang", Stats = "ATK,CRIT", Count = 2, Attributes = "Poison,Durable", Attached = "" },
        new Anatomy() { Name = "Spinneret", Stats = "", Count = 2, Attributes = "Web,Ammo", Attached = "" },
      };
      AnatomyLookup[BodyType.Dragon] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Head", Stats = "ACC,INIT,SPDEF,HP", Count = 1, Attributes = "Brain", Attached = "Horn:2,Mouth" },
        new Anatomy() { Name = "Neck", Stats = "EVA,HP,END,ACC", Count = 1, Attributes = "Segmented:3", Attached = "Head" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "" },
        new Anatomy() { Name = "Body", Stats = "HP,END,DEF,SPDEF", Count = 1, Attributes = "Center", Attached = "Arm:2,Leg:2,Tail,Neck,Wing:2" },
        new Anatomy() { Name = "Wing", Stats = "EVA,END,SPD", Count = 2, Attributes = "Fly", Attached = "Claw:1" },
        new Anatomy() { Name = "Leg", Stats = "HP,END,DEF,EVA", Count = 2, Attributes = "Stance,Grapple,Segmented:2", Attached = "Claw:5," },
        new Anatomy() { Name = "Arm", Stats = "ATK,HP,END,", Count = 2, Attributes = "Grasp,Grapple,Segmented:2", Attached = "Claw:5" },
        new Anatomy() { Name = "Tail", Stats = "ATK,SPATK,SPD,HP,END,", Count = 1, Attributes = "Grapple,Segmented:5", Attached = "" },
        new Anatomy() { Name = "Claw", Stats = "ATK,CRIT", Count = 22, Attributes = "Durable", Attached = "" },
      };

      AnatomyLookup[BodyType.Plant] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Vine", Stats = "ATK,SPATK", Count = 4, Attributes = "Grasp,Grapple,Segmented:5", Attached = "" },
        new Anatomy() { Name = "Root", Stats = "-SPD,DEF,SPDEF, CONST,RES,HP,END,WEAK", Count = 1, Attributes = "Stance,Root,Regenerates", Attached = "" },
        new Anatomy() { Name = "Leaf", Stats = "SPATK,SPDEF,WEAK", Count = 20, Attributes = "Regenerates,Ammo:4", Attached = "" },
        new Anatomy() { Name = "SeedPod", Stats = "SPATK", Count = 1, Attributes = "Internal,Ammo:15", Attached = "" },
        new Anatomy() { Name = "SporeSack", Stats = "SPATK", Count = 2, Attributes = "Internal,Regenerates,Ammo:20", Attached = "" },
      };

      AnatomyLookup[BodyType.Poison] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Valve", Stats = "SPATK", Count = 1, Attributes = "Liquid", Attached = "PoisonGland" },
        new Anatomy() { Name = "PoisonGland", Stats = "SPATK", Count = 2, Attributes = "Internal,Regenerates,Ammo:6", Attached = "" },
        new Anatomy() { Name = "VenomGland", Stats = "SPATK,ATK", Count = 2, Attributes = "Internal,Regenerates,Ammo:4", Attached = "" },
        new Anatomy() { Name = "Fang", Stats = "ATK,CRIT", Count = 2, Attributes = "Poison,Durable", Attached = "VenomGland" },
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "Fang:2" }
      };

      AnatomyLookup[BodyType.Electric] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Bioemitter", Stats = "SPATK,SPD", Count = 2, Attributes = "Discharge", Attached = "Biocapacitor:2" },
        new Anatomy() { Name = "Biocapacitor", Stats = "SPATK,SPD", Count = 2, Attributes = "Internal,Regenerates,Ammo:20", Attached = "" },
        new Anatomy() { Name = "Ground", Stats = "SPDEF", Count = 1, Attributes = "Ground", Attached = "" },
      };

      AnatomyLookup[BodyType.Fire] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Propellant", Stats = "SPATK", Count = 1, Attributes = "Fire", Attached = "OilGland" },
        new Anatomy() { Name = "OilGland", Stats = "SPATK", Count = 1, Attributes = "Internal,Regenerates,Ammo:20", Attached = "" },
      };

      AnatomyLookup[BodyType.Water] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Valve", Stats = "SPATK", Count = 1, Attributes = "Liquid", Attached = "Reservoir:3" },
        new Anatomy() { Name = "Reservoir", Stats = "SPATK", Count = 3, Attributes = "Internal,Regenerates,Ammo:20", Attached = "" },
      };

      AnatomyLookup[BodyType.MouthParts] = new List<Anatomy>()
      {
        new Anatomy() { Name = "Mouth", Stats = "ATK,SPATK,ACC", Count = 1, Attributes = "Bite", Attached = "Beak,Fang:2,Tooth:20" },
        new Anatomy() { Name = "Fang", Stats = "ATK,CRIT", Count = 2, Attributes = "Poison,Durable", Attached = "" },
        new Anatomy() { Name = "Tooth", Stats = "ATK,CRIT", Count = 20, Attributes = "Durable,Removable", Attached = "" },
        new Anatomy() { Name = "Beak", Stats = "ATK,CRIT", Count = 1, Attributes = "Durable", Attached = "" },
      };

      #endregion

    }

    private void PasteClick(object sender, RoutedEventArgs e)
    {
      Paste();
    }

    private void CopyClick(object sender, RoutedEventArgs e)
    {
      Copy();
    }

    public void Copy()
    {
      Clipboard = new Dictionary<string, AnatomyTag>(Tags);
    }

    public void Paste()
    {
      ResetAll(Clipboard);
    }

    public override void AddItem(string s)
    {
      if (string.IsNullOrEmpty(s))
        return;

      if (!Tags.ContainsKey(s))
      {
        Tags[s] = new AnatomyTag(s);
        Tags[s].CloseClick += DeleteItem;
        ItemPanel.Children.Add(Tags[s]);

        InvokeAdded(s);
      }
    }

    public override void AddItem(AnatomyTag part)
    {
      if (part == null)
        return;

      if (!Tags.ContainsKey(part.TagName))
      {
        Tags[part.TagName] = part;
        part.CloseClick += DeleteItem;
        ItemPanel.Children.Add(part);

        InvokeAdded(part.TagName);
      }
    }
    public override void DeleteItem(string s)
    {
      if (string.IsNullOrEmpty(s))
        return;

      if (Tags.ContainsKey(s))
      {
        ItemPanel.Children.Remove(Tags[s]);
        Tags.Remove(s);

        InvokeDeleted(s);
      }
    }
    public override void ResetAll(IEnumerable<string> list)
    {
      ItemPanel.Children.Clear();
      Tags.Clear();
      if (list != null)
      {
        foreach (string s in list)
        {
          Tags[s] = new AnatomyTag(s);
          Tags[s].CloseClick += DeleteItem;
          ItemPanel.Children.Add(Tags[s]);
        }
      }
    }
    public void ResetAll(Dictionary<string, Anatomy> dict)
    {
      ItemPanel.Children.Clear();
      Tags.Clear();
      if (dict != null)
      {
        foreach (string s in dict.Keys)
        {
          Tags[s] = new AnatomyTag(s);
          Tags[s].Data = dict[s];
          Tags[s].CloseClick += DeleteItem;
          ItemPanel.Children.Add(Tags[s]);
        }
      }
    }

    public void ResetAll(Dictionary<string, AnatomyTag> dict)
    {
      ItemPanel.Children.Clear();
      Tags.Clear();
      if (dict != null)
      {
        foreach (string s in dict.Keys)
        {
          Tags[s] = new AnatomyTag(dict[s].Data);
          Tags[s].CloseClick += DeleteItem;
          ItemPanel.Children.Add(Tags[s]);
        }
      }
    }

    public void ClearAll()
    {
      ItemPanel.Children.Clear();
      Tags.Clear();
    }

    public BodyType FindType(Button b)
    {
      foreach(var pair in ControlPanel.Buttons)
      {
        if (pair.Value == b)
          return pair.Key;
      }

      return BodyType.None;
    }

    private void PresetClick(object sender, RoutedEventArgs e)
    {
      var type = FindType(sender as Button);

      if (type == BodyType.None)
        ClearAll();
      else
        AddPresetItems(AnatomyLookup[type]);
    }

    private void AddPresetItems(List<Anatomy> list)
    {
      foreach (var part in list)
      {
        foreach (var child in ItemPanel.Children)
        {
          if (child is AnatomyTag)
          {
            AnatomyTag tag = child as AnatomyTag;
            if (tag.TagName == part.Name)
            {
              tag.Data = part;
              goto found;
            }
          }
        }

        AddItem(new AnatomyTag(part));

        found:
        continue;
      }
    }
  }
}
