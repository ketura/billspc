﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for RowAdder.xaml
  /// </summary>
  public partial class RowAdder : UserControl
  {
    public ItemsControl Grid { get; set; }

    public Action Sort { get; set; }
    public Action DeleteAll { get; set; }

    private ObservableCollection<MoveTag> Items
    {
      get { return Grid.ItemsSource as ObservableCollection<MoveTag>; }
      set { Grid.ItemsSource = value; }
    }

    /// <summary>
    /// The automatic height of generated rows.
    /// Exposed to XAML.
    /// </summary>
    public int RowHeight
    {
      get { return (int)GetValue(RowHeightProperty); }
      set { SetValue(RowHeightProperty, value); }
    }
    public static readonly DependencyProperty RowHeightProperty = DependencyProperty.RegisterAttached("RowHeight",
            typeof(int), typeof(TextAdderControlPanel), new FrameworkPropertyMetadata(40));

    public RowAdder()
    {
      InitializeComponent();
    }

    private void btnSort_Click(object sender, RoutedEventArgs e)
    {
      Sort?.Invoke();
    }

    private void btnDeleteAll_Click(object sender, RoutedEventArgs e)
    {
      DeleteAll?.Invoke();
    }
  }
}
