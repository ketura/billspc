﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BillsPC.Controls
{
  public class MoveAdderControlPanel : TextAdderControlPanel
  {
    public MoveAdderControlPanel() : base()
    {
      LabelName = "New Move:";
    }
  }

  public class MoveAdder : GroupAdder<WrapPanel, MoveAdderControlPanel, MoveTag>
  {
    public MoveAdder() : base()
    {
      ControlPanel = new MoveAdderControlPanel();
      ControlPanelStack.AddChildAt(ControlPanel, 0);

      ControlPanel.Add += AddItem;
      ControlPanel.Delete += DeleteItem;
      ControlPanel.Contains = Tags.ContainsKey;
    }

    public override void AddItem(string s)
    {
      if (string.IsNullOrEmpty(s))
        return;

      if (!Tags.ContainsKey(s))
      {
        Tags[s] = new MoveTag(s);
        Tags[s].CloseClick += DeleteItem;
        ItemPanel.Children.Add(Tags[s]);

        InvokeAdded(s);
      }
    }
    public override void DeleteItem(string s)
    {
      if (string.IsNullOrEmpty(s))
        return;

      if (Tags.ContainsKey(s))
      {
        ItemPanel.Children.Remove(Tags[s]);
        Tags.Remove(s);

        InvokeDeleted(s);
      }
    }
    public override void ResetAll(IEnumerable<string> list)
    {
      ItemPanel.Children.Clear();
      Tags.Clear();
      if (list != null)
      {
        foreach (string s in list)
        {
          Tags[s] = new MoveTag(s);
          Tags[s].CloseClick += DeleteItem;
          ItemPanel.Children.Add(Tags[s]);
        }
      }
    }
    public void ResetAll(Dictionary<string, SpeciesMove> dict)
    {
      ItemPanel.Children.Clear();
      Tags.Clear();
      if (dict != null)
      {
        foreach (string s in dict.Keys)
        {
          Tags[s] = new MoveTag(s);
          Tags[s].Data = dict[s];
          Tags[s].CloseClick += DeleteItem;
          ItemPanel.Children.Add(Tags[s]);
        }
      }
    }
  }
}

