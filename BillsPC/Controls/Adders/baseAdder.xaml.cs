﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{

  //Each "adder" is comprised of four parts: the control panel, the manager, the tag display,
  // and the tags themselves.  The control panel is the add/delete buttons with some sort of 
  // user-definable control.  The manager keeps a list of all the currently spawned tags and 
  // handles adding/deletion and checking for uniqueness.  The tag display is mostly just a 
  // panel that holds the tags.  And the tags themselves are collections of controls that are
  // tied directly to some base object

  
  public abstract class GroupAdder<TPanel, TControlPanel, TTag> : BaseAdder
    where TPanel : Panel, new()
    where TControlPanel : BaseAdderControlPanel
    where TTag : TagWrapper
  {
    public Dictionary<string, TTag> Tags { get; protected set; }
    public TPanel ItemPanel { get; protected set; }
    public TControlPanel ControlPanel { get; protected set; }

    public virtual void AddItem(TTag tag) { }

    public GroupAdder() : base()
    {
      Tags = new Dictionary<string, TTag>();
      ItemPanel = new TPanel();
      ControlPanelStack.AddChildAt(ItemPanel, 0);
    }
  }

  /// <summary>
  /// Interaction logic for Adder.xaml
  /// </summary>
  public partial class BaseAdder : UserControl
  {
    //list or dictionary to the items that have been added?


    /// <summary>
    /// The text to put on the add button's face.
    /// Exposed to XAML.
    /// </summary>
    public string AddLabel
    {
      get { return (string)GetValue(AddLabelProperty); }
      set { SetValue(AddLabelProperty, value); }
    }
    public static readonly DependencyProperty AddLabelProperty = DependencyProperty.RegisterAttached("AddLabel",
            typeof(string), typeof(BaseAdder), new FrameworkPropertyMetadata("Add"));

    /// <summary>
    /// The text to put on the delete button's face.
    /// Exposed to XAML.
    /// </summary>
    public string DeleteLabel
    {
      get { return (string)GetValue(DeleteLabelProperty); }
      set { SetValue(DeleteLabelProperty, value); }
    }
    public static readonly DependencyProperty DeleteLabelProperty = DependencyProperty.RegisterAttached("DeleteLabel",
            typeof(string), typeof(BaseAdder), new FrameworkPropertyMetadata("Delete"));
    private static void OnDeleteLabelPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      BaseAdder ba = d as BaseAdder;
      string s = e.NewValue as string;
      ba.LabelNameChanged(s);
    }
    protected virtual void DeleteLabelChanged(string name) { }


    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }

    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(BaseAdder), new PropertyMetadata("Label Text", OnLabelNamePropertyChanged));

    private static void OnLabelNamePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      BaseAdder ba = d as BaseAdder;
      string s = e.NewValue as string;
      ba.LabelNameChanged(s);
    }
    protected virtual void LabelNameChanged(string name) { }

    public virtual void AddItem(string s) { }
    public virtual void DeleteItem(string s) { }
    public virtual void ResetAll(IEnumerable<string> list) { }

    public event Action<string> ItemAdded;
    public event Action<string> ItemDeleted;
    public event Action<string> ItemChanged;

    protected void InvokeAdded(string s) { ItemAdded?.Invoke(s); }
    protected void InvokeDeleted(string s) { ItemDeleted?.Invoke(s); }
    protected void InvokeChanged(string s) { ItemChanged?.Invoke(s); }

    public BaseAdder()
    {
      InitializeComponent();
    }
  }
}
