﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  public delegate bool CheckContent(string str);

  //TControl is text box or combobox or some other means of user name selection
  public abstract class AdderControlPanel<TControl> : BaseAdderControlPanel
    where TControl : UIElement, new()
  {
    public TControl Selector { get; protected set; }

    public abstract void AddItem();
    public abstract void RemoveItem();
       

    public AdderControlPanel(TControl t) : base()
    {
      Selector = t;
      Init();
    }

    public AdderControlPanel() : base()
    {
      Selector = new TControl();
      Init();
    }

    private void Init()
    {
      InsertSelector(Selector);
      btnAdd.IsEnabled = false;
      btnDelete.IsEnabled = false;
    }

    protected override void btnAdd_Click(object sender, RoutedEventArgs e)
    {
      AddItem();
    }
    protected override void btnDelete_Click(object sender, RoutedEventArgs e)
    {
      if (ConfirmDelete)
      {
        if (MessageBoxResult.No == MessageBox.Show(DeletionMessage, "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation))
        {
          return;
        }
      }

      RemoveItem();
    }

    protected override void DeleteLabelChanged(string name) { btnDelete.Content = name; }
  }

  /// <summary>
  /// Interaction logic for AdderControlPanel.xaml
  /// </summary>
  public partial class BaseAdderControlPanel : UserControl
  {
    #region XAML Crap
    public static readonly DependencyProperty SelectorWidthProperty = DependencyProperty.RegisterAttached("SelectorWidth",
            typeof(int), typeof(BaseAdderControlPanel), new FrameworkPropertyMetadata(120, OnSelectorWidthPropertyChanged));
    public int SelectorWidth
    {
      get { return (int)GetValue(SelectorWidthProperty); }
      set { SetValue(SelectorWidthProperty, value); }
    }
    private static void OnSelectorWidthPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      BaseAdderControlPanel bacp = d as BaseAdderControlPanel;
      int? i = e.NewValue as int?;
      bacp.SelectorWidthChanged(i);
    }
    protected virtual void SelectorWidthChanged(int? width) { }


    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(BaseAdderControlPanel), new PropertyMetadata("Label Text"));
    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }
    private static void OnLabelNamePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      BaseAdderControlPanel bacp = d as BaseAdderControlPanel;
      string s = e.NewValue as string;
      bacp.LabelNameChanged(s);
    }
    protected virtual void LabelNameChanged(string name) { }


    public string AddLabel
    {
      get { return (string)GetValue(AddLabelProperty); }
      set { SetValue(AddLabelProperty, value); }
    }
    public static readonly DependencyProperty AddLabelProperty = DependencyProperty.RegisterAttached("AddLabel",
            typeof(string), typeof(BaseAdderControlPanel), new FrameworkPropertyMetadata("Add"));
    private static void OnAddLabelPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      BaseAdderControlPanel bacp = d as BaseAdderControlPanel;
      string s = e.NewValue as string;
      bacp.AddLabelChanged(s);
    }
    protected virtual void AddLabelChanged(string name) { btnAdd.Content = name; }

    /// <summary>
    /// The text to put on the delete button's face.
    /// Exposed to XAML.
    /// </summary>
    public string DeleteLabel
    {
      get { return (string)GetValue(DeleteLabelProperty); }
      set { SetValue(DeleteLabelProperty, value); }
    }
    public static readonly DependencyProperty DeleteLabelProperty = DependencyProperty.RegisterAttached("DeleteLabel",
            typeof(string), typeof(BaseAdderControlPanel), new FrameworkPropertyMetadata("Delete"));
    private static void OnDeleteLabelPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      BaseAdderControlPanel bacp = d as BaseAdderControlPanel;
      string s = e.NewValue as string;
      bacp.DeleteLabelChanged(s);
    }
    protected virtual void DeleteLabelChanged(string name) { btnDelete.Content = name; }

    #endregion


    public event Action<string> Add;
    public event Action<string> Delete;
    public CheckContent Contains;

    public bool ConfirmDelete { get; set; }

    public string DeletionMessage { get; set; }

    public BaseAdderControlPanel()
    {
      InitializeComponent();
    }

    protected void InsertSelector(UIElement control)
    {
      ControlPanel.AddChildAt(control, 1);
    }

    protected void InvokeAdd(string s)
    {
      Add?.Invoke(s);
    }
    protected void InvokeDelete(string s)
    {
      Delete?.Invoke(s);
    }

    public virtual void Display(string s) { }

    protected virtual void btnAdd_Click(object sender, RoutedEventArgs e) { }
    protected virtual void btnDelete_Click(object sender, RoutedEventArgs e) { }
  }
}
