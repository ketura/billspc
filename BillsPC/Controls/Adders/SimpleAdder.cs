﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BillsPC.Controls
{

  public class SimpleTextAdder : SimpleAdder<TextAdderControlPanel>
  {
    public SimpleTextAdder() : base() { }
  }

  public class SimpleAdder<T> : BaseAdder
    where T : BaseAdderControlPanel, new()
  {
    

    public bool AutoSwitch { get; set; }
    public bool AutoFill { get; set; }
    public ComboBox Picker { get; protected set; }
    public ObservableCollection<object> Content { get; protected set; }
    public T ControlPanel { get; protected set; }

    private bool Working { get; set; }
    public SimpleAdder() : base()
    {
      AutoSwitch = true;
      AutoFill = true;

      Content = new ObservableCollection<object>();

      Picker = new ComboBox()
      {
        HorizontalAlignment = HorizontalAlignment.Left,
        VerticalAlignment = VerticalAlignment.Center,
        Margin = new Thickness(5),
        Width = 150,
      };
      Picker.SetValue(VirtualizingStackPanel.IsVirtualizingProperty, true);
      Picker.SetValue(VirtualizingStackPanel.VirtualizationModeProperty, VirtualizationMode.Recycling);
      Picker.SelectionChanged += Picker_SelectionChanged;
      Picker.ItemsSource = Content;
      ControlPanelStack.Children.Add(Picker);
      ControlPanelStack.Orientation = Orientation.Horizontal;

      ControlPanel = new T();
      ControlPanel.Add += AddItem;
      ControlPanel.Delete += DeleteItem;
      ControlPanel.Contains = Picker.Items.Contains;
      ControlPanel.ConfirmDelete = true;
      ControlPanel.DeletionMessage = "Are you sure you want to delete this item?  Unsaved changes will be lost.";
      ControlPanelStack.Children.Add(ControlPanel);
    }

    private void Picker_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (AutoFill)
        ControlPanel.Display(Picker.SelectedItem as string);

      if (!Working)
        InvokeChanged(Picker.SelectedItem as string);
    }

    public override void AddItem(string s)
    {
      if (string.IsNullOrEmpty(s))
        return;

      if (!Content.Contains(s))
      {
        Content.Add(s);

        InvokeAdded(s);

        if (AutoSwitch)
          Picker.SelectedItem = s;
      }
    }

    public void SwitchItem(string s)
    {
      if(Content.Contains(s))
      {
        Picker.SelectedItem = s;
      }
    }

    public override void DeleteItem(string s)
    {
      if (string.IsNullOrEmpty(s))
        return;

      if (Content.Contains(s))
      {
        if (AutoSwitch)
        {
          if ((string)Picker.SelectedItem == s)
          {
            Picker.SelectedIndex -= 1;
          }
        }

        Content.Remove(s);

        if (AutoSwitch)
        {
          if (Picker.SelectedIndex == -1 && Content.Count > 0)
          {
            Picker.SelectedIndex += 1;
          }
        }

        InvokeDeleted(s);
      }
    }

    public override void ResetAll(IEnumerable<string> list)
    {
      ResetAll(list);
    }

    public void ResetAll(IEnumerable<object> list=null)
    {
      Working = true;
      if (list == null)
      {
        Content.Clear();
      }
      else
      {
        Content = new ObservableCollection<object>(list);
        Picker.ItemsSource = Content;
      }

      if (Content.Count() > 0 && Picker.SelectedIndex == -1)
        Picker.SelectedIndex = 0;

      Working = false;
    }

    public void ResetContent(ObservableCollection<object> content)
    {
      Working = true;
      Content = content;
      Picker.ItemsSource = Content;

      if (Content.Count() > 0 && Picker.SelectedIndex == -1)
        Picker.SelectedIndex = 0;

      Working = false;
    }

    protected override void LabelNameChanged(string name)
    {
      ControlPanel.LabelName = name;
    }

  }

}
