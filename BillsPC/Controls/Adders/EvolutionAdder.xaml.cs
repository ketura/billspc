﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for EvolutionAdder.xaml
  /// </summary>
  public partial class EvolutionAdder : UserControl
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public TextAdderControlPanel EvolutionType { get; protected set; }

    public ObservableCollection<string> Pokemon { get; protected set; }

    public ControlArray<string, EvolutionTag> Evolutions { get; protected set; }

    public int ItemWidth
    {
      get { return Evolutions.ItemWidth; }
      set { Evolutions.ItemWidth = value; }
    }

    public void ResetPokemon(List<string> newlist)
    {
      Pokemon.Clear();
      Pokemon.Add("None");
      foreach (string str in newlist)
      {
        Pokemon.Add(str);
      }
    }

    public void AddPokemonToPool(string name)
    {
      Pokemon.Add(name);
    }

    public void DeletePokemonFromPool(string name)
    {
      Pokemon.Remove(name);
    }

    public void AddEvolution(string name)
    {
      Evolutions[name] = new EvolutionTag(name);
      Evolutions[name].CloseClick += RemoveEvolution;
      
    }

    public void RemoveEvolution(string name)
    {
      if (Evolutions.Contains(name))
        Evolutions.Remove(name);
    }

    public void SyncEvolutions(Dictionary<string, EvolutionCondition> evolutions)
    {
      Evolutions.Clear();
      foreach(string key in evolutions.Keys)
      {
        AddEvolution(key);
        Evolutions[key].Data = evolutions[key];
      }
    }

    public void Select(string name)
    {
      if(Pokemon.Contains(name) || name == "None")
      {
        cbEvolvesFrom.SelectedItem = name;
      }
    }

    public EvolutionAdder()
    {
      InitializeComponent();

      Pokemon = new ObservableCollection<string>();
      Pokemon.Add("None");
      cbEvolvesFrom.ItemsSource = Pokemon;

      Evolutions = new ControlArray<string, EvolutionTag>("", null, false) { LabelName = "" };
      EvolutionGrid.Children.Add(Evolutions);
      Grid.SetRow(Evolutions, 1);
      ItemWidth = 680;

      EvolutionType = new TextAdderControlPanel() { LabelName = "Evolves to:", AddLabel = "Add", DeleteLabel = "Remove" };
      EvolutionType.Contains = Evolutions.Contains;
      EvolutionType.Add += AddEvolution;
      EvolutionType.Delete += RemoveEvolution;
      EvolutionPanel.Children.Add(EvolutionType);

    }
  }
}
