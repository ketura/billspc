﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BillsPC;

namespace BillsPC.Controls
{
  /// <summary>
  /// Interaction logic for SizeGrid.xaml
  /// </summary>
  public partial class SizeGrid : UserControl
  {
    public int Height
    {
      get { return txtHeight.Value; }
      set { txtHeight.Value = value; }
    }

    public int Weight
    {
      get { return txtWeight.Value; }
      set { txtWeight.Value = value; }
    }

    public float HeightIV
    {
      get { return txtHeightIV.Value; }
      set { txtHeightIV.Value = value; }
    }

    public float WeightIV
    {
      get { return txtWeightIV.Value; }
      set { txtWeightIV.Value = value; }
    }

    public float HeightTileFactor
    {
      get { return TileFactors.Controls["HeightTileFactor"].Value; }
      set { TileFactors.Controls["HeightTileFactor"].Value = value; }
    }

    public float WeightTileFactor
    {
      get { return TileFactors.Controls["WeightTileFactor"].Value; }
      set { TileFactors.Controls["WeightTileFactor"].Value = value; }
    }

    public int HeightGrowth
    {
      get { return txtHeightGrowth.Value; }
      set { txtHeightGrowth.Value = value; }
    }

    public int WeightGrowth
    {
      get { return txtWeightGrowth.Value; }
      set { txtWeightGrowth.Value = value; }
    }

    public HeightType HeightType
    {
      get { return (HeightType)(cbHeightType.SelectedItem); }
      set { cbHeightType.SelectedItem = value; }
    }

    public float TileSize
    {
      get { return txtTileSize.Value; }
      set { txtTileSize.Value = value; }
    }

    public Brush ValidColor { get; set; }
    public Brush InvalidColor { get; set; }

    public bool ManualOverride
    {
      get { return cbOverride.IsChecked.Value; }
      set { cbOverride.IsChecked = value; }
    }

    private bool initialized = false;

    public SizeGrid()
    {
      InitializeComponent();

      TileFactors.AutoSort = false;

      cbHeightType.Picker.ItemsSource = new HeightType().GetValues();
      cbHeightType.SelectedItem = HeightType.Height;
      cbHeightType.PickerWidth = 80;

      ValidColor = new SolidColorBrush(Brushes.Yellow.Color) { Opacity = 0.75 };
      InvalidColor = new SolidColorBrush(Brushes.Crimson.Color) { Opacity = 0.9 };
      initialized = true;
    }

    private void SizeModified(object sender, TextChangedEventArgs e)
    {
      if(initialized)
        RecalculateSize();
    }

    private void cbHeightType_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      RecalculateSize();
    }

    public void RecalculateSize()
    {
      double fatsize = txtWeight.Value / 100.0f + 4;
      //0.9196KG/L = 0.001 cubic meter
      double tallsize = txtHeight.Value / 100.0f;

      //r^3 = V/(4/3*pi)
      switch (HeightType)
      {
        case HeightType.Height:
        default:
          tallsize *= 3;
          break;
        case HeightType.Shoulder:
          tallsize *= 6;
          break;
        case HeightType.Length:
          tallsize *= 9;
          break;
        case HeightType.Wingspan:
          tallsize *= 4;
          break;
      }

      double finalsize = ((fatsize * WeightTileFactor) + (tallsize * HeightTileFactor)) *10;

      if(ManualOverride)
        UpdateTileSize(finalsize);
      else
        txtTileSize.Value = (float)(finalsize/50.0f);


    }

    private void UpdateTileSize(double amount)
    {
      if (amount > 200)
        HexGrid.HighlightBrush = InvalidColor;
      else
        HexGrid.HighlightBrush = ValidColor;

      HexGrid.Size = Math.Min(200, amount );
      
    }

    private void cbOverride_Checked(object sender, RoutedEventArgs e)
    {
      txtTileSize.IsEnabled = true;
    }

    private void cbOverride_Unchecked(object sender, RoutedEventArgs e)
    {
      txtTileSize.IsEnabled = false;
      RecalculateSize();
    }

    private void txtTileSize_TextChanged(object sender, TextChangedEventArgs e)
    {
      UpdateTileSize( float.Parse(txtTileSize.Text) * 50.0f );
    }
  }
}
