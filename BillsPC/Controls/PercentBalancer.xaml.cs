﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  public class FreePercentBalancer : PercentBalancer
  {
    

    public FreePercentBalancer() : base() { }

    public void AddBoxTo(Panel parent, string name, float amount = 0)
    {
      AddBox(name, amount);
      PercentFields.Remove(Controls[name]);
      parent.Children.Add(Controls[name]);
    }
  }
  /// <summary>
  /// Interaction logic for PercentBalancer.xaml
  /// </summary>
  public partial class PercentBalancer : UserControl
  {
    public event TextChangedEventHandler TextChanged;

    public bool Balancing { get; set; }

    public PercentTextLabel BiasControl { get; protected set; }

    public Dictionary<string, PercentTextLabel> Controls { get; protected set; }

    public Dictionary<PercentTextLabel, float> Values { get; protected set; }

    public int Count { get { return PercentFields.Count; } }

    public bool Contains(string str)
    {
      return Controls.ContainsKey(GetName(str));
    }

    public void SetBias(string str)
    {
      BiasControl = this[str];
    }

    public PercentTextLabel this[string name]
    {
      get
      {
        name = GetName(name);
        return Controls.ContainsKey(name) ? Controls[name] : null;
      }
      set { Controls[GetName(Name)] = value; }
    }

    protected ObservableCollection<PercentTextLabel> PercentFields;
    
    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(PercentBalancer), new PropertyMetadata(null, OnLabelNamePropertyChanged));

    //This is necessary if binding is actually wanted to child controls. For some reason.
    private static void OnLabelNamePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      PercentBalancer pb = d as PercentBalancer;
      string str = e.NewValue as string;
      pb.Resize(str);
    }

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }

    public bool AutoSort { get; set; }

    public PercentBalancer()
    {
      Balancing = true;
      InitializeComponent();
      AutoSort = true;
      (this.Content as FrameworkElement).DataContext = this;

      BiasControl = null;

      Resize(LabelName);
      Balancing = false;
    }

    public void Resize(string names)
    {
      Controls = new Dictionary<string, PercentTextLabel>();
      PercentFields = new ObservableCollection<PercentTextLabel>();
      Values = new Dictionary<PercentTextLabel, float>();

      if (string.IsNullOrEmpty(names))
        return;

      foreach (string s in names.Split('|'))
      {
        AddBox(s, 0);
      }

      //initialize the amounts on the face of things

      float amount = 1.0f / Count;
      Balancing = true;
      foreach (PercentTextLabel control in PercentFields)
      {
        control.Value = amount;
        Values[control] = amount;
      }

      PercentPanel.ItemsSource = PercentFields;

      Balancing = false;
    }

    public static string GetName(string name)
    {
      Regex regex = new Regex("[^a-zA-Z0-9_]");
      return regex.Replace(name, "");
    }

    public virtual void AddBox(string name, float amount=0)
    {
      string newName = GetName(name);

      if (Controls.ContainsKey(newName))
      {
        Controls[newName].Value = amount;
        return;
      }

      Controls[newName] = new PercentTextLabel();
      Controls[newName].LabelName = name;
      Controls[newName].MaxValue = 1.0f;
      Controls[newName].Name = newName;

      Values[Controls[newName]] = amount;
      Controls[newName].Value = amount;

      PercentFields.Add(Controls[newName]);

      Controls[newName].TextChanged += TextBoxChanged;
      if(Controls.Count == 1)
      {
        Controls[newName].Value = 1.0f;
      }
      SortBoxes();
    }

    public void SortBoxes()
    {
      var temp = PercentFields.OrderBy(i => i.Value);
      
      PercentFields = new ObservableCollection<PercentTextLabel>(temp.Reverse());
      if (BiasControl != null)
      {
        PercentFields.Remove(BiasControl);
        PercentFields.Insert(0, BiasControl);
      }
      PercentPanel.ItemsSource = PercentFields;
    }

    protected void ChangeInPlace(PercentTextLabel text, float amount)
    {
      Balancing = true;
      text.Value = amount;
      Balancing = false;
    }

    public virtual void RemoveBox(string name)
    {
      name = GetName(name);
      if (!Controls.ContainsKey(name))
        return;

      Controls[name].Value = 0;

      if (PercentFields.Contains(Controls[name]))
      {
        PercentFields.Remove(Controls[name]);
      }
      Controls.Remove(name);
    }

    public void ClearAllButBias()
    {
      var dict = Controls.ToDictionary(entry => entry.Key, entry => entry.Value);
      foreach (var key in dict.Keys)
      {
        if(dict[key] != BiasControl)
        {
          RemoveBox(key);
        }
      }
    }

    private void TextBoxChanged(object sender, TextChangedEventArgs e)
    {
      // see http://www.sangakoo.com/en/unit/proportional-distributions-direct-and-inverse for this algorithm
      // (or the math behind it, anyway)
      if (!Balancing)
      {
        Balancing = true;
        PercentTextLabel _sender = sender as PercentTextLabel;

        float target = Math.Max(_sender.Value, -1.0f);
        target = Math.Min(target, 1.0f);

        float difference = Values[_sender] - target;
        difference = Math.Min(difference, 1.0f);
        difference = Math.Max(difference, -1.0f);

        //now that we have our target set, attempt to take/add them to the bias control
        if (BiasControl != null && _sender != BiasControl)
        {
          float compare = 0;
          if (difference < 0)
            compare = BiasControl.Value;
          else
            compare = (1.0f - BiasControl.Value) * -1;

          if (Math.Abs(compare) >= Math.Abs(difference))
          {
            BiasControl.Value += difference;
            Values[BiasControl] = BiasControl.Value;
            Values[_sender] = target;
            SortBoxes();
            Balancing = false;
            return;
          }
          else
          {
            BiasControl.Value -= compare;
            Values[BiasControl] = BiasControl.Value;
            difference += compare;
          }
        }

        float total = 0;

        foreach (var control in PercentFields)
        {
          if (control != _sender)
          {
            total += control.Value;
          }
        }

        total = Math.Min(total, 1.0f);

        float newtotal = 0;

        foreach (var control in PercentFields)
        {
          if (control != _sender)
          {
            if (total == 0)
            {
              //see about load balancing when the difference doesn't evenly divide
              control.Value = difference / (Count - 1);
            }
            else
            {
              control.Value += (difference / total) * control.Value;
            }

            Values[control] = control.Value;
            newtotal += control.Value;
          }
          else
          {
            Values[control] = target;
          }
          
        }

        //So apparently very small adjustments of ~1-2% lead to permanent desynching.  This is going to require a more robust rewrite.
        //bool discrepency = !NearlyEqual(total + difference, newtotal);

        if(AutoSort)
          SortBoxes();

        TextChanged?.Invoke(this, e);

        Balancing = false;
      }
      
    }


    public static bool NearlyEqual(double a, double b, double epsilon=0.00001)
    {
      double absA = Math.Abs(a);
      double absB = Math.Abs(b);
      double diff = Math.Abs(a - b);

      if (a == b)
      { // shortcut, handles infinities
        return true;
      }
      else if (a == 0 || b == 0 || diff < Double.Epsilon)
      {
        // a or b is zero or both are extremely close to it
        // relative error is less meaningful here
        return diff < epsilon;
      }
      else
      { // use relative error
        return diff / (absA + absB) < epsilon;
      }
    }
  }

}
