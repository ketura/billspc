﻿using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  
  /// <summary>
  /// Interaction logic for ChartComparison.xaml
  /// </summary>
  public partial class ChartComparison : Window
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public Action<string> OnClose { get; set; }

    protected override void OnClosed(EventArgs e)
    {
      base.OnClosed(e);
      OnClose?.Invoke(Title);
    }

    public SeriesCollection SeriesCollection { get; set; }
    public Func<double, string> YFormatter { get; set; }
    public Func<double, string> XFormatter { get; set; }
    public SpeciesDefinition Pokemon { get; protected set; }
    public bool LogScale
    {
      get { return (bool)GetValue(LogScaleProperty); }
      set { SetValue(LogScaleProperty, value); }
    }

    public static readonly DependencyProperty LogScaleProperty = DependencyProperty.Register("LogScale", typeof(bool),
          typeof(ChartComparison), new UIPropertyMetadata(false, OnLogScalePropertyChanged));

    private static void OnLogScalePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      ChartComparison cc = d as ChartComparison;
      bool? b = e.NewValue as bool?;
      cc.LogScaleChanged(b);
    }
    protected void LogScaleChanged(bool? width)
    {
      Refresh();
    }

    public Stat CurrentStat
    {
      get
      {
        return (Stat)StatPicker.Picker.SelectedItem ;
      }
      set
      {
        StatPicker.Picker.SelectedItem = value;
      }
    }

    private Dictionary<int, LineSeries> ExampleCurves;

    protected ExamplePokemonCollection ExamplePokemon { get; set; }

    protected Dictionary<int, ChartValues<ObservablePoint>> Values { get; set; }

    public ChartValues<ObservablePoint> Max
    {
      get
      {
        return Values[0];
      }
    }

    public ChartValues<ObservablePoint> Min
    {
      get
      {
        return Values[1];
      }
    }

    public int LevelSpacing { get; set; }
    public int MaxPokemon { get; set; }

    private int _pointInterval = 20;

    public ChartComparison(SpeciesDefinition pokemon, Dictionary<string, SpeciesDefinition> examples=null)
    {
      InitializeComponent();

      txtMaxExamples.Value = WindowCoordinator.Instance.CurrentSettings.DefaultMaxCurves;
      txtLevelOverlap.Value = WindowCoordinator.Instance.CurrentSettings.DefaultCurveSpacing;

      Pokemon = pokemon;
      Title = pokemon.Name;

      Values = new Dictionary<int, ChartValues<ObservablePoint>>();
      for (int x = 0; x < 2; x++)
      {
        ChartValues<ObservablePoint> value = new ChartValues<ObservablePoint>();
        for (int j = 0; j <= 100; j += 10)
        {
          value.Add(new ObservablePoint(j, 0));
        }
        Values[x] = value;
      }

      #region SeriesCollection definition
      SeriesCollection = new SeriesCollection(Mappers.Xy<ObservablePoint>()
                .Y(point => point.Y == 0 ? 0 : LogScale ? Math.Log(point.Y) : point.Y)
                .X(point => point.X))
      {
        new LineSeries
        {
            Title = "Max",
            PointGeometrySize = 4,
            Values = Values[0],
            FontFamily = new FontFamily("Segoe UI"),
            FontWeight = FontWeights.Bold,
            Foreground = System.Windows.Media.Brushes.Black,
            DataLabels = true,
            LabelPoint = point => (LogScale ? Math.Pow(Math.E, point.Y).ToString("0") : point.Y.ToString("0"))
        },
        new LineSeries
        {
            Title = "Min",
            PointGeometrySize = 4,
            Values = Values[1],
            FontFamily = new FontFamily("Segoe UI"),
            DataLabels = true,
            LabelPoint = point => (LogScale ? Math.Pow(Math.E, point.Y).ToString("0") : point.Y.ToString("0")),
            //This covers up max's fill below the red line, albeit at the cost of obscuring the axis dividers.
            Fill = new SolidColorBrush(Colors.White) { Opacity = 0.75 }
        }
      };

      #endregion

      Chart.DisableAnimations = !WindowCoordinator.Instance.CurrentSettings.AnimationsEnabled;
      Chart.AnimationsSpeed = TimeSpan.FromMilliseconds(200);
      Chart.DataTooltip = null;

      YFormatter = y => LogScale ? Math.Pow(Math.E, y).ToString("0") : y.ToString("0");
      XFormatter = value => value.ToString("0");

      ExamplePokemon = new ExamplePokemonCollection(pokemon.Name, examples);
      ExampleCurves = new Dictionary<int, LineSeries>();
      

      int i = 2;

      foreach (var pair in ExamplePokemon.Examples)
      {
        ChartValues<ObservablePoint> value = new ChartValues<ObservablePoint>();
        for (int j = 0; j <= 100; j += 10)
        {
          //We only want five points, but the log graph is skewed if we skip level 10, so we'll settle for six uneven points
          if(j == 10 || j % _pointInterval == 0)
            value.Add(new ObservablePoint(j, 0));
        }
        Values[i] = value;

        var curve = new LineSeries
        {
          Title = "Example",
          PointGeometry = null,
          Fill = Brushes.Transparent,
          Stroke = SystemColors.ControlLightBrush,
          Foreground = SystemColors.GrayTextBrush,
          Values = Values[i],
          DataLabels = true,
          FontFamily = new FontFamily("Segoe UI"),
          LabelPoint = DisplayTitle(i)
        };

        SeriesCollection.Add(curve);

        i++;
      }

      StatPicker.Picker.ItemsSource = new Stat().GetValues();
      StatPicker.SelectionChanged += StatPicker_SelectionChanged;
      StatPicker.Picker.SelectedIndex = 0;

      LogScale = true;

      UpdateExamples();
      DataContext = this;
    }

    private Func<ChartPoint, string> DisplayTitle(int num)
    {
      int interval = Values[num].Count - 2;
      int thresh = (100 / interval) * (interval / 2);
      return point =>
      {
        if (point.X > thresh)
          return "";

        if (point.Y > YAxis.MaxValue)
          return "";

        if(point.X == thresh)
          return ((LineSeries)SeriesCollection[num]).Title;

        var list = Values[num].GetPoints(SeriesCollection[num]).OrderBy(p => p.X);

        if (list.Skip(list.IndexOf(point) + 1).First().Y < YAxis.MaxValue)
          return "";

        return ((LineSeries)SeriesCollection[num]).Title;
      };
    }

    private void StatPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      Refresh();
      Chart.AxisY[0].Title = CurrentStat.GetDescription();
    }

    protected void UpdateCurrentPokemon()
    {
      if (Pokemon == null)
        return; 

      int stat = Pokemon.BaseStats[CurrentStat];
      float iv = Pokemon.StatVariance[CurrentStat];

      for (int i = 0; i <= 10; i ++)
      {

        Values[0][i].Y = StatGrowthFormulas.GetCumulativeSum(Pokemon.BaseStatGrowth, (int)Math.Floor(Values[0][i].X), stat * (1 + iv/2));
        Values[1][i].Y = StatGrowthFormulas.GetCumulativeSum(Pokemon.BaseStatGrowth, (int)Math.Floor(Values[0][i].X), stat * (1 - iv/2));
      }

      if(LogScale)
      {
        if(stat > 0)
        {
          YAxis.MaxValue = Math.Log(Values[0].Max(x => x.Y) * 1.1);
          var min = Values[0].Min(x => x.X >= 10 ? x.Y : float.MaxValue);
          YAxis.MinValue = min == 0 ? 0 : Math.Log(min);
        }
        else
        {
          YAxis.MaxValue = Math.Log(50);
          YAxis.MinValue = 0;
        }

        XAxis.MinValue = 10;
      }
      else
      {
        if (stat > 0)
        {
          YAxis.MaxValue = Values[0].Max(x => x.Y) * 1.1;
          YAxis.MinValue = Values[0].Min(x => x.Y);
        }
        else
        {
          YAxis.MaxValue = 50;
          YAxis.MinValue = 0;
        }

        XAxis.MinValue = 0;
      }
    }

    protected void UpdateExamples()
    {
      var pokemon = ParedThresholds(CurrentStat);

      for(int i = 2; i < Values.Count; i++)
      {
        
        if (i-2 < pokemon.Count)
        {
          var pair = pokemon.ElementAt(i-2);
          int stat = pair.Value.BaseStats[CurrentStat];

          ((LineSeries)SeriesCollection[i]).Title = pair.Value.Name;
          for (int j = 0; j < Values[i].Count; j++)
          {
            ((LineSeries)SeriesCollection[i]).Visibility = Visibility.Visible;
            Values[i][j].Y = StatGrowthFormulas.GetCumulativeSum(pair.Value.BaseStatGrowth, (int)Math.Floor(Values[i][j].X), stat);
          }
        }
        else
        {
          ((LineSeries)SeriesCollection[i]).Visibility = Visibility.Hidden;
          ((LineSeries)SeriesCollection[i]).Title = "";

          for (int j = 0; j < Values[i].Count; j++)
          {
            Values[i][j].Y = 0;
          }
        }
      }
    }

    private SortedDictionary<int, SpeciesDefinition> ParedThresholds(Stat statType)
    {
      var collapsed = new SortedDictionary<int, SpeciesDefinition>(new DescendingComparer<int>());
      var dict = new SortedDictionary<int, SpeciesDefinition>(new DescendingComparer<int>());
      var thresholds = ExamplePokemon.Thresholds[statType];

      int prevStat = -1;

      foreach (var pokemon in thresholds)
      {
        int stat = pokemon.Value.BaseStats[CurrentStat];
        if ((prevStat == -1 || prevStat - stat > LevelSpacing) && stat != 0)
        {
          collapsed[pokemon.Key] = pokemon.Value;
          prevStat = stat;
        }
      }

      if (MaxPokemon >= collapsed.Count)
        return collapsed;
      
      if(MaxPokemon >= 1)
      {
        var pair = collapsed.First();
        dict[pair.Key] = pair.Value;
        //thresholds.Remove(pair.Key);
      }

      if(MaxPokemon >= 2)
      {
        var pair = collapsed.Last();
        dict[pair.Key] = pair.Value;
        //thresholds.Remove(pair.Key);
      }

      if (MaxPokemon >= 3)
      {
        double spacing = (collapsed.Count - 2.0) / (MaxPokemon - 1.0);
        for(int i = 1; i <= MaxPokemon - 2; i++)
        {
          var pair = collapsed.ElementAt((int)(spacing*i));
          dict[pair.Key] = pair.Value;
        }
      }

      return dict;
    }

    public void StatUpdated(Stat stat, int amount, float ivSpread)
    {
      Pokemon.BaseStats[stat] = amount;
      Pokemon.StatVariance[stat] = ivSpread;
      CurrentStat = stat;
      UpdateCurrentPokemon();
    }

    public void CurveUpdated(StatGrowth curve)
    {
      Pokemon.BaseStatGrowth = curve;
      UpdateCurrentPokemon();
    }

    public void Refresh()
    {
      LevelSpacing = txtLevelOverlap.Value;
      MaxPokemon = txtMaxExamples.Value;
      UpdateCurrentPokemon();
      UpdateExamples();
    }

    private void btnRefresh_Click(object sender, RoutedEventArgs e)
    {
      Refresh();
    }

    private void txtLevelOverlap_TextChanged(object sender, TextChangedEventArgs e)
    {
      LevelSpacing = txtLevelOverlap.Value;
    }

    private void txtMaxExamples_TextChanged(object sender, TextChangedEventArgs e)
    {
      MaxPokemon = txtMaxExamples.Value;
    }
  }

  public static class NoisyCollectionExtensions
  {
    public static void UpdateAt<T>(this LiveCharts.Helpers.NoisyCollection<T> collection, int index, T obj)
    {
      collection.RemoveAt(index);
      collection.Insert(index, obj);
    }

    public static int IndexOf<T>(this IEnumerable<T> source, T value)
    {
      int index = 0;
      var comparer = EqualityComparer<T>.Default; // or pass in as a parameter
      foreach (T item in source)
      {
        if (comparer.Equals(item, value)) return index;
        index++;
      }
      return -1;
    }
  }
}
