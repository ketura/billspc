﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.Controls
{
  public class AutoBalancePicker<T> : AutoBalancePicker
    where T : struct, IConvertible, IComparable
  {
    public PickerAdder<T> Picker { get; private set; }

    protected override void LabelNameChanged(string label)
    {
      Picker.LabelName = label;
    }

    public List<T> Filter { get; private set; }
    public bool HasBias { get; private set; }
    public T? Bias { get; private set; }
    public int PickerLabelWidth
    {
      get { return Picker.LabelWidth; }
      set { Picker.LabelWidth = value; }
    }

    public AutoBalancePicker() : this(null) { }

    public AutoBalancePicker(List<T> unwanted, T? bias=null) : base()
    {
      InitializeComponent();

      Filter = unwanted ?? new List<T>();
      if(bias.HasValue)
      {
        HasBias = true;
        Bias = bias.Value;
      }
      else
      {
        HasBias = false;
        Bias = null;
      }

      List<T> modifiedSource = new T().GetValues().ToList();
      foreach (T t in Filter)
      {
        modifiedSource.Remove(t);
      }

      Picker = new PickerAdder<T>()
      {
        LabelName = this.LabelName,
        PickerWidth = this.PickerWidth,
        VerticalAlignment = VerticalAlignment.Center,
      };
      BalancerPanel.AddChildAt(Picker, 0);
      Picker.ComboSource = modifiedSource;
      Picker.AddElement += AddType;
      Picker.DeleteElement += RemoveType;
      Picker.ConfirmDelete = false;
      Picker.LabelWidth = 120;

      if (HasBias)
      {
        AddType(Bias.Value);
        Balancer.SetBias(Bias.ToString());        
      }

      Picker.Contains = Balancer.Contains;
    }

    public void AddType(T t, float amount)
    {
      Balancer.AddBox(t.ToString(), amount);
    }

    public void AddType(T t)
    {
      AddType(t, 0);
    }

    public void RemoveType(T t)
    {
      if (t.Equals(Bias))
        Balancer.AddBox(t.ToString(), 0);
      else
        Balancer.RemoveBox(t.ToString());
    }
  }

  public partial class AutoBalancePicker : UserControl
  {

    public string LabelName
    {
      get { return (string)GetValue(LabelNameProperty); }
      set { SetValue(LabelNameProperty, value); }
    }
    public static readonly DependencyProperty LabelNameProperty = DependencyProperty.RegisterAttached("LabelName",
            typeof(string), typeof(AutoBalancePicker), new FrameworkPropertyMetadata("", OnLabelNamePropertyChanged));

    private static void OnLabelNamePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      AutoBalancePicker abp = d as AutoBalancePicker;
      string s = e.NewValue.ToString();
      abp.LabelNameChanged(s);
    }
    protected virtual void LabelNameChanged(string label) { }

    public int PickerWidth
    {
      get { return (int)GetValue(PickerWidthProperty); }
      set { SetValue(PickerWidthProperty, value); }
    }
    public static readonly DependencyProperty PickerWidthProperty = DependencyProperty.RegisterAttached("PickerWidth",
            typeof(int), typeof(AutoBalancePicker), new FrameworkPropertyMetadata(120));


    public AutoBalancePicker()
    {
      //InitializeComponent();      
    }
  }
}
