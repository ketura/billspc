﻿using LiveCharts.Defaults;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace BillsPC
{
  public enum Stat
  {
    [Description("Hit Points")]
    [ShortName("HP")]
    HP,
    [ShortName("END")]
    Endurance,
    [ShortName("ATK")]
    Attack,
    [ShortName("DEF")]
    Defense,
    [Description("Sp. Atk")]
    [ShortName("SPATK")]
    SpecialAttack,
    [Description("Sp. Def")]
    [ShortName("SPDEF")]
    SpecialDefense,
    [ShortName("SPD")]
    Speed,
    [ShortName("INIT")]
    Initiative,
    [ShortName("HT")]
    Height,
    [ShortName("WT")]
    Weight,
    //The below are all 500-based.
    [ShortName("CRIT")]
    Critical,
    [ShortName("OBED")]
    Obedience,
    [ShortName("ATT")]
    Attitude,
    [ShortName("INT")]
    Intelligence,
    [ShortName("CONST")]
    Constitution,
    [ShortName("ACC")]
    Accuracy,
    [ShortName("EVA")]
    Evasion,
    [ShortName("RES")]
    Resistance,
    [ShortName("WEAK")]
    Weakness,
    [ShortName("GEN")]
    Gender,
    [ShortName("TEMP")]
    Temperament,
  }

  public enum StatGrowth
  {
    Standard, //https://en.wikipedia.org/wiki/Logistic_function
    Bug, //linear; y = m * x + b
    Erratic, //shit's cray, yo
    Dragon // y = x ^ 2 + b
  }

  //http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiJtaW4oKDUwKjAuOCkvNTUqeCwoNTAqMC44KSouOCkrKCgzMDArKDUwKjAuOCkpLygxK2VeKC0wLjA1NSooeC01MCkpKSkiLCJjb2xvciI6IiMwMDAwMDAifSx7InR5cGUiOjAsImVxIjoibWluKDUwLzkuMCp4LDUwKSsoMip4KSsyMCIsImNvbG9yIjoiIzg3OUUwNiJ9LHsidHlwZSI6MCwiZXEiOiJtaW4oNTAvNDAuMCp4LDUwKSsoeC81LjApXjIiLCJjb2xvciI6IiNGRjAwMDAifSx7InR5cGUiOjAsImVxIjoic2luKHgvMy44KzQpKm1pbih4KjIsNTApKygoKDMyMCsoNTAqMC44KSkvKDErZV4oLTAuMDU1Kih4LTQ1KSkpKSkiLCJjb2xvciI6IiMwMDZERkMifSx7InR5cGUiOjEwMDAsIndpbmRvdyI6WyItMS4zNDI1NTY2ODgyNDExNDk1IiwiMTAxLjY2OTQ3ODM4NjIwNDk5IiwiLTE3Ljg0MDI0NjQ5NTI5MTkyIiwiNTQzLjU0NTQ1MjI0NTg0NTYiXX1d
  //http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIoKDQwMCsoNTAqMC44KSkvKDErZV4oLTAuMDU1Kih4LTYwKSkpKSIsImNvbG9yIjoiIzAwMDAwMCJ9LHsidHlwZSI6MCwiZXEiOiJtaW4oNip4LDUwKSsoMS41KngpKzIwIiwiY29sb3IiOiIjODc5RTA2In0seyJ0eXBlIjowLCJlcSI6IjEuOF4oKHgtMykvOCkreCs1IiwiY29sb3IiOiIjRkYwMDAwIn0seyJ0eXBlIjowLCJlcSI6InNpbih4LzMrNCkqbWluKHgsMTAwKSsoKCgzNTArKDUwKjAuOCkpLygxK2VeKC0wLjA1NSooeC01MCkpKSkpLTEwIiwiY29sb3IiOiIjMDA2REZDIn0seyJ0eXBlIjoxMDAwLCJ3aW5kb3ciOlsiLTEiLCIxMDEiLCItMjAiLCI1MDAiXSwic2l6ZSI6Wzk1MCw1MDBdfV0-
  //  min((50 * 0.8) / 55 * x, (50 * 0.8) * .8) + ((300 + (50 * 0.8)) / (1 + e^( -0.055 * (x - 45))))

  //min(50 / 9.0 * x, 50) + (2.65 * x)

  //min(50 / 40.0 * x, 50) + (x/ 5.0)^ 2

  //min(50 / 35.0 * x, 50) + ((3 + floor(x / 20)) ^ 3) - max((x - 75) * 8, (10 + floor(x / 20)))

  //sin(x/3.8+4)*min(x*2,50) + (((320 + (50 * 0.8)) / (1 + e^( -0.055 * (x - 45)))))


  public static class StatGrowthFormulas
  {
    public delegate double StatGrowthFormula(int level, float stat);
    public static double Standard(int level, float stat)
    {
      //https://en.wikipedia.org/wiki/Logistic_function
      //return Math.Min((baseStat * 0.8) / 55 * level, (baseStat * 0.8) * .8) + ((300 + (baseStat * 0.8)) / (1 + Math.Pow(Math.E, -0.055 * (level - 45))));
      return stat / 100  * (400 + (50 * 0.8)) / (1 + Math.Pow(Math.E, (-0.055 * (level - 60))))/10;
    }

    public static double Bug(int level, float stat)
    {
      //linear; y = m * x + b
      //return Math.Min(baseStat / 9.0 * level, baseStat) + (2.65 * level);
      return stat / 100 * ((Math.Min(7 * level, 80) + (0.5 * level) + 20)/10);
    }

    public static double Dragon(int level, float stat)
    {
      // y = x ^ 2 + b
      //return Math.Min(baseStat / 40.0 * level, baseStat) + (Math.Pow(level / 5.0, 2));
      return stat / 100 * ((Math.Pow(1.8, ((level - 3) / 8.0)) + level + 5)/ 10);
    }

    public static double Erratic(int level, float stat)
    {
      //shit's cray, yo
      //return Math.Min(baseStat / 35.0 * level, baseStat) + (Math.Pow(3 + level / 20, 3)) - Math.Max((level - 75) * 8, (10 + level / 20));
      return stat / 100 * ((Math.Sin(level / 3.0 + 3) * Math.Min(level, 100) + (((220 + (50 * 0.8)) / (1 + Math.Pow(Math.E, (-0.055 * (level - 50)))))) + 30)/ 10);
    }

    public static StatGrowthFormula GetFormula(StatGrowth growth)
    {
      switch (growth)
      {
        default:
        case StatGrowth.Standard:
          return Standard;
        case StatGrowth.Bug:
          return Bug;
        case StatGrowth.Erratic:
          return Erratic;
        case StatGrowth.Dragon:
          return Dragon;
      }
    }

    public static double GetCumulativeSum(StatGrowth growth, int level, float baseStat)
    {
      return GetCumulativeSum(GetFormula(growth), level, baseStat);
    }

    public static double GetCumulativeSum(StatGrowthFormula growth, int level, float baseStat)
    {
      double total = 0;
      for (int i = 1; i <= level; i++)
      {
        total += growth(i, baseStat);
      }

      return total;
    }

    public static List<ObservablePoint> GetGrowthCurve(StatGrowth curve, float stat, int count)
    {
      List<ObservablePoint> points = new List<ObservablePoint>();

      if (count < 2)
        return points;

      StatGrowthFormula func = GetFormula(curve);

      float space = 100 / (count - 1);

      for(int i = 0; i < count - 1; i++)
      {
        points.Add(new ObservablePoint(i*space, func((int)(i*space), stat)));
      }

      points.Add(new ObservablePoint(100, func(100, stat)));

      return points;
    }
  }

  public class StatPage : Dictionary<Stat, int>
  {
    public StatPage() : base()
    {
      Fill();
    }
    public StatPage(bool fill) : base()
    {
      if (fill)
      {
        Fill();
      }
    }

    private void Fill()
    {
      foreach (Stat stat in new Stat().GetValues())
      {
        Add(stat, 0);
      }
    }
  }

  public class StatModifier : Dictionary<Stat, float>
  {
    public StatModifier()
    {
      foreach (Stat stat in new Stat().GetValues())
      {
        Add(stat, 1.0f);
      }
    }

    public StatModifier(float amount=0.2f) : base()
    {
      foreach (Stat stat in new Stat().GetValues())
      {
        Add(stat, amount);
      }
    }
  }

  public class Stats
  {
    public StatPage Base { get; set; }
    public StatPage IV { get; set; }
    public StatPage EV { get; set; }

    public float IVFactor { get; set; }
    public float EVFactor { get; set; }

    public Stats()
    {
      Base = new StatPage();
      IV = new StatPage();
      EV = new StatPage();
    }

    public int this[Stat stat]
    {
      get { return (int)Math.Floor(Base[stat] + (IVFactor * IV[stat]) + (EVFactor * EV[stat])); }
    }
  }



}