﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BillsPC
{
  public enum OverwriteMode { Skip, Integrate, Overwrite, Duplicate }
  public partial class WebPrompt : Window
  {
    public int StartIndex { get; private set; }
    public int EndIndex { get; private set; }

    public bool Result { get; private set; }

    public OverwriteMode OverwriteMode { get; private set; }

    public WebPrompt()
    {
      InitializeComponent();

      txtDescription.Text = "This will access the page located at " + WindowCoordinator.Instance.CurrentSettings.BulbapediaURL + 
        " and extract the current list of pokemon. \r\n\r\nPlease note that mega forms will not be imported, and pokemon with multiple forms will not parse and are not supported (so no Deoxys).  You'll have to import them manually if you want those, skrub." +
        "\r\n\r\nEnter the National Pokedex number range you would like to import (inclusive):";

      txtStart.Value = 1;
      txtEnd.Value = 721;

      Result = false;
    }

    private void btnOK_Click(object sender, RoutedEventArgs e)
    {
      Result = true;
      SaveValues();
      this.Close();
    }

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Result = false;
      SaveValues();
      this.Close();
    }

    private void SaveValues()
    {
      if (txtStart.Value < 0)
        txtStart.Value = 0;
      if (txtStart.Value > txtEnd.Value)
        Result = false;

      StartIndex = txtStart.Value;
      EndIndex = txtEnd.Value;

      if (rbSkip.IsChecked == true)
      {
        OverwriteMode = OverwriteMode.Skip;
      }
      if (rbIntegrate.IsChecked == true)
      {
        OverwriteMode = OverwriteMode.Integrate;
      }
      if (rbOverwrite.IsChecked == true)
      {
        OverwriteMode = OverwriteMode.Overwrite;
      }
      if (rbDuplicate.IsChecked == true)
      {
        OverwriteMode = OverwriteMode.Duplicate;
      }
    }
  }
}
