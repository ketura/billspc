﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace BillsPC
{
  public class DynamicWrapLayoutModeConverter : IValueConverter
  {
    public static readonly DynamicWrapLayoutModeConverter Row = new DynamicWrapLayoutModeConverter { RowMode = true };
    public static readonly DynamicWrapLayoutModeConverter Column = new DynamicWrapLayoutModeConverter { RowMode = false };

    public bool RowMode { get; set; }

    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double width = System.Convert.ToDouble(value);
      double targetWidth = System.Convert.ToDouble(parameter);
      if (RowMode)
        return width > targetWidth ? 1 : 2;
      else
        return width > targetWidth ? 2 : 1;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
