﻿
using System.Collections;
using System.Collections.Generic;

namespace RPokemon
{
  public class Monster 
  {
    public Species pSpecies { get; protected set; }
    public string Name
    {
      get { return NickName ?? pSpecies.Name; }
      set { NickName = value; }
    }

    public string NickName { get; set; }

    public Composite Stats;
    public Composite Attributes;
    public Composite Resistances;

    void Start()
    {

    }

    void Update()
    {

    }
  }
}
