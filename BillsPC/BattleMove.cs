﻿using System.Collections;
using System.Collections.Generic;


namespace BillsPC
{

  public abstract class IBattleMove
  {
    public string Name;
    public string Description;

    public int Power;
    public int Accuracy;
    public int Fatigue;
    public int DeltaFatigue;
    public float RechargeRate;
    public float EnduranceFactor;

    public Dictionary<PokeType, float> Types;

    public float PhysSpec;

    public Dictionary<Stat, float> OffensiveStats;
    public Dictionary<Stat, float> DefensiveStats;

    public virtual void Execute() { }
  }



  public interface StatCollection
  {

  }

  public class CompositeStat
  {

  }

  public class AttackMove
  {
    public string Name;
    public string Description;

    public Dictionary<Stat, float> OffStats { get; set; }
    public Dictionary<Stat, float> DefStats { get; set; }

    public void Effect() { }
  }

  //public class NormalizedScalars
  //{
  //  int Count;
  //  Dictionary<int, float> Values;

  //  public NormalizedScalars(params float[] values)
  //  {
  //    for (int i = 0; i < values.Length; ++i)
  //    {
  //      Values.Add(i, values[i]);
  //    }
  //  }

  //  public float this[int i]
  //  {
  //    get { return Values[i]; }
  //    set
  //    {
  //      float t = Mathf.Max(value, 1.0f);
  //      float partsum = Sum() - Values[i];
  //      float diff = value - Values[i];
  //      //currently works in one direction, needs to work in both, to enforce total==1
  //      //1.0f - partsum < t
  //      if (diff < 1) //requested change is smaller
  //      {
  //        Values[i] = t;
  //      }
  //      else
  //      {

  //        for (int j = 0; j < Values.Count; ++j)
  //        {
  //          if (j == i)
  //          {
  //            continue;
  //          }

  //          float factor = Values[j] / partsum;
  //          Values[j] -= diff * factor;
  //        }
  //      }

  //      Values[i] = value;
  //    }
  //  }


  //}

}