﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace BillsPC
{
  //http://www.wpftutorial.net/RadioButton.html
  public class EnumToBooleanConverter : IValueConverter
  {
    public object Convert(object value, Type targetType,
                          object parameter, CultureInfo culture)
    {
      if (value == null || parameter == null)
        return false;

      string checkValue = value.ToString();
      string targetValue = parameter.ToString();
      return checkValue.Equals(targetValue,
               StringComparison.InvariantCultureIgnoreCase);
    }

    public object ConvertBack(object value, Type targetType,
                              object parameter, CultureInfo culture)
    {
      if (value == null || parameter == null)
        return null;

      bool useValue = (bool)value;
      string targetValue = parameter.ToString();
      if (useValue)
        return Enum.Parse(targetType, targetValue);

      return null;
    }
  }

  public static class EnumExtensions
  {
    static public bool HasProperty(this Type type, string name)
    {
      return type
          .GetProperties(BindingFlags.Public | BindingFlags.Instance)
          .Any(p => p.Name == name);
    }

    public static string GetShortName<T>(this T value)
    {
      FieldInfo fi = value.GetType().GetField(value.ToString());

      ShortNameAttribute[] attributes = null;

      if (fi != null)
      {
        attributes =
          (ShortNameAttribute[])fi.GetCustomAttributes(
          typeof(ShortNameAttribute),
          false);
      }

      if (attributes != null && attributes.Length > 0)
        return attributes[0].Name;
      else
        return value.ToString();
    }

    public static string GetDescription<T>(this T value)
    {
      FieldInfo fi = value.GetType().GetField(value.ToString());

      DescriptionAttribute[] attributes = null;

      if (fi != null)
      {
        attributes =
          (DescriptionAttribute[])fi.GetCustomAttributes(
          typeof(DescriptionAttribute),
          false);
      }

      if (attributes != null && attributes.Length > 0)
        return attributes[0].Description;
      else
        return value.ToString();
    }

    public static IEnumerable<string> GetDescriptions<T>(this T value)
    {
      List<string> desc = new List<string>();
      foreach(T t in value.GetValues())
      {
        desc.Add(t.GetDescription());
      }
      return desc;
    }

    public static IEnumerable<T> GetValues<T>(this T value)
    {
      return Enum.GetValues(typeof(T)).Cast<T>();
    }

    public static IEnumerable<T> GetValues<T>(this Enum value)
    {
      return Enum.GetValues(typeof(T)).Cast<T>();
    }

    public static T Parse<T>(this T me, string str)
    {
      if (string.IsNullOrEmpty(str))
        return me.GetValues().First();

      return (T)Enum.Parse(typeof(T), str);
    }
  }


  //http://brianlagunas.com/a-better-way-to-data-bind-enums-in-wpf/
  public class EnumBindExtension : MarkupExtension
  {
    private Type _enumType;
    public Type EnumType
    {
      get { return this._enumType; }
      set
      {
        if (value != this._enumType)
        {
          if (null != value)
          {
            Type enumType = Nullable.GetUnderlyingType(value) ?? value;
            if (!enumType.IsEnum)
              throw new ArgumentException("Type must be for an Enum.");
          }

          this._enumType = value;
        }
      }
    }

    public EnumBindExtension() { }

    public EnumBindExtension(Type enumType)
    {
      this.EnumType = enumType;
    }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
      if (null == this._enumType)
        throw new InvalidOperationException("The EnumType must be specified.");

      Type actualEnumType = Nullable.GetUnderlyingType(this._enumType) ?? this._enumType;
      Array enumValues = Enum.GetValues(actualEnumType);

      if (actualEnumType == this._enumType)
        return enumValues;

      Array tempArray = Array.CreateInstance(actualEnumType, enumValues.Length + 1);
      enumValues.CopyTo(tempArray, 1);
      return tempArray;
    }
  }

  public class EnumDescription : EnumConverter
  {
    public EnumDescription(Type type) : base(type) { }

    public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
    {
      if (destinationType == typeof(string))
      {
        if (value != null)
        {
          FieldInfo fi = value.GetType().GetField(value.ToString());
          if (fi != null)
          {
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return ((attributes.Length > 0) && (!String.IsNullOrEmpty(attributes[0].Description))) ? attributes[0].Description : value.ToString();
          }
        }

        return string.Empty;
      }

      return base.ConvertTo(context, culture, value, destinationType);
    }
  }
}
