﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillsPC
{
  public enum PokeType
  {
    Normal, Fighting, Flying, Poison, Ground, Rock, Bug, Ghost, Steel,
    Fire, Water, Grass, Electric, Psychic, Ice, Dragon, Dark, Fairy
  }

  public enum Affinity { Weak, Disadvantaged, Neutral, Strong }

  public class TypeSpread : Dictionary<PokeType, float>
  {
    public TypeSpread() : base()
    {
      foreach(PokeType type in new PokeType().GetValues())
      {
        Add(type, 1.0f);
      }
    }
  }

  public class TypeChart : Dictionary<PokeType, TypeSpread>
  {
    public TypeChart() : base()
    {
      foreach (PokeType type in new PokeType().GetValues())
      {
        Add(type, new TypeSpread());
      }
    }
  }

  
}
