﻿
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public struct StatInfo
{
  //public Stat Stat;
  public string Name;
  public string ShortName;
  public string Description;
}

public class StatInformation 
{
  //public int NumStats = System.Enum.GetNames(typeof(Stat)).Length;
  public List<StatInfo> AllStats = new List<StatInfo>();
}
