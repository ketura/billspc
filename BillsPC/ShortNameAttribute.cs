﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillsPC
{
  [System.AttributeUsage(System.AttributeTargets.Field)
]
  public class ShortNameAttribute : System.Attribute
  {
    public string Name { get; }

    public ShortNameAttribute(string name)
    {
      this.Name = name;
    }
  }
}
