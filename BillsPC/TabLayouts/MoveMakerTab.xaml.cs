﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using BillsPC.Controls;

namespace BillsPC.TabLayouts
{
  
  /// <summary>
  /// Interaction logic for MoveMakerTab.xaml
  /// </summary>
  public partial class MoveMakerTab : UserControl
  {
    public MoveMaker MM { get; private set; }

    public AutoBalancePicker<Stat> OffensiveStats { get; private set; }
    public AutoBalancePicker<Stat> DefensiveStats { get; private set; }
    public AutoBalancePicker<DamageType> DamageTypes { get; private set; }
    public AutoBalancePicker<PokeType> AffinityTypes { get; private set; }
    public AutoBalancePicker<PokeType> OffensiveTypes { get; private set; }

    public EnumControlArray<MoveAttribute, IntegerTextLabel> BaseStats { get; private set; }


    private int PickerWidth = 120;

    public MoveMakerTab()
    {
      MM = new MoveMaker();
      InitializeComponent();

      btnsExpand.Content = MainScrollView;

      TypeHeader.SetContent(TypePanel);
      AnatomyHeader.SetContent(AnatomyPanel);
      TargetHeader.SetContent(TargetPanel);

      List<Stat> metrics = new List<Stat> { Stat.Height, Stat.Weight };

      OffensiveStats = new AutoBalancePicker<Stat>(metrics)
      {
        LabelName = "Offensive Stats:",
        PickerLabelWidth = PickerWidth,
        HorizontalContentAlignment = HorizontalAlignment.Stretch,
        HorizontalAlignment = HorizontalAlignment.Stretch

      };

      DefensiveStats = new AutoBalancePicker<Stat>(metrics)
      {
        LabelName = "Defensive Stats:",
        PickerLabelWidth = PickerWidth,
        HorizontalContentAlignment = HorizontalAlignment.Stretch,
        HorizontalAlignment = HorizontalAlignment.Stretch

      };

      DamageTypes = new AutoBalancePicker<DamageType>()
      {
        LabelName = "Damage Types:",
        PickerLabelWidth = PickerWidth,
        HorizontalContentAlignment = HorizontalAlignment.Stretch,
        HorizontalAlignment = HorizontalAlignment.Stretch

      };

      AffinityTypes = new AutoBalancePicker<PokeType>()
      {
        LabelName = "Affinity Types:",
        PickerLabelWidth = PickerWidth,
        HorizontalContentAlignment = HorizontalAlignment.Stretch,
        HorizontalAlignment = HorizontalAlignment.Stretch

      };

      OffensiveTypes = new AutoBalancePicker<PokeType>()
      {
        LabelName = "Offensive Types:",
        PickerLabelWidth = PickerWidth,
        HorizontalContentAlignment = HorizontalAlignment.Stretch,
        HorizontalAlignment = HorizontalAlignment.Stretch

      };
      TypePanel.AddChildAt(OffensiveStats, 0);
      TypePanel.AddChildAt(DefensiveStats, 1);
      TypePanel.AddChildAt(DamageTypes, 2);
      TypePanel.AddChildAt(AffinityTypes, 3);
      TypePanel.AddChildAt(OffensiveTypes, 4);

      BaseStats = new EnumControlArray<MoveAttribute, IntegerTextLabel>(0, 3, 999, true, true)
      { LabelName = "Stats:", ItemWidth = 145 };

      ScrollStack.AddChildAt(BaseStats, 7);

      cbAimType.Picker.ItemsSource = new AimType().GetValues();
      cbAimType.SelectedItem = AimType.None;



    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      MM.Initialize(this);

    }


    private void cbMoves_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      //string move = "";
      //if (cbMoves.SelectedItem != null)
      //{
      //  MoveWorkArea.IsEnabled = true;
      //  move = cbMoves.SelectedItem.ToString();

      //  if (txtNewMoveName.IsVisible)
      //  {
      //    txtNewMoveName.Text = move;
      //  }

      //  MM.SwitchMove(move);
      //}
      //else
      //{
      //  MoveWorkArea.IsEnabled = false;
      //}
    }

    private void btnNewMove_Checked(object sender, RoutedEventArgs e)
    {
      //btnNewMove.Content = "X";
      //txtNewMoveName.Visibility = Visibility.Visible;
      //txtNewMoveName.Text = "";
      //Keyboard.Focus(txtNewMoveName);
    }

    private void btnNewMove_Unchecked(object sender, RoutedEventArgs e)
    {
      //btnNewMove.Content = "+";
      //txtNewMoveName.Visibility = Visibility.Hidden;
      //btnDeleteMove.Visibility = Visibility.Hidden;
    }

    private void btnDeleteMove_Click(object sender, RoutedEventArgs e)
    {
      //var items = cbMoves.Items ?? null;

      //if (items != null && items.Contains(txtNewMoveName.Text))
      //{
      //  if (MessageBoxResult.Yes == MessageBox.Show("Are you sure you want to delete this move?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Exclamation))
      //  {
      //    MM.RemoveMove(txtNewMoveName.Text);
      //    txtNewMoveName.Text = "";
      //  }
      //}
    }

    private void txtNewMoveName_TextChanged(object sender, TextChangedEventArgs e)
    {
      //var items = cbMoves.Items ?? null;

      //if (items != null && items.Contains(txtNewMoveName.Text))
      //{
      //  btnDeleteMove.Visibility = Visibility.Visible;
      //}
      //else
      //{
      //  if (btnDeleteMove != null)
      //    btnDeleteMove.Visibility = Visibility.Hidden;
      //}
    }

    private void EnterNewMoveName(object sender, KeyEventArgs e)
    {
      //if (e.Key == Key.Enter)
      //{
      //  if (!cbMoves.Items.Contains(txtNewMoveName.Text))
      //  {
      //    MM.AddMove(txtNewMoveName.Text);
      //    cbMoves.SelectedItem = txtNewMoveName.Text;

      //    txtNewMoveName.Text = "";
      //  }
      //}
    }

    private void IntegerTextBox_TextChangedCheck(object sender, TextChangedEventArgs e)
    {

    }

    private void IntegerFilter(object sender, TextCompositionEventArgs e)
    {

    }

    private void AutoSelectOnTab(object sender, RoutedEventArgs e)
    {

    }

    private void SwitchMove(string obj)
    {

    }
  }
}
