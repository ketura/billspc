﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillsPC
{
  public class Settings
  {
    public bool AnimationsEnabled { get; set; }
    public string DefaultPokedex { get; set; }
    public string DefaultTypes { get; set; }
    public string DefaultMoves { get; set; }
    public bool VerboseLogging { get; set; }
    public string LogFile { get; set; }
    public string BulbapediaURL { get; set; }
    public int DefaultCurveSpacing { get; set; }
    public int DefaultMaxCurves { get; set; }
    public int AnimationSpeed { get; set; }
    public bool SortOnExport { get; set; }

    public Settings()
    {
      Reset();
    }

    public void Reset()
    {
      AnimationsEnabled = true;
      VerboseLogging = false;
      BulbapediaURL = "http://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_base_stats_(Generation_VI-present)";
      DefaultPokedex = Path.Combine(WindowCoordinator.StartupPath, "Templates\\calibration.dex");
      DefaultTypes = Path.Combine(WindowCoordinator.StartupPath, "Templates\\typedef.type");
      DefaultMoves = Path.Combine(WindowCoordinator.StartupPath, "Templates\\movesets.move");
      LogFile = Path.Combine(WindowCoordinator.StartupPath, "BillsLog.txt");

      DefaultCurveSpacing = 80;
      DefaultMaxCurves = 5;
      AnimationSpeed = 200;

      SortOnExport = false;
    }

    public void SaveToFile()
    {
      SaveToFile(DefaultConfigPath);
    }

    public void SaveToFile(string filename)
    {
      string output = JsonConvert.SerializeObject(this, Formatting.Indented);
      File.WriteAllText(filename, output);
    }

    public static Settings GenerateDefaultSettings(string filename)
    {
      Settings settings = new Settings();
      settings.SaveToFile(filename);

      return settings;
    }

    public static Settings TryLoadSettings()
    {
      if (File.Exists(DefaultConfigPath))
        return JsonConvert.DeserializeObject<Settings>(File.ReadAllText(DefaultConfigPath));

      return GenerateDefaultSettings(DefaultConfigPath);
    }

    public static readonly string DefaultConfigPath = WindowCoordinator.StartupPath + "\\Settings.json";
  }
}
