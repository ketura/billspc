﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Controls;
using BillsPC.TabLayouts;
using System.IO;

using BillsPC.Controls;
using System.Windows;

namespace BillsPC
{

  public enum HeightType
  {
    Height,
    Shoulder,
    Length,
    Wingspan
  }

  public enum GroupType { None, Pairs, Family, Pack, Herd }

  public enum EvolutionMethod { None, Level, Stone, Trade, TradeItem, TimeOfDay, Happiness, Item, Move, Location, Weather}
  public class EvolutionCondition
  {
    public EvolutionMethod Type;
    public string Pokemon;
    public string Details;
    public int Amount;
    public float Variance;
  }

  public enum AnatomyAttributes { None, Grasp, OnFire, Flight, Dig, Swim, Stance, Segmented, Center, Brain }
  public struct Anatomy
  {
    public string Name { get; set; }
    public string Stats { get; set; }

    public int Count { get; set; }

    public string Attached { get; set; }

    public string Attributes { get; set; }
  }

  public enum GenderType { Both, MaleOnly, FemaleOnly, None }

  public enum Gender { None, Male, Female }


  public class SpeciesDefinition
  {
    public int Version { get; set; }
    public string Name { get; set; }
    public int PokedexNumber { get; set; }
    public string Notes { get; set; }
    public string Lore { get; set; }
    public GenderType Gender { get; set; }
    public float GenderMaleSpread { get; set; }
    public Dictionary<PokeType, float> Types { get; set; }

    public StatPage BaseStats { get; set; }
    public StatModifier StatVariance { get; set; } //IVs
    public Dictionary<Gender, StatModifier> Dimorphism { get; set; }

    public Dictionary<string, SpeciesMove> AllowedMoves { get; set; }

    public string EvolvesFrom { get; set; }

    public Dictionary<string, EvolutionCondition> EvolutionInfo { get; set; }

    public Dictionary<string, SpeciesAbility> Abilities { get; set; }

    public Dictionary<string, Anatomy> AnatomyDefinition { get; set; }
    public StatGrowth BaseStatGrowth { get; set; }

    public float HeightTileFactor { get; set; }
    public float WeightTileFactor { get; set; }
    public HeightType HeightType { get; set; }
    public float TileSize { get; set; }

    public GroupType GroupType { get; set; }
    public int MaxGroupSize { get; set; }
    public bool HasNests { get; set; }
    public bool Nocturnal { get; set; }
    public bool Territorial { get; set; }
    public string TerritorialList { get; set; }
    public int BreedingLevel { get; set; }
    public string PreferredFood { get; set; }
    public bool Nomadic { get; set; }
    public bool Aggressive { get; set; }
    public bool Carnivore { get; set; }
    public bool Herbivore { get; set; }

    public bool Legendary { get; set; }
    public int HeightGrowth { get; set; }
    public int WeightGrowth { get; set; }

    public float Diversity { get; set; }

    public SpeciesDefinition()
    {
      Version = 1;
      Name = "";
      Lore = "";
      PokedexNumber = -1;

      Gender = GenderType.Both;
      GenderMaleSpread = 0.5f;
      Types = new Dictionary<PokeType, float>();
      foreach(PokeType type in new PokeType().GetValues())
      {
        Types[type] = 0;
      }
      Types[PokeType.Normal] = 1.0f;
      BaseStats = new StatPage(true);
      StatVariance = new StatModifier();
      foreach (Stat stat in new Stat().GetValues())
      {
        switch (stat)
        {
          default:
            BaseStats[stat] = 0;
            StatVariance[stat] = 0.2f;
            break;

          case Stat.Accuracy:
          case Stat.Evasion:
          case Stat.Resistance:
          case Stat.Weakness:
          case Stat.Gender:
          case Stat.Temperament:
          case Stat.Constitution:
          case Stat.Intelligence:
          case Stat.Obedience:
          case Stat.Attitude:
          case Stat.Critical:
            BaseStats[stat] = 500;
            StatVariance[stat] = 0.5f;
            break;
        }
      }


      AllowedMoves = new Dictionary<string, SpeciesMove>();

      Dimorphism = new Dictionary<BillsPC.Gender, StatModifier>();
      Dimorphism[BillsPC.Gender.Male] = new StatModifier(1.0f);
      Dimorphism[BillsPC.Gender.Female] = new StatModifier(1.0f);

      EvolutionInfo = new Dictionary<string, EvolutionCondition>();

      Abilities = new Dictionary<string, SpeciesAbility>();
      AnatomyDefinition = new Dictionary<string, Anatomy>();
      BaseStatGrowth = StatGrowth.Standard;

      Legendary = false;
      Diversity = 0.50F;
    }

    public void AddMove(Controls.MoveTag tag)
    {
      AllowedMoves.Add(tag.LabelName, tag.Data);
    }

    public void AddMove(string name)
    {
      SpeciesMove move = new SpeciesMove();
      move.Name = name;

      AllowedMoves.Add(move.Name, move);
    }
  }


  public struct SpeciesMove
  {
    public string Name { get; set; }

    public int Level { get; set; }

    public float Affinity { get; set; }

    private float _iv;
    public float IV
    {
      get { return _iv; }
      set { _iv = Math.Min(Math.Max(0, value), 1.0f); }
    }
  }

  public struct SpeciesAbility
  {
    public string Name { get; set; }
    public string Details { get; set; }

    public float Chance { get; set; }
  }

  [JsonObject(MemberSerialization.OptIn)]
  public class SpeciesSynthesis : TabController
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public SpeciesSynthesisTab main { get; private set; }

    [JsonProperty]
    public Dictionary<string, SpeciesDefinition> Species { get; private set; }

    [JsonProperty]
    public string CurrentPokemon { get; private set; }

    public SpeciesDefinition CurrentPokeDef
    {
      get { return CurrentPokemon == null || !Species.Keys.Contains(CurrentPokemon) ? null : Species[CurrentPokemon]; }
      set { if(CurrentPokemon != null) Species[CurrentPokemon] = value; }
    }

    protected Dictionary<string, ChartComparison> ChartWindows { get; set; }

    public SpeciesSynthesis()
    {
      Species = new Dictionary<string, SpeciesDefinition>();
      ChartWindows = new Dictionary<string, ChartComparison>();
      CurrentPokeDef = null;
      Initialized = false;
    }

    public void Initialize(SpeciesSynthesisTab ss)
    {
      if (!Initialized)
      {
        Log.Debug("Initializing SpeciesSynthesis...");

        main = ss;
        DD.OnControlChange += SetDirty;

        ResetFields();

        DD.WatchAllCustomControls(main.MainScrollView);
        Initialized = true;
        Dirty = false;

        WindowCoordinator.Instance.SS = this;

        Log.Debug("Initializing Complete.");
      }
      else
      {
        SyncDataToFields();
      }

    }

    public override void SyncDataToFields()
    {
      if (CurrentPokeDef == null)
        return;

      Busy = true;

      Log.Debug("Synching all Species text boxes with data.");

      main.txtNotes.Text = CurrentPokeDef.Notes;
      main.cbLegendary.IsChecked = CurrentPokeDef.Legendary;
      main.txtLore.Text = CurrentPokeDef.Lore;
      main.txtPokedexNumber.Value = CurrentPokeDef.PokedexNumber;
      main.GenderTypePicker.Picker.SelectedItem = CurrentPokeDef.Gender;
      main.GenderSpread["Male"].Value = CurrentPokeDef.GenderMaleSpread;

      main.txtDiversity.Value = CurrentPokeDef.Diversity;

      main.EvolutionAdder.ControlPanel.EvolvesFrom.Text = CurrentPokeDef.EvolvesFrom;
      main.EvolutionAdder.ResetAll(CurrentPokeDef.EvolutionInfo);

      main.Behavior.GroupType = CurrentPokeDef.GroupType;
      main.Behavior.MaxGroupSize = CurrentPokeDef.MaxGroupSize;
      main.Behavior.HasNests = CurrentPokeDef.HasNests;
      main.Behavior.Nocturnal = CurrentPokeDef.Nocturnal;
      main.Behavior.Territorial = CurrentPokeDef.Territorial;
      main.Behavior.TerritorialList = CurrentPokeDef.TerritorialList;
      main.Behavior.BreedingLevel = CurrentPokeDef.BreedingLevel;
      main.Behavior.PreferredFood = CurrentPokeDef.PreferredFood;
      main.Behavior.Nomadic = CurrentPokeDef.Nomadic;
      main.Behavior.Aggressive = CurrentPokeDef.Aggressive;
      main.Behavior.SetFoodType(CurrentPokeDef.Carnivore, CurrentPokeDef.Herbivore);

      main.SizeGrid.Height = CurrentPokeDef.BaseStats[Stat.Height];
      main.SizeGrid.Weight = CurrentPokeDef.BaseStats[Stat.Weight];
      main.SizeGrid.HeightIV = CurrentPokeDef.StatVariance[Stat.Height];
      main.SizeGrid.WeightIV = CurrentPokeDef.StatVariance[Stat.Weight];
      main.SizeGrid.HeightTileFactor = CurrentPokeDef.HeightTileFactor;
      main.SizeGrid.WeightTileFactor = CurrentPokeDef.WeightTileFactor;
      main.SizeGrid.HeightGrowth = CurrentPokeDef.HeightGrowth;
      main.SizeGrid.WeightGrowth = CurrentPokeDef.WeightGrowth;
      main.SizeGrid.HeightType = CurrentPokeDef.HeightType;
      main.SizeGrid.cbOverride.IsChecked = CurrentPokeDef.TileSize != 0;
      main.SizeGrid.TileSize = CurrentPokeDef.TileSize;
      main.SizeGrid.RecalculateSize();

      main.SetGrowth(CurrentPokeDef.BaseStatGrowth);

      foreach (Stat stat in new Stat().GetValues())
      {
        if (stat == Stat.Height || stat == Stat.Weight)
          continue;

        main.MaleStats[stat].Value = CurrentPokeDef.Dimorphism[Gender.Male][stat];
        main.FemaleStats[stat].Value = CurrentPokeDef.Dimorphism[Gender.Female][stat];
      }

      main.SecondaryTypePicker.Balancer.ClearAllButBias();
      foreach (PokeType type in new PokeType().GetValues())
      {
        if(CurrentPokeDef.Types[type] > 0)
        {
          main.SecondaryTypePicker.AddType(type, CurrentPokeDef.Types[type]);
        }
        else
        {
          main.SecondaryTypePicker.RemoveType(type);
        }
      }

      foreach (Stat stat in new Stat().GetValues())
      {
        if (stat == Stat.Height || stat == Stat.Weight)
          continue;
        main.BaseStats[stat].Value = CurrentPokeDef.BaseStats[stat];
        main.IVs[stat].Value = CurrentPokeDef.StatVariance[stat];
      }

      main.MoveAdder.ResetAll(CurrentPokeDef.AllowedMoves);
      main.AbilityAdder.ResetAll(CurrentPokeDef.Abilities);
      main.AnatomyAdder.ResetAll(CurrentPokeDef.AnatomyDefinition);

      Busy = false;

      Log.Debug("Sync complete.");
    }

    private void SaveStats()
    {
      if (CurrentPokeDef == null)
        return;

      foreach (Stat stat in new Stat().GetValues())
      {
        if (stat == Stat.Height || stat == Stat.Weight)
          continue;

        CurrentPokeDef.BaseStats[stat] = (int)main.BaseStats[stat].Value;
        CurrentPokeDef.StatVariance[stat] = (float)main.IVs[stat].Value;
      }
    }

    public override void SaveFieldsToData()
    {
      if (CurrentPokeDef == null)
        return;

      Log.Debug("Saving all Species text box to data.");

      CurrentPokeDef.Notes = main.txtNotes.Text;
      CurrentPokeDef.Legendary = main.cbLegendary.IsChecked.Value;
      CurrentPokeDef.Lore = main.txtLore.Text;
      CurrentPokeDef.PokedexNumber = main.txtPokedexNumber.Value;

      CurrentPokeDef.Diversity = main.txtDiversity.Value;

      CurrentPokeDef.GroupType = main.Behavior.GroupType;
      CurrentPokeDef.MaxGroupSize = main.Behavior.MaxGroupSize;
      CurrentPokeDef.HasNests = main.Behavior.HasNests;
      CurrentPokeDef.Nocturnal = main.Behavior.Nocturnal;
      CurrentPokeDef.Territorial = main.Behavior.Territorial;
      CurrentPokeDef.TerritorialList = main.Behavior.TerritorialList;
      CurrentPokeDef.BreedingLevel = main.Behavior.BreedingLevel;
      CurrentPokeDef.PreferredFood = main.Behavior.PreferredFood;
      CurrentPokeDef.Nomadic = main.Behavior.Nomadic;
      CurrentPokeDef.Aggressive = main.Behavior.Aggressive;
      CurrentPokeDef.Carnivore = main.Behavior.Carnivore;
      CurrentPokeDef.Herbivore = main.Behavior.Herbivore;

      CurrentPokeDef.BaseStats[Stat.Height] = main.SizeGrid.Height;
      CurrentPokeDef.BaseStats[Stat.Weight] = main.SizeGrid.Weight;
      CurrentPokeDef.StatVariance[Stat.Height] = main.SizeGrid.HeightIV;
      CurrentPokeDef.StatVariance[Stat.Weight] = main.SizeGrid.WeightIV;
      CurrentPokeDef.HeightTileFactor = main.SizeGrid.HeightTileFactor;
      CurrentPokeDef.WeightTileFactor = main.SizeGrid.WeightTileFactor;
      CurrentPokeDef.HeightGrowth = main.SizeGrid.HeightGrowth;
      CurrentPokeDef.WeightGrowth = main.SizeGrid.WeightGrowth;
      CurrentPokeDef.HeightType = main.SizeGrid.HeightType;
      if (main.SizeGrid.cbOverride.IsChecked == true)
        CurrentPokeDef.TileSize = main.SizeGrid.TileSize;
      else
        CurrentPokeDef.TileSize = 0;

      if (main.GenderTypePicker.Picker.SelectedItem != null)
        CurrentPokeDef.Gender = (GenderType)main.GenderTypePicker.Picker.SelectedItem;

      CurrentPokeDef.BaseStatGrowth = main.GrowthCurve;

      CurrentPokeDef.GenderMaleSpread = main.GenderSpread["Male"].Value;

      CurrentPokeDef.EvolvesFrom = main.EvolutionAdder.ControlPanel.EvolvesFrom.Text;
      CurrentPokeDef.EvolutionInfo.Clear();
      foreach(string evolution in main.EvolutionAdder.Tags.Keys)
      {
        CurrentPokeDef.EvolutionInfo[evolution] = main.EvolutionAdder.Tags[evolution].Data;
      }
      
      foreach (Stat stat in new Stat().GetValues())
      {
        if (stat == Stat.Height || stat == Stat.Weight)
          continue;

        CurrentPokeDef.Dimorphism[Gender.Male][stat] = main.MaleStats[stat].Value;
        CurrentPokeDef.Dimorphism[Gender.Female][stat] = main.FemaleStats[stat].Value;
      }

      foreach (PokeType type in new PokeType().GetValues())
      {
        if (main.SecondaryTypePicker.Balancer.Contains(type.GetDescription()))
        {
          CurrentPokeDef.Types[type] = main.SecondaryTypePicker.Balancer[type.GetDescription()].Value;
        }
        else
        {
          CurrentPokeDef.Types[type] = 0;
        }
      }

      SaveStats();

      CurrentPokeDef.AllowedMoves.Clear();
      foreach (var tag in main.MoveAdder.Tags.Keys)
      {
        CurrentPokeDef.AllowedMoves[tag] = main.MoveAdder.Tags[tag].Data;
      }

      CurrentPokeDef.Abilities.Clear();
      foreach (var tag in main.AbilityAdder.Tags.Keys)
      {
        CurrentPokeDef.Abilities[tag] = main.AbilityAdder.Tags[tag].Data;
      }

      CurrentPokeDef.AnatomyDefinition.Clear();
      foreach (var tag in main.AnatomyAdder.Tags.Keys)
      {
        CurrentPokeDef.AnatomyDefinition[tag] = main.AnatomyAdder.Tags[tag].Data;
      }

      Log.Debug("Save complete.");
    }

    public override void ResetFields()
    {
      main.txtNotes.Text = "";
      main.txtLore.Text = "";
      main.txtPokedexNumber.Value = -1;
      
      foreach(PokeType type in new PokeType().GetValues())
      {
        main.SecondaryTypePicker.RemoveType(type);
      }

      main.txtDiversity.Value = 0.5f;

      main.Behavior.GroupType = GroupType.None;
      main.Behavior.MaxGroupSize = 0;
      main.Behavior.HasNests = false;
      main.Behavior.Nocturnal = false;
      main.Behavior.Territorial = false;
      main.Behavior.TerritorialList = "";
      main.Behavior.BreedingLevel = 0;
      main.Behavior.PreferredFood = "";
      main.Behavior.Nomadic = false;
      main.Behavior.Aggressive = false;
      main.Behavior.SetFoodType(false, false);

      main.SizeGrid.Height = 0;
      main.SizeGrid.Weight = 0;
      main.SizeGrid.HeightIV = 0.2f;
      main.SizeGrid.WeightIV = 0.2f;
      main.SizeGrid.HeightTileFactor = 0.5f;
      main.SizeGrid.WeightTileFactor = 0.5f;
      main.SizeGrid.HeightType = HeightType.Height;
      main.SizeGrid.TileSize = 0.4f;
      main.SizeGrid.cbOverride.IsChecked = false;
      main.SizeGrid.RecalculateSize();

      main.SetGrowth(StatGrowth.Standard);

      main.EvolutionAdder.ControlPanel.EvolvesFrom.Text = "";
      main.EvolutionAdder.ResetAll(null);
      main.SecondaryTypePicker.AddType(PokeType.Normal);
      main.SecondaryTypePicker.Balancer[PokeType.Normal.GetDescription()].Value = 1.0f;

      main.ResetBaseStats();
      main.ResetIVs();

      main.ClearGender();

      main.AbilityAdder.ResetAll(null);
      main.MoveAdder.ResetAll(null);
      main.AnatomyAdder.ClearAll();
    }

    public void CreatePokemon(string name)
    {
      if(!Species.ContainsKey(name) || Species[name] == null)
      {
        Species[name] = new SpeciesDefinition();
        Species[name].Name = name;
        main.SwitchPokemon(name);
        //ResetFields();
        main.SortMode = SortMode.None;
      }
    }

    public void AddPokemon(SpeciesDefinition pokemon)
    {
      if (pokemon != null && !Species.ContainsKey(pokemon.Name))
      {
        InsertPokemon(pokemon);
      }
    }

    public void InsertPokemon(SpeciesDefinition pokemon)
    {
      if (pokemon != null )
      {
        Species[pokemon.Name] = pokemon;
        bool autoswitch = main.CurrentPokemon.AutoSwitch;
        main.CurrentPokemon.AutoSwitch = false;
        main.CurrentPokemon.AddItem(pokemon.Name);
        main.CurrentPokemon.AutoSwitch = autoswitch;

        if (CurrentPokemon == pokemon.Name)
          SyncDataToFields();
      }
    }

    public void RemovePokemon(string name)
    {
      if (Species.Keys.Contains(name))
      {
        Species.Remove(name);
      }
    }

    public void SwitchPokemon(string name)
    {
      if (!Initialized || Busy)
        return;

      if (CurrentPokeDef != null)
      {
        Busy = true;
        SaveFieldsToData();
      }
      CurrentPokemon = name;
      if (!string.IsNullOrEmpty(name))
      {
        CurrentPokeDef = Species[name];
        SyncDataToFields();
      }
      
      Busy = false;
    }

    public ChartComparison CurrentChart()
    {
      if (CurrentPokeDef == null || !ChartWindows.ContainsKey(CurrentPokeDef.Name))
        return null;

      return ChartWindows[CurrentPokeDef.Name];

    }

    public void OpenChartWindow()
    {
      //need to bind close to remove entry
      if (CurrentPokeDef == null)
        return;

      if (!ChartWindows.ContainsKey(CurrentPokeDef.Name))
      {
        ChartWindows[CurrentPokeDef.Name] = WindowCoordinator.Instance.AddChartWindow(CurrentPokeDef);
        ChartWindows[CurrentPokeDef.Name].OnClose = CloseChart;
        ChartWindows[CurrentPokeDef.Name].Show();
      }
      else
      {
        ChartWindows[CurrentPokeDef.Name].Activate();
      }
    }

    public void CloseChart(string name)
    {
      if (ChartWindows.ContainsKey(name))
      {
        ChartWindows[name].Close();
        ChartWindows.Remove(name);
      }
    }

    public void CloseAllCharts()
    {
      foreach(var chart in ChartWindows.Values.ToList())
      {
        chart.Close();
      }
    }

    public void UpdateStat(Stat stat)
    {
      if (CurrentPokeDef == null || Busy)
        return;

      SaveStats();
      CurrentChart()?.StatUpdated(stat, CurrentPokeDef.BaseStats[stat], CurrentPokeDef.StatVariance[stat]);
    }

    public void ResetAll()
    {
      CloseAllCharts();
      Species = new Dictionary<string, SpeciesDefinition>();
      CurrentPokemon = null;
    }

    public void UpdateCurve(StatGrowth curve)
    {
      if (CurrentPokeDef == null || Busy)
        return;

      CurrentChart()?.CurveUpdated(curve);
    }

    public override void ExportAllData(string filename)
    {
      Log.Info("Exporting all Species JSON to " + filename + ".");

      SaveFieldsToData();
      Dirty = false;
      Dictionary<string, SpeciesDefinition> export = null;
      if(WindowCoordinator.Instance.CurrentSettings.SortOnExport)
      {
        var list = Species.ToList();
        list.Sort((pair1, pair2) => pair1.Value.PokedexNumber.CompareTo(pair2.Value.PokedexNumber));
        export = new Dictionary<string, SpeciesDefinition>();
        foreach(var pair in list)
        {
          export.Add(pair.Key, pair.Value);
        }
      }
      else
      {
        export = Species;
      }
      string output = JsonConvert.SerializeObject(export, Formatting.Indented);
      File.WriteAllText(filename, output);

      Log.Info("Species Export complete.");
    }

    public override void ImportAllData(string filename)
    {
      Log.Info("Importing all Species JSON from " + filename + ".");

      //should possibly do more exception handling here?
      string input = File.ReadAllText(filename);

      try
      {
        Species = JsonConvert.DeserializeObject<Dictionary<string, SpeciesDefinition>>(input);
      }
      catch (JsonSerializationException e)
      {
        MessageBox.Show("The JSON could not be read!  File is either out of date or corrupted.\n\n" + e.Message);
        Log.Debug(e.Message);
      }
      SyncDataToFields();
      Dirty = false;
      main.CurrentPokemon.ResetAll(Species.Keys);
      main.SwitchPokemon(main.CurrentPokemon.Picker.SelectedItem as string);

      Log.Info("Mass Species Import complete.");
    }

    public override void ImportSingleData(string filename)
    {
      Log.Info("Importing single Species JSON from " + filename + ".");

      string input = File.ReadAllText(filename);

      //This will enforce the loading of only the first species found in the json.  A bit pointless, but else why
      // does this function exist?
      SpeciesDefinition species = JsonConvert.DeserializeObject<Dictionary<string, SpeciesDefinition>>(input).Values.First();
      if (/*Dirty &&*/ Species.ContainsKey(species.Name))
      {
        var result = MessageBox.Show("Warning! This will overwrite your current data for " + species.Name + ", and any unsaved data for this pokemon will be irrevokably lost.  Continue anyway?", "Unsaved data", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
        if (result == MessageBoxResult.No)
        {
          Log.Info("User aborted single species import.");
          return;
        }
      }
      InsertPokemon(species);

      Log.Info("Type Import complete.");
    }

    /// <summary>
    /// Saves the currently-viewed type definition to a 1-length JSON dictionary.
    /// </summary>
    /// <param name="filename">
    /// The file path of the JSON file to save to.
    /// </param>
    public override void ExportSingleData(string filename)
    {
      Log.Info("Exporting single Species " + CurrentPokemon + " JSON to " + filename + ".");
      SaveFieldsToData();

      //Have to create a fresh dictionary to wrap the definition in.
      var tempdict = new Dictionary<string, SpeciesDefinition>();
      tempdict[CurrentPokemon] = CurrentPokeDef;

      string output = JsonConvert.SerializeObject(tempdict, Formatting.Indented);
      File.WriteAllText(filename, output);

      Log.Info("Species Export complete.");
    }

    internal void MassImport(PokemonCache pokecache, OverwriteMode mode)
    {
      if (pokecache == null)
        return;

      foreach (var pair in pokecache.Definitions)
      {
        SpeciesDefinition pokemon = null;

        if (Species.ContainsKey(pair.Key))
        {
          switch (mode)
          {
            case OverwriteMode.Skip:
              continue;

            default:
            case OverwriteMode.Duplicate:
            case OverwriteMode.Integrate:
              pokemon = Species[pair.Key];
              pokemon.PokedexNumber = pair.Value.Item1;
              var newStats = pair.Value.Item2;
              foreach (Stat stat in new Stat().GetValues())
              {
                if (newStats[stat] != 0)
                {
                  pokemon.BaseStats[stat] = newStats[stat];
                }
              }
              break;

            case OverwriteMode.Overwrite:
              pokemon = new SpeciesDefinition()
              {
                Name = pair.Key,
                PokedexNumber = pair.Value.Item1,
                BaseStats = pair.Value.Item2
              };

              break;
          }

          Species[pair.Key] = pokemon;
        }
        else
        {
          if (mode != OverwriteMode.Duplicate)
          {
            pokemon = new SpeciesDefinition()
            {
              Name = pair.Key,
              PokedexNumber = pair.Value.Item1,
              BaseStats = pair.Value.Item2
            };
          }
        }

        if (pokemon != null)
        {
          AddPokemon(pokemon);
        }
      }
    }

    
  }
}
