﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using BillsPC.Controls;
using System.Collections.ObjectModel;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace BillsPC.TabLayouts
{
  public partial class SpeciesSynthesisTab : UserControl
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public SpeciesSynthesis SS { get; set; }

    public EnumControlArray<Stat, IntegerTextLabel> BaseStats { get; private set; }
    public EnumControlArray<Stat, PercentTextLabel> IVs { get; private set; }
    public EnumControlArray<Stat, PercentTextLabel> MaleStats { get; private set; }
    public EnumControlArray<Stat, PercentTextLabel> FemaleStats { get; private set; }

    public AutoBalancePicker<PokeType> SecondaryTypePicker { get; private set; }

    public StatGrowth GrowthCurve;

    public SpeciesSynthesisTab()
    {
      SS = new SpeciesSynthesis();

      InitializeComponent();

      CurrentPokemon.ItemAdded += SS.CreatePokemon;
      CurrentPokemon.ItemDeleted += SS.RemovePokemon;
      CurrentPokemon.ControlPanel.ConfirmDelete = true;
      CurrentPokemon.ControlPanel.DeletionMessage = "Are you sure you want to delete this Pokemon?  Unsaved changes will be lost.";

      btnsExpand.Content = MainScrollView;

      LoreHeader.SetContent(LorePanel);
      EvolutionHeader.SetContent(EvolutionAdder);
      TypeHeader.SetContent(TypePickerPanel);
      BehaviorHeader.SetContent(BehaviorPanel);
      SizeHeader.SetContent(SizePanel);
      LevelHeader.SetContent(LevelPanel);
      GenderHeader.SetContent(GenderStack);
      AbilityHeader.SetContent(AbilityAdder.BaseGrid);
      MoveHeader.SetContent(MoveAdder.BaseGrid);
      AnatomyHeader.SetContent(AnatomyAdder.BaseGrid);

      List<PokeType> modifiedSource = new List<PokeType>() { PokeType.Normal };

      SecondaryTypePicker = new AutoBalancePicker<PokeType>(modifiedSource, PokeType.Normal) { LabelName = "Add Type: ", PickerWidth = 100, };
      TypePickerPanel.AddChildAt(SecondaryTypePicker, 0);
      

      BaseStats = new EnumControlArray<Stat, IntegerTextLabel>(0, 3, 999, true, true, new List<Stat>() { Stat.Height, Stat.Weight })
        { LabelName = "Base Stats:", ItemWidth = 140 };
      foreach(var box in BaseStats.Values)
      {
        box.TextChanged += BaseStat_TextChanged;
        box.GotFocus += BaseStat_GotFocus;
      }
      ScrollStack.AddChildAt(BaseStats, 13);

      IVs = new EnumControlArray<Stat, PercentTextLabel>(0.2f, 3, 999, true, false, new List<Stat>() { Stat.Height, Stat.Weight })
        { LabelName = "IV Spread: ", ItemWidth = 140 };
      foreach (var box in IVs.Values)
      {
        box.TextChanged += IV_TextChanged;
        box.GotFocus += IV_GotFocus;
      }
      ScrollStack.AddChildAt(IVs, 14);

      GenderSpread.AutoSort = false;

      MaleStats = new EnumControlArray<Stat, PercentTextLabel>(0.2f, 3, 999, true, false, new List<Stat>() { Stat.Height, Stat.Weight })
        { LabelName = "Male Dimorphism: ", ItemWidth = 140 };
      GenderStack.AddChildAt(MaleStats, 1);

      FemaleStats = new EnumControlArray<Stat, PercentTextLabel>(0.2f, 3, 999, true, false, new List<Stat>() { Stat.Height, Stat.Weight })
        { LabelName = "Female Dimorphism: ", ItemWidth = 140 };
      GenderStack.AddChildAt(FemaleStats, 1);

      GenderTypePicker.Picker.ItemsSource = new GenderType().GetValues();
      GenderTypePicker.Picker.SelectionChanged += GenderTypePicker_SelectionChanged;

      MoveAdder.ControlPanel.ConfirmDelete = false;

      btnsImportExportAll.AutoSaveCheck = true;
      btnsImportExportAll.ImportDirtyCheck += SS.IsDirty;
      btnsImportExportAll.OnImportFileSelected += SS.ImportAllData;
      btnsImportExportAll.OnExportFileSelected += SS.ExportAllData;
      btnsImportExportAll.FilenameFilter = "Pokedex File (*.dex)|*.dex|JSON file (*.json)|*.json|All Files (*.*)|*.*";

      btnsImportExportSingle.AutoSaveCheck = false;
      btnsImportExportSingle.OnImportFileSelected += SS.ImportSingleData;
      btnsImportExportSingle.OnExportFileSelected += SS.ExportSingleData;
      btnsImportExportSingle.Multiselect = true;
      btnsImportExportSingle.FilenameFilter = "Pokemon File (*.pkmn)|*.pkmn|Pokedex File (*.dex)|*.dex|JSON file (*.json)|*.json|All Files (*.*)|*.*";

      StandardGrowth.Content = new Controls.SimpleChart(StatGrowth.Standard) { RenderTransform = new RotateTransform(90, 64, 64) };
      BugGrowth.Content = new Controls.SimpleChart(StatGrowth.Bug) { RenderTransform = new RotateTransform(90, 64, 64) };
      DragonGrowth.Content = new Controls.SimpleChart(StatGrowth.Dragon) { RenderTransform = new RotateTransform(90, 64, 64) };
      ErraticGrowth.Content = new Controls.SimpleChart(StatGrowth.Erratic) { RenderTransform = new RotateTransform(90, 64, 64) };
      

      SwitchPokemon(null);
      GrowthCurve = StatGrowth.Standard;
      //http://blog.jerrynixon.com/2014/12/lets-code-data-binding-to-radio-button.html
    }

    private void IV_GotFocus(object sender, RoutedEventArgs e)
    {
      SS.UpdateStat(IVs.GetKey(sender as PercentTextLabel));
    }

    private void BaseStat_GotFocus(object sender, EventArgs e)
    {
      SS.UpdateStat(BaseStats.GetKey(sender as IntegerTextLabel));
    }

    private void BoxChecked(object sender, RoutedEventArgs e)
    {
      SetGrowth(new StatGrowth().Parse(((RadioButton)sender).Tag.ToString()));
      Log.Debug(GrowthCurve.ToString());
    }

    public void SetGrowth(StatGrowth growth)
    {
      GrowthCurve = growth;
      switch (growth)
      {
        case StatGrowth.Standard:
        default:
          StandardGrowth.IsChecked = true;
          break;
        case StatGrowth.Bug:
          BugGrowth.IsChecked = true;
          break;
        case StatGrowth.Erratic:
          ErraticGrowth.IsChecked = true;
          break;
        case StatGrowth.Dragon:
          DragonGrowth.IsChecked = true;
          break;
      }
      SS.UpdateCurve(growth);
    }

    private void IV_TextChanged(object sender, TextChangedEventArgs e)
    {
      SS.UpdateStat(IVs.GetKey(sender as PercentTextLabel));
    }

    private void BaseStat_TextChanged(object sender, TextChangedEventArgs e)
    {
      SS.UpdateStat(BaseStats.GetKey(sender as IntegerTextLabel));
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      SS.Initialize(this);
    }

    public void SwitchPokemon(string pokename)
    {
      CurrentPokemon.SwitchItem(pokename);
      SS.SwitchPokemon(pokename);
      SecondaryTypePicker.Picker.AdjustSelection();

      CheckForSelected();
    }

    public void CheckForSelected()
    {
      if (CurrentPokemon.Picker.SelectedIndex > -1)
      {
        ScrollStack.Visibility = Visibility.Visible;
        btnsExpand.Visibility = Visibility.Visible;
        btnOpenChart.Visibility = Visibility.Visible;
      }
      else
      {
        ScrollStack.Visibility = Visibility.Collapsed;
        btnsExpand.Visibility = Visibility.Collapsed;
        btnOpenChart.Visibility = Visibility.Collapsed;
      }
    }

    private void ForwardMove(string move)
    {

    }

    public void ResetBaseStats()
    {
      foreach (var box in BaseStats.Keys)
      {
        switch (box)
        {
          default:
            BaseStats[box].Value = 0;
            break;

          case Stat.Accuracy:
          case Stat.Evasion:
          case Stat.Resistance:
          case Stat.Weakness:
          case Stat.Gender:
          case Stat.Temperament:
          case Stat.Constitution:
          case Stat.Intelligence:
          case Stat.Obedience:
          case Stat.Attitude:
          case Stat.Critical:
            BaseStats[box].Value = 500;
            break;
        }
      }
    }

    public void ClearBaseStats()
    {
      foreach (var box in BaseStats.Values)
      {
        box.Value = 0;
      }
    }

    public void ResetIVs()
    {
      foreach (var box in IVs.Keys)
      {
        switch (box)
        {
          default:
            IVs[box].Value = 0.2f;
            break;

          case Stat.Resistance:
          case Stat.Weakness:
          case Stat.Temperament:
          case Stat.Gender:
            IVs[box].Value = 1.0f;
            break;
        }
      }
    }

    public void ClearIVs()
    {
      foreach (var box in IVs.Values)
      {
        box.Value = 0;
      }
    }

    public void ResetGender()
    {
      if ((GenderType)GenderTypePicker.SelectedItem == GenderType.Both)
      {
        GenderSpread.IsEnabled = true;
        GenderSpread.Visibility = Visibility.Visible;
        MaleStats.Visibility = Visibility.Visible;
        FemaleStats.Visibility = Visibility.Visible;
        btnDefaultGender.Visibility = Visibility.Visible;
      }
      else
      {
        if ((GenderType)GenderTypePicker.SelectedItem == GenderType.None)
        {
          GenderSpread.Visibility = Visibility.Collapsed;
        }
        else
        {
          GenderSpread.Visibility = Visibility.Visible;
        }

        GenderSpread.IsEnabled = false;
        MaleStats.Visibility = Visibility.Collapsed;
        FemaleStats.Visibility = Visibility.Collapsed;
        btnDefaultGender.Visibility = Visibility.Collapsed;
      }

      switch ((GenderType)GenderTypePicker.SelectedItem)
      {
        case GenderType.Both:
        case GenderType.None:
        default:
          GenderSpread.Controls["Male"].Value = 0.5f;
          GenderSpread.Controls["Female"].Value = 0.5f;
          break;

        case GenderType.FemaleOnly:
          GenderSpread.Controls["Male"].Value = 0f;
          GenderSpread.Controls["Female"].Value = 1.0f;
          break;

        case GenderType.MaleOnly:
          GenderSpread.Controls["Male"].Value = 1.0f;
          GenderSpread.Controls["Female"].Value = 0f;
          break;
      }
    }

    public void ClearGender()
    {
      GenderTypePicker.Picker.SelectedItem = GenderType.Both;

      foreach (Stat stat in new Stat().GetValues())
      {
        if (stat == Stat.Height || stat == Stat.Weight)
          continue;

        MaleStats[stat].Value = 1.0f;
        FemaleStats[stat].Value = 1.0f;
      }
    }

    private void GenderTypePicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ResetGender();
    }

    private void btnDefaultGender_Click(object sender, RoutedEventArgs e)
    {
      //this is awful, lol.  fix this.

      Dictionary<Stat, float> MaleValues = new Dictionary<Stat, float>()
      {
        { Stat.HP, 1.0f },
        { Stat.Endurance, 1.0f },
        { Stat.Attack, 1.1f },
        { Stat.Defense, 0.9f },
        { Stat.SpecialAttack, 0.9f },
        { Stat.SpecialDefense, 1.1f },
        { Stat.Resistance, 0.9f },
        { Stat.Weakness, 1.1f },
        { Stat.Speed, 1.1f },
        { Stat.Initiative, 0.9f },
        { Stat.Accuracy, 0.9f },
        { Stat.Evasion, 1.1f },
        { Stat.Critical, 1.0f },
        { Stat.Height, 1.1f },
        { Stat.Weight, 1.1f },
        { Stat.Obedience, 1.1f },
        { Stat.Intelligence, 1.0f },
        { Stat.Gender, 1.0f },
        { Stat.Temperament, 1.0f },
      };

      Dictionary<Stat, float> FemaleValues = new Dictionary<Stat, float>()
      {
        { Stat.HP, 1.0f },
        { Stat.Endurance, 1.0f },
        { Stat.Attack, 0.9f },
        { Stat.Defense, 1.1f },
        { Stat.SpecialAttack, 1.1f },
        { Stat.SpecialDefense, 0.9f },
        { Stat.Resistance, 1.1f },
        { Stat.Weakness, 0.9f },
        { Stat.Speed, 0.9f },
        { Stat.Initiative, 1.1f },
        { Stat.Accuracy, 1.1f },
        { Stat.Evasion, 0.9f },
        { Stat.Critical, 1.0f },
        { Stat.Height, 0.9f },
        { Stat.Weight, 0.9f },
        { Stat.Obedience, 0.9f },
        { Stat.Intelligence, 1.0f },
        { Stat.Gender, 1.0f },
        { Stat.Temperament, 1.0f },
      };

      foreach (Stat s in MaleValues.Keys)
      {
        MaleStats[s].Value = MaleValues[s];
        FemaleStats[s].Value = FemaleValues[s];
      }
    }

    private void MainTab_Unloaded(object sender, RoutedEventArgs e)
    {
      SS.SaveFieldsToData();
    }

    private void btnOpenChart_Click(object sender, RoutedEventArgs e)
    {
      SS.OpenChartWindow();
    }

    private bool autoscroll = false;

    //Taken from:http://stackoverflow.com/questions/2984803/how-to-automatically-scroll-scrollviewer-only-if-the-user-did-not-change-scrol?answertab=votes#tab-top
    private void MainScrollView_ScrollChanged(object sender, ScrollChangedEventArgs e)
    {
      if(e.ExtentHeightChange > 0 && MainScrollView.VerticalOffset == MainScrollView.ScrollableHeight - e.ExtentHeightChange)
      {
        MainScrollView.ScrollToVerticalOffset(MainScrollView.VerticalOffset + e.ExtentHeightChange * 0.3);
      }
    }

    private SortMode _sortMode;
    public SortMode SortMode
    {
      get { return _sortMode; }
      set
      {
        SortPokemon(value);
      }
    }

    private void btnSort_Click(object sender, RoutedEventArgs e)
    {
      switch (SortMode)
      {
        default:
        case SortMode.None:
          SortMode = SortMode.Alphabet;
          break;
        case SortMode.Alphabet:
          SortMode = SortMode.Number;
          break;
        case SortMode.Number:
          SortMode = SortMode.Alphabet;
          break;
      }
    }

    public void SortPokemon(SortMode mode = SortMode.None)
    {
      _sortMode = mode;
      btnSort.Content = mode.GetShortName();

      switch (mode)
      {
        default:
        case SortMode.None:
          break;
        case SortMode.Alphabet:
          CurrentPokemon.ResetAll(CurrentPokemon.Content.OrderBy(x => x.ToString()));
          break;
        case SortMode.Number:
          CurrentPokemon.ResetAll(CurrentPokemon.Content.OrderBy(x => SS.Species[x.ToString()].PokedexNumber));
          break;
      }
      
    }

    public static string downloadWebPage(string theURL)
    {
      //### download a web page to a string
      WebClient client = new WebClient();

      client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

      Stream data = client.OpenRead(theURL);
      StreamReader reader = new StreamReader(data);
      string s = reader.ReadToEnd();
      return s;
    }

    private void btnBulbapedia_Click(object sender, RoutedEventArgs e)
    {
      var prompt = new WebPrompt() { Owner = Window.GetWindow(this) };
      prompt.ShowDialog();

      if (prompt.Result != true)
        return;

      string filename = System.IO.Path.Combine(WindowCoordinator.StartupPath, "Templates", "pokecache.json");
      PokemonCache pokecache = null;

      if (File.Exists(filename))
      {
        try
        {
          Log.Debug("Pulling pokecache from " + filename);
          pokecache = JsonConvert.DeserializeObject<PokemonCache>(File.ReadAllText(filename));

          if((DateTime.Now - pokecache.DatePulled).TotalHours > 1 )
          {
            pokecache = null;
            Log.Info("Pokecache is more than an hour old; repulling.");
          }
          else
          {
            Log.Info("Pokecache is less than an hour old; using cached data.");
            
          }
        }
        catch (JsonSerializationException)
        {
          Log.Info("Pokecache is corrupted; repulling.");
          pokecache = null;
        }
      }
      else
      {
        Log.Info("Pokecache not found; repulling.");
      }

      if(pokecache == null)
      { 
        //this bastard reads bulbapedia's wiki page and parses out the relevant fields from the html.
        string regex = @"<tr align=""\w+"" style=""background:#\w+"">\r?\n<td align=""\w+""> ?<b>(\d+)</b>\r?\n</td>\r?\n<td> <span class=""plainlinks""><a href=""/wiki/[\w%\(\)\.]+"" title=""[\w ="":/\.><]+</span>\r?\n</td>\r?\n<td align=""\w+""> <a href=""/wiki/[\w%\(\) _'\.-]+"" title="".+? \(Pokémon\)"">(.+?)</a>\r?\n</td>\r?\n<td style=""background:#\w+""> ([\d\.]+)\r?\n</td>\r?\n<td style=""background:#\w+""> ([\d\.]+)\r?\n</td>\r?\n<td style=""background:#\w+""> ([\d\.]+)\r?\n</td>\r?\n<td style=""background:#\w+""> ([\d\.]+)\r?\n</td>\r?\n<td style=""background:#\w+""> ([\d\.]+)\r?\n</td>\r?\n<td style=""background:#\w+""> ([\d\.]+)\r?\n</td>\r?\n<td> (\d+)\r?\n</td>\r?\n<td> ([\d.]+)\r?\n</td></tr>";
        //regex = @"(?s)<tr.+?> <b>(\d+)</b>.+?\(Pokémon\)"">(.+?)</a>\n</td>\n<td .+?> ([\d\.]+)\n</td>\n<td .+?> ([\d\.]+)\n</td>\n<td .+?> ([\d\.]+)\n</td>\n<td .+?> ([\d\.]+)\n</td>\n<td .+?> ([\d\.]+)\n</td>\n<td .+?> ([\d\.]+).+?/tr>";
        Regex r = new Regex(regex);
        string webpage = downloadWebPage(WindowCoordinator.Instance.CurrentSettings.BulbapediaURL);

        //This sucker is 20k lines.  The above regex fails on it.  So we'll instead start with all the table rows in the document and parse those
        // individually instead.
        List<string> rows = new List<string>();
        foreach (Match m in Regex.Matches(webpage, @"(?s)<tr.*?</tr>"))
        {
          rows.Add(m.Value);
        }

        var results = new Dictionary<string, Tuple<int, StatPage>>();
        foreach (string s in rows)
        {
          Match m = r.Match(s);
          if (string.IsNullOrEmpty(m.Value))
            continue;

          int dexNumber = int.Parse(m.Groups[1].Value);
          string name = m.Groups[2].Value;
          StatPage stats = new StatPage(true);
          stats[Stat.HP] = (int)(int.Parse(m.Groups[3].Value) * 3.5);
          stats[Stat.Endurance] = (int)(int.Parse(m.Groups[3].Value) * 6);
          stats[Stat.Attack] = (int)(int.Parse(m.Groups[4].Value) * 4.5);
          stats[Stat.Defense] = (int)(int.Parse(m.Groups[5].Value) * 4);
          stats[Stat.SpecialAttack] = (int)(int.Parse(m.Groups[6].Value) * 5);
          stats[Stat.SpecialDefense] = (int)(int.Parse(m.Groups[7].Value) * 4.5);
          stats[Stat.Initiative] = (int)(int.Parse(m.Groups[8].Value) * 6);

          results[name] = new Tuple<int, StatPage>(dexNumber, stats);
        }

        pokecache = new PokemonCache() { DatePulled = DateTime.Now, Definitions = results };

        string output = JsonConvert.SerializeObject(pokecache, Formatting.Indented);

        try
        {
          Log.Debug("Writing pokecache to " + filename);
          File.WriteAllText(filename, output);
        }
        catch(Exception ex)
        {
          Log.Debug("Error writing pokecache:\n\n" + ex.Message);
        }
      }

      pokecache.Definitions = pokecache.Definitions.Where(x => x.Value.Item1 >= prompt.StartIndex && x.Value.Item1 <= prompt.EndIndex).ToDictionary(x => x.Key, x => x.Value);

      SS.MassImport(pokecache, prompt.OverwriteMode);
    }

    private void btnClearAll_Click(object sender, RoutedEventArgs e)
    {
      if (CurrentPokemon.Content.Count <= 0)
        return;

      var result = MessageBox.Show("Warning!  This will delete all pokemon.  Unsaved changes will be lost.\r\n\r\nContinue?", "Delete All", MessageBoxButton.YesNo);
      if(result == MessageBoxResult.Yes)
      {
        CurrentPokemon.ResetAll();
        SS.ResetAll();
        CheckForSelected();
      }
    }

    private void SSMainGrid_PreviewKeyDown(object sender, KeyEventArgs e)
    {
      if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
      {
        if (e.Key == Key.E)
        {
          Log.Debug("ctrl+E");
          if(EvolutionAdder.Tags.Count > 0)
          {
            CurrentPokemon.SwitchItem(EvolutionAdder.Tags.First().Key);
          }
          
        }
        else if (e.Key == Key.D)
        {
          Log.Debug("ctrl+D");
          string prevolution = EvolutionAdder.ControlPanel.EvolvesFrom.Text;
          if (prevolution != "")
          {
            CurrentPokemon.SwitchItem(prevolution);
          }
        }
        else if (e.Key == Key.Up)
        {
          Log.Debug("ctrl+Up");
          PreviousPokemon();
        }
        else if (e.Key == Key.Down)
        {
          Log.Debug("ctrl+Down");
          NextPokemon();
        }
      }

    }

    private void NextPokemon()
    {
      if (CurrentPokemon.Picker.Items.Count < 2)
        return;

      if (CurrentPokemon.Picker.SelectedIndex == CurrentPokemon.Picker.Items.Count - 1)
      {
        CurrentPokemon.Picker.SelectedIndex = 0;
      }
      else
      {
        CurrentPokemon.Picker.SelectedIndex += 1;
      }
    }

    private void PreviousPokemon()
    {
      if (CurrentPokemon.Picker.Items.Count < 2)
        return;

      if (CurrentPokemon.Picker.SelectedIndex == 0)
      {
        CurrentPokemon.Picker.SelectedIndex = CurrentPokemon.Picker.Items.Count - 1;
      }
      else
      {
        CurrentPokemon.Picker.SelectedIndex -= 1;
      }

    }

    
  }

  public enum SortMode
  {
    [ShortName(" - ")]
    None,
    [ShortName("ABC")]
    Alphabet,
    [ShortName("123")]
    Number
  }

  
}
