﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

using Newtonsoft.Json;
using log4net;
using BillsPC.TabLayouts;

namespace BillsPC
{
  /// <summary>
  /// The actual data representation of a pokemon type.  
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class TypeDefinition
  {
    [JsonProperty]
    public int Version { get; protected set; }

    //Only used for when individual types are exported.
    [JsonProperty]
    public string Name { get; protected set; }

    //A "row" of the type match chart; this is a Dictionary<PokeType, float>. The float is the actual multiplier,
    // so "2.0" and not "200%".
    [JsonProperty]
    public TypeSpread Damage { get; protected set; }

    public float this[PokeType type]
    {
      get { return Damage[type]; }
      set { Damage[type] = value; }
    }

    //Notes will probably not be used in-game, so treat this essentially like comments.
    [JsonProperty]
    public string Notes { get; set; }

    //The relationships between types
    [JsonProperty]
    public Dictionary<PokeType, Affinity> Affinities { get; private set; }

    public TypeDefinition(PokeType name)
    {
      Version = 1;
      Name = name.ToString();
      Damage = new TypeSpread();
      Affinities = new Dictionary<PokeType, Affinity>();
      foreach(PokeType type in new PokeType().GetValues())
      {
        Affinities[type] = Affinity.Neutral;
      }
    }
  }

  /// <summary>
  /// The gatekeeper that stands between the TypeDefinition and the TypeTemplateTab.  As much as possible, all control-specific
  /// code should be contained within the tab itself, and this class should only be concerned with validation, import,
  /// export, field population, and related tasks.
  /// </summary>
  public class TypeTemplate : TabController
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public TypeTemplateTab main { get; private set; }

    private PokeType _currentType;
    public PokeType CurrentType
    {
      get { return _currentType; }
      set { SwitchCurrentType(value); }
    }

    public TypeDefinition CurrentTypeDef
    {
      get { return Types[CurrentType]; }
      set { Types[CurrentType] = value; }
    }

    //This is the data that is actually serialized upon export.
    public Dictionary<PokeType, TypeDefinition> Types;

    public TypeTemplate() : base()
    {
      Initialized = false;
      Types = new Dictionary<PokeType, TypeDefinition>();
      foreach(PokeType type in new PokeType().GetValues())
      {
        Types[type] = new TypeDefinition(type);
      }
    }

    /// <summary>
    /// This is called after every Window_Loaded event fired by the tab, which in practice is each time the tab itself
    /// is navigated to.  Thus, we must track whether or not the "real" initialization has occurred.
    /// </summary>
    /// <param name="tt">
    /// The TabTemplateTab that called this function.
    /// </param>
    public void Initialize(TypeTemplateTab tt)
    {
      if (!Initialized)
      {
        Log.Debug("Initializing TypeTemplate...");

        main = tt;
        DD.OnControlChange += SetDirty;

        CurrentType = PokeType.Normal;
        ResetFields();

        DD.WatchAllCustomControls(main.MainScrollView);
        Initialized = true;
        Dirty = false;
        WindowCoordinator.Instance.TT = this;
        Log.Debug("Initializing Complete.");
      }
      else
      {
        SyncDataToFields();
      }
    }

    /// <summary>
    /// Takes all of the data within the TypeDefinition and displays it throughout the UI.
    /// </summary>
    public override void SyncDataToFields()
    {
      Log.Debug("Syncing all Type text boxes with data.");

      main.txtNotes.Text = Types[CurrentType].Notes;
      foreach (PokeType type in new PokeType().GetValues())
      {
        main.TypeBoxes[type].Value = CurrentTypeDef[type];
        main.DefenseBoxes[type].Value = Types[type][CurrentType];
        main.AffinityBoxes[type].cbPicker.SelectedItem = CurrentTypeDef.Affinities[type];
      }

      main.DefenseBoxes[CurrentType].IsEnabled = false;
      main.DefenseBoxes[CurrentType].Value = CurrentTypeDef[CurrentType];

      Log.Debug("Sync complete.");
    }

    /// <summary>
    /// Parses all of the data in the UI and saves it into the current TypeDefinition.
    /// </summary>
    public override void SaveFieldsToData()
    {
      Log.Debug("Saving all Type text box to data.");

      Types[CurrentType].Notes = main.txtNotes.Text;
      foreach (PokeType type in new PokeType().GetValues())
      {
        CurrentTypeDef[type] = main.TypeBoxes[type].Value;
        Types[type][CurrentType] = main.DefenseBoxes[type].Value;
        CurrentTypeDef.Affinities[type] = (Affinity)main.AffinityBoxes[type].cbPicker.SelectedItem;
      }

      Log.Debug("Save complete.");
    }

    /// <summary>
    /// Scrubs the UI to show an empty, default set of data.
    /// </summary>
    public override void ResetFields()
    {
      main.txtNotes.Text = "";
      foreach(PokeType type in new PokeType().GetValues())
      {
        main.TypeBoxes[type].Value = 1;
        main.DefenseBoxes[type].Value = 1;
        main.AffinityBoxes[type].cbPicker.SelectedItem = Affinity.Neutral;
      }

      main.DefenseBoxes[CurrentType].IsEnabled = false;
    }

    /// <summary>
    /// Changes the current type and adjusts the UI accordingly.
    /// </summary>
    /// <param name="newType">
    /// The type to switch to.
    /// </param>
    public void SwitchCurrentType(PokeType newType)
    {
      //At run time the comboboxes are all over the place and this will get called several times before the
      // window is actually loaded.
      if (!Initialized)
        return;

      main.DefenseBoxes[CurrentType].IsEnabled = true;

      bool dirty = Dirty;
      SaveFieldsToData();
      _currentType = newType;
      SyncDataToFields();
      Dirty = dirty;
    }

    /// <summary>
    /// Imports several types at a time from a serialized JSON file.
    /// </summary>
    /// <param name="filename">
    /// The file path of the JSON file to load.
    /// </param>
    public override void ImportAllData(string filename)
    {
      Log.Info("Importing all Type JSON from " + filename + ".");
      
      //should possibly do more exception handling here?
      string input = File.ReadAllText(filename);

      Dictionary<PokeType, TypeDefinition> typedefs = JsonConvert.DeserializeObject<Dictionary<PokeType, TypeDefinition>>(input);

      //Save the dirty status.  The whole dirty system is kind of broken for small chunks of updated data, but this
      // will at least make a token effort to prevent message spam to the user.
      bool dirty = Dirty;

      foreach(PokeType type in typedefs.Keys)
      {
        Types[type] = typedefs[type];
      }

      SyncDataToFields();

      //Either use the saved dirty status or we just wiped the entire list and can assume we are not dirty.
      Dirty = dirty || typedefs.Keys.Count != new PokeType().GetValues().Count();

      Log.Info("Mass Type Import complete.");
    }

    /// <summary>
    /// Exports all type definition data to a serialized JSON file.
    /// </summary>
    /// <param name="filename">
    /// The file path of the JSON file to save to.
    /// </param>
    public override void ExportAllData(string filename)
    {
      Log.Info("Exporting all Type JSON to " + filename + ".");

      SaveFieldsToData();
      Dirty = false;
      string output = JsonConvert.SerializeObject(Types, Formatting.Indented);
      File.WriteAllText(filename, output);

      Log.Info("Mass Type Export complete.");
    }

    /// <summary>
    /// Imports a single type definition.  This is honestly a bit superflourous, I just didn't want to split the 
    /// import/export buttons control into two separate controls.  This does have its own dirty handling in an attempt
    /// to justify its own existance over ImportAllData().
    /// </summary>
    /// <param name="filename">
    /// The file path of the JSON file to load.
    /// </param>
    public override void ImportSingleData(string filename)
    {
      Log.Info("Importing single Type JSON from " + filename + ".");

      string input = File.ReadAllText(filename);

      //This will enforce the loading of only the first type found in the json.  A bit pointless, but else why
      // does this function exist?
      TypeDefinition typedef = JsonConvert.DeserializeObject<Dictionary<PokeType, TypeDefinition>>(input).Values.First();
      if (Dirty)
      {
        var result = MessageBox.Show("Warning! This will overwrite your current data for " + typedef.Name + ", and any unsaved data for this type will be irrevokably lost.  Continue anyway?" , "Unsaved data", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
        if (result == MessageBoxResult.No)
        {
          Log.Info("User aborted single type import.");
          return; 
        }
      }
      Types[new PokeType().Parse(typedef.Name)] = typedef;
      SyncDataToFields();

      Log.Info("Type Import complete.");
    }

    /// <summary>
    /// Saves the currently-viewed type definition to a 1-length JSON dictionary.
    /// </summary>
    /// <param name="filename">
    /// The file path of the JSON file to save to.
    /// </param>
    public override void ExportSingleData(string filename)
    {
      Log.Info("Exporting single Type " + CurrentType + " JSON to " + filename + ".");
      SaveFieldsToData();

      //Have to create a fresh dictionary to wrap the definition in.
      Dictionary<PokeType, TypeDefinition> tempdict = new Dictionary<PokeType, TypeDefinition>();
      tempdict[CurrentType] = CurrentTypeDef;

      string output = JsonConvert.SerializeObject(tempdict, Formatting.Indented);
      File.WriteAllText(filename, output);

      Log.Info("Type Export complete.");
    }
  }
}

