﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BillsPC.TabLayouts
{

  public partial class SettingsTab : UserControl
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
     (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public Settings Settings { get; private set; }

    public SettingsTab()
    {
      InitializeComponent();
      Settings = Settings.TryLoadSettings();
      var wc = WindowCoordinator.Instance;
      wc.CurrentSettings = Settings;
      this.Dispatcher.ShutdownStarted += Dispatcher_ShutdownStarted;

      SynchSettings();
      
    }

    private void Dispatcher_ShutdownStarted(object sender, EventArgs e)
    {
      SaveSettings();
      Settings.SaveToFile();
    }

    private string StripAppPath(string fullpath)
    {
      string path = fullpath;

      if (fullpath.StartsWith(WindowCoordinator.StartupPath))
      {
        path = fullpath.Replace(WindowCoordinator.StartupPath, "");
        path = Regex.Replace(path, @"^\\", "");
      }

      return path;
    }

    private void SynchSettings()
    {
      txtMoves.Text = StripAppPath(Settings.DefaultMoves);
      txtTypes.Text = StripAppPath(Settings.DefaultTypes);
      txtPokedex.Text = StripAppPath(Settings.DefaultPokedex);
      txtLog.Text = StripAppPath(Settings.LogFile);

      chkAnimations.IsChecked = Settings.AnimationsEnabled;

      txtAnimationSpeed.Value = Settings.AnimationSpeed;
      txtBulbapedia.Text = Settings.BulbapediaURL;
      txtCurveSpacing.Value = Settings.DefaultCurveSpacing;
      txtMaxCurves.Value = Settings.DefaultMaxCurves;

      chkVerbose.IsChecked = Settings.VerboseLogging;
      chkSortExport.IsChecked = Settings.SortOnExport;
    }

    private void SaveSettings()
    {
      Settings.DefaultMoves = txtMoves.Text;
      Settings.DefaultTypes = txtTypes.Text;
      Settings.DefaultPokedex = txtPokedex.Text;
      Settings.LogFile = txtLog.Text;

      Settings.AnimationsEnabled = chkAnimations.IsChecked.Value;

      Settings.AnimationSpeed = txtAnimationSpeed.Value;
      Settings.BulbapediaURL = txtBulbapedia.Text;
      Settings.DefaultCurveSpacing = txtCurveSpacing.Value;
      Settings.DefaultMaxCurves = txtMaxCurves.Value;

      Settings.VerboseLogging = chkVerbose.IsChecked.Value;
      Settings.SortOnExport = chkSortExport.IsChecked.Value;
    }

    private void btnBrowseTypes_Click(object sender, RoutedEventArgs e)
    {

    }

    private void btnBrowseMoves_Click(object sender, RoutedEventArgs e)
    {

    }

    private void btnBrowsePokedex_Click(object sender, RoutedEventArgs e)
    {

    }

    private void btnBrowseLog_Click(object sender, RoutedEventArgs e)
    {

    }

    private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
    {
      Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
      e.Handled = true;
    }

    private void btnResetAll_Click(object sender, RoutedEventArgs e)
    {
      var result = MessageBox.Show("Do you really want to reset all settings to default values?  This cannot be undone.", "Reset to defaults", MessageBoxButton.YesNo);
      if (result == MessageBoxResult.Yes)
      {
        Settings.Reset();
        SynchSettings();
      }
    }

    private void UserControl_Unloaded(object sender, RoutedEventArgs e)
    {
      SaveSettings();
      Settings.SaveToFile();
    }
  }
}
