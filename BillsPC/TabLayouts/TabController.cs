﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BillsPC
{
  public interface DataExporter
  {
    void ExportAllData(string filename);
    void ImportAllData(string filename);
  }

  [JsonObject(MemberSerialization.OptIn)]
  public abstract class TabController : DataExporter
  {
    public bool Initialized { get; protected set; }
    public bool Busy { get; protected set; }
    public bool Dirty { get; set; }

    public DirtyDetector DD { get; protected set; }

    protected WindowCoordinator WC { get; set; }

    public TabController()
    {
      DD = new DirtyDetector();
      WC = WindowCoordinator.Instance;
    }

    public virtual bool IsDirty()
    {
      return Dirty;
    }

    public virtual void SetDirty(string name)
    {
      Dirty = true;
    }

    public abstract void ExportAllData(string filename);
    public abstract void ImportAllData(string filename);
    public virtual void ExportSingleData(string filename) { }
    public virtual void ImportSingleData(string filename) { }

    public virtual void ResetFields() { }
    public virtual void SyncDataToFields() { }
    public virtual void SaveFieldsToData() { }
  }
}
