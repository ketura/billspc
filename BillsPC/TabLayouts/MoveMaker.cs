﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillsPC.TabLayouts;
using Newtonsoft.Json;

namespace BillsPC
{
  public enum DamageType { Contact, Projectile, Energy, Mental }
  public enum AimType { None, Self, Tile, Sentry, Target, AoE, Terrain}
  public enum MoveAttribute { Power, Difficulty, Duration, SkillScale, Accuracy, InitiativeCost, BaseStamina, StaminaUse, FatiguePenalty, Flinch}

  public class PokeMove
  {
    public string Name;
    [JsonProperty]
    public string Notes { get; set; }
    public DamageType Damage;
    public int Speed;
    public uint Difficulty;
    public float Accuracy;
    public int BasePower;
    public int BaseStamina;
    public int StaminaUse;
    public float Penalty;

    public PercentDict<Stat> OffensiveStats;
    public PercentDict<Stat> DefensiveStats;
    public PercentDict<PokeType> Types;

  }

  public class MoveMaker : TabController
  {
    public MoveMakerTab main { get; private set; }

    public Dictionary<string, PokeMove> Moves { get; private set; }

    private string _currentMoveName;
    public string CurrentMoveName
    {
      get { return _currentMoveName; }
      set { if (!Busy) SwitchMove(value); }
    }

    public PokeMove CurrentMove
    {
      get { return CurrentMoveName == null ? null : Moves[CurrentMoveName]; }
      set { if (CurrentMoveName != null) Moves[CurrentMoveName] = value; }
    }

    public MoveMaker()
    {
      Moves = new Dictionary<string, PokeMove>();
      WindowCoordinator.Instance.MM = this;
    }

    public void Initialize(MoveMakerTab mm)
    {
      main = mm;
    }

    public void SwitchMove(string move)
    {
      Busy = true;
      _currentMoveName = move;
      CurrentMove = Moves[move];
      ResetBoard(CurrentMove);
      Busy = false;
    }

    public void AddMove(string move)
    {
      //Moves[move] = new PokeMove();
      //main.cbMoves.Items.Add(move);
    }

    public void RemoveMove(string move)
    {
      //if (Moves.Keys.Contains(move))
      //{
      //  Moves.Remove(move);
      //  main.cbMoves.Items.Remove(move);
      //  ResetBoard();
      //}
    }

    public void ResetBoard(PokeMove move)
    {
      //main.cbSecondaryTypePicker.SelectedItem = pokemon.PrimaryType();
      //main.cbGenderType.SelectedItem = pokemon.Gender;
      //main.txtTypePercent.Text = (pokemon.Types[pokemon.PrimaryType()] * 100).ToString();

      //foreach (var stat in main.SpeciesBoxes.Keys)
      //{
      //  main.SpeciesBoxes[stat].Text = pokemon.BaseStats[stat].ToString("##0");
      //}

    }

    public void ResetBoard()
    {
      //main.cbGenderType.SelectedItem = null;
      //main.cbSecondaryTypePicker.SelectedItem = null;
      //main.txtTypePercent.Text = "";

      //foreach (var box in main.SpeciesBoxes.Values)
      //{
      //  box.Text = "0";
      //}
    }

    public override void ExportAllData(string filename)
    {
      throw new NotImplementedException();
    }

    public override void ImportAllData(string filename)
    {
      throw new NotImplementedException();
    }
  }
}
