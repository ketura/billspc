﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using BillsPC.Controls;
using System.Collections.ObjectModel;

namespace BillsPC.TabLayouts
{
  public partial class TypeTemplateTab : UserControl
  {
    public TypeTemplate TT { get; set; }

    public Dictionary<PokeType, PercentTextLabel> TypeBoxes { get; private set; }
    private List<PercentTextLabel> TypeFields;

    public Dictionary<PokeType, PercentTextLabel> DefenseBoxes { get; private set; }
    private List<PercentTextLabel> DefenseFields;

    public Dictionary<PokeType, PickerLabel> AffinityBoxes { get; private set; }
    private List<PickerLabel> AffinityFields;

    public static readonly string FilenameFilter = "Type Definition file (*.type)|*.type|JSON file (*.json)|*.json|All Files (*.*)|*.*";

    public TypeTemplateTab()
    {
      //Instantiate the tab controller object and get all the controls in order.
      TT = new TypeTemplate();
      InitializeComponent();

      DamageHeader.SetContent(TypePanel);
      DefenseHeader.SetContent(DefensePanel);
      AffinityHeader.SetContent(AffinityPanel);

      //Configure the import/export buttons to have all the proper events bound and fields set.

      btnsImportExport.ImportDirtyCheck += TT.IsDirty;
      btnsImportExport.OnImportFileSelected += TT.ImportAllData;
      btnsImportExport.OnExportFileSelected += TT.ExportAllData;
      btnsImportExport.AutoSaveCheck = true;
      btnsImportExport.FilenameFilter = FilenameFilter;

      btnsImportExportSingle.ImportDirtyCheck += TT.IsDirty;
      btnsImportExportSingle.OnImportFileSelected += TT.ImportSingleData;
      btnsImportExportSingle.OnExportFileSelected += TT.ExportSingleData;
      btnsImportExportSingle.AutoSaveCheck = false;
      btnsImportExportSingle.FilenameFilter = FilenameFilter;

      //Generate and bind all of the damage textboxes

      TypeFields = new List<PercentTextLabel>();

      TypeBoxes = new Dictionary<PokeType, PercentTextLabel>();
      foreach (PokeType type in new PokeType().GetValues())
      {
        TypeBoxes[type] = new PercentTextLabel();
        TypeBoxes[type].LabelName = type.GetDescription();
        TypeBoxes[type].Value = 1;
        TypeBoxes[type].MaxValue = 999;
        TypeBoxes[type].MaxDigits = 3;

        TypeFields.Add(TypeBoxes[type]);
      }

      TypePanel.ItemsSource = TypeFields;

      //Generate and bind all of the defense textboxes

      DefenseFields = new List<PercentTextLabel>();

      DefenseBoxes = new Dictionary<PokeType, PercentTextLabel>();
      foreach (PokeType type in new PokeType().GetValues())
      {
        DefenseBoxes[type] = new PercentTextLabel();
        DefenseBoxes[type].LabelName = type.GetDescription();
        DefenseBoxes[type].Value = 1;
        DefenseBoxes[type].MaxValue = 999;
        DefenseBoxes[type].MaxDigits = 3;

        DefenseFields.Add(DefenseBoxes[type]);
      }

      DefensePanel.ItemsSource = DefenseFields;

      //Generate and bind all of the affinity comboboxes.

      AffinityFields = new List<PickerLabel>();
      AffinityBoxes = new Dictionary<PokeType, PickerLabel>();

      foreach(PokeType type in new PokeType().GetValues())
      {
        AffinityBoxes[type] = new PickerLabel();
        AffinityBoxes[type].LabelName = type.GetDescription();
        AffinityBoxes[type].cbPicker.ItemsSource = new Affinity().GetValues();
        AffinityBoxes[type].cbPicker.SelectedItem = Affinity.Neutral;
        AffinityBoxes[type].PickerWidth = 65;

        AffinityFields.Add(AffinityBoxes[type]);
      }

      AffinityPanel.ItemsSource = AffinityFields;

      //And don't let the main combobox go empty.
      cbTypePicker.ItemsSource = new PokeType().GetValues();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      TT.Initialize(this);
    }

    //wrapper for the tabcontroller's selection changing handler.
    private void cbTypePicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (cbTypePicker.SelectedItem == null)
        return;

      TT.SwitchCurrentType((PokeType)(sender as ComboBox).SelectedItem);
    }
  }
}
