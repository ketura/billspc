﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace RPokemon
{

  public enum EType
  {
    None, Normal, Fighting, Flying, Poison, Ground, Rock, Bug, Ghost,
    Steel, Fire, Water, Grass, Electric, Psychic, Ice, Dragon, Dark, Fairy
  };



  public enum Attribute { Height, Weight, Independence, Respect, SoulLink, };

  public struct TypePercent
  {
    public EType Type;
    public float Amount;
  }



  public struct Attributes
  {
    public int Height { get; set; }
    public int Weight { get; set; }
    public int Independence { get; set; }
    public int Respect { get; set; }
    public int SoulLink { get; set; }
  }

  public class Composite
  {
    Dictionary<string, int> Base { get; set; }
    Dictionary<string, int> IV { get; set; }
    Dictionary<string, int> EV { get; set; }

    public float IVFactor { get; set; }
    public float EVFactor { get; set; }

    public Composite(List<string> traits)
    {
      Base = new Dictionary<string, int>();
      IV = new Dictionary<string, int>();
      EV = new Dictionary<string, int>();

      foreach (string t in traits)
      {
        Base.Add(t, 0);
        IV.Add(t, 0);
        EV.Add(t, 0);
      }
    }

    public virtual int this[string s]
    {
      get
      {
        return Base[s] + (int)Math.Floor(IV[s] * IVFactor) + (int)Math.Floor(EV[s] * EVFactor);
      }
    }
  }

  //CompositeMask

  public class Species 
  {
    public string Name;
    public TypePercent[] Types;

    public Composite Stats;
    public Composite Attributes;


    void Start()
    {

    }

    void Update()
    {

    }
  }
}
