﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BillsPC.Controls;
using Newtonsoft.Json;

namespace BillsPC
{
  //(?s)<tr.*?>.*?title="(\w+) .*?(<td( style="background:#\w+")?> (\d+)\n</td>\n?)+</tr>
  //(?s)<tr.*?>.*?<b>(\d+)</b>.*?title="(\w+) .*?<td style="background:#\w+"> (\d+)\r\n</td>\r\n<td style="background:#\w+"> (\d+)\r\n</td>\r\n<td style="background:#\w+"> (\d+)\r\n</td>\r\n<td style="background:#\w+"> (\d+)\r\n</td>\r\n<td style="background:#\w+"> (\d+)\r\n</td>\r\n<td style="background:#\w+"> (\d+)\r\n</td>\r\n<td> (\d+)\r\n</td>\r\n<td> ([\d.]+)\r\n</td></tr>
  //^ for bulbapedia; ten groups
  
  public class WindowCoordinator : Singleton<WindowCoordinator>
  {
    private TypeTemplate _tt;
    private SpeciesSynthesis _ss;
    private MoveMaker _mm;
    public TypeTemplate TT
    {
      get { return _tt; }
      set
      {
        _tt = value;
        string typedef = Path.Combine(StartupPath, CurrentSettings.DefaultTypes);
        if (File.Exists(typedef))
          _tt.ImportAllData(typedef);
      }

    }
    public SpeciesSynthesis SS
    {
      get { return _ss; }
      set
      {
        _ss = value;
        string calibration = Path.Combine(StartupPath, CurrentSettings.DefaultPokedex);
        if(File.Exists(calibration))
          _ss.ImportAllData(calibration);
      }

    }
    public MoveMaker MM { get; set; }
    public List<ChartComparison> ChartWindows { get; protected set; }

    private static string _path;
    public static string StartupPath
    {
      get
      {
        if(string.IsNullOrEmpty(_path))
        {
          _path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }
        return _path;
      }
    }

    public Settings CurrentSettings { get; set; }

    public WindowCoordinator()
    {
      ChartWindows = new List<ChartComparison>();
    }

    public ChartComparison AddChartWindow(SpeciesDefinition pokemon)
    {
      ChartComparison newchart = new ChartComparison(pokemon, SS.Species);
      ChartWindows.Add(newchart);
      return newchart;
    }

    public void CloseChartWindow(ChartComparison window)
    {
      if (ChartWindows.Contains(window))
        ChartWindows.Remove(window);

      window.Close();
    }

    public void CloseAllWindows()
    {
      foreach(var window in ChartWindows)
      {
        window.Close();
      }
      ChartWindows.Clear();
    }

  }

  public class PokemonCache
  {
    public Dictionary<string, Tuple<int, StatPage>> Definitions;
    public DateTime DatePulled;

    public PokemonCache()
    {
      Definitions = new Dictionary<string, Tuple<int, StatPage>>();
      DatePulled = DateTime.Now;
    }
  }
}
