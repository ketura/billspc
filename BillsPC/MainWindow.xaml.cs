﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using log4net;
using BillsPC.TabLayouts;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace BillsPC
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    WindowCoordinator i;

    public MainWindow()
    {
      InitializeComponent();
      i = WindowCoordinator.Instance;
    }

    private void Grid_Loaded(object sender, RoutedEventArgs e)
    {

    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      WindowCoordinator.Instance.SS.CloseAllCharts();
    }



    ////Makes it so mousewheel will scroll on the last focused combobox.
    //protected override void OnMouseWheel(MouseWheelEventArgs e)
    //{
    //  IInputElement element = FocusManager.GetFocusedElement(this);
    //  if (element != null && e.Source != element)
    //  {
    //    MouseWheelEventArgs args = new MouseWheelEventArgs(Mouse.PrimaryDevice, e.Timestamp, e.Delta) { RoutedEvent = UIElement.MouseWheelEvent };
    //    element.RaiseEvent(args);
    //    e.Handled = true;
    //  }
    //  base.OnMouseWheel(e);
    //}



  }
}
