﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace BillsPC
{
  

  public class PokeTypes
  {
    public TypeChart Damage;
    public TypeChart Resistance;
    public TypeChart Accuracy;

    public PokeTypes()
    {
      Initialize();
    }

    protected void Initialize()
    {
      Damage = new TypeChart();
      Resistance = new TypeChart();
      Accuracy = new TypeChart();

      foreach (PokeType type in AllTypes())
      {
        Damage.Add(type, null);
        Resistance.Add(type, null);
        Accuracy.Add(type, null);
      }
    }

    void Start()
    {
      foreach (var x in Damage.Keys)
      {
        if (Damage[x] == null)
          continue;

        foreach (var type in Damage[x].Keys)
        {
          Console.WriteLine("Type: " + type + ", Amount: " + Damage[x][type]);
        }
      }
    }

    public void AddDamage(PokeType type, TypeSpread dam)
    {
      Damage[type] = dam;
    }

    public void AddResistance(PokeType type, TypeSpread res)
    {
      Resistance[type] = res;
    }

    public void AddAccuracy(PokeType type, TypeSpread acc)
    {
      Accuracy[type] = acc;
    }

    public float DamageTo(PokeType source, PokeType to)
    {
      return Damage[source][to];
    }

    public float ResistanceTo(PokeType source, PokeType to)
    {
      return Resistance[source][to];
    }

    public float AccuracyTo(PokeType source, PokeType to)
    {
      return Accuracy[source][to];
    }

    private static List<PokeType> _allTypes;

    public static List<PokeType> AllTypes()
    {
      if (_allTypes != null)
        return _allTypes;

      List<PokeType> types = new List<PokeType>();
      foreach (var type in System.Enum.GetValues(typeof(PokeType)))
      {
        types.Add((PokeType)type);
      }
      _allTypes = types;
      return _allTypes;
    }
  }
}