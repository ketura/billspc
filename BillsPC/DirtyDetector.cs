﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace BillsPC
{
  /// <summary>
  /// Permits looping through a control's children.  Taken from http://stackoverflow.com/a/30020287/888539
  /// </summary>
  public static class VisualExtensions
  {
    public static IEnumerable<Visual> GetControls(this Visual parent, bool recurse = true)
    {
      if (parent != null)
      {
        //Added this to streamline the functions I'm using it for, but this might not actually be
        // wanted behavior in the more general sense
        yield return parent;

        int count = VisualTreeHelper.GetChildrenCount(parent);
        for (int i = 0; i < count; i++)
        {
          // Retrieve child visual at specified index value.
          var child = VisualTreeHelper.GetChild(parent, i) as Visual;

          if (child != null)
          {
            if(child is Control)
              yield return child;

            if (recurse)
            {
              foreach (var grandChild in child.GetControls(true))
              {
                if(grandChild is Control)
                  yield return grandChild;
              }
            }
          }
        }
      }
    }
  }

  /// <summary>
  /// A helper class for easy triggering of the Dirty state.  This can be passed controls that need to be watched
  /// and all pertinant controls will have their various StateChanged events subscribed to and wrapped around
  /// a given delegate function.
  /// </summary>
  public class DirtyDetector
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public event Action<string> OnControlChange;

    private List<Control> WatchedControls;

    public DirtyDetector()
    {
      WatchedControls = new List<Control>();
    }

    /// <summary>
    /// Searches the passed control and all child controls for supported composite usercontrols.  This is primarily
    /// intended to allow for broad-stroke filtering of all non-useful controls such as Borders, Labels, etc, and
    /// only search composite controls, since those make up the vast majority of data-bound controls that we are
    /// interested in.  However, if any single controls exist that need to be watched, WatchControl will simply need
    /// to be called for each orphaned controls in question.
    /// </summary>
    /// <param name="control">
    /// The control hierarchy to search.
    /// </param>
    /// <param name="recurse">
    /// If set to false, only the passed control and its children will be searched, with any grandchildren 
    /// that might be supported ignored.
    /// </param>
    public void WatchAllCustomControls(ContentControl control, bool recurse=true)
    {
      Log.Debug("Adding children for " + control.ToString());
      foreach (var c in control.GetControls(recurse))
      {
        if (c is UserControl)
        {
          WatchControl((UserControl)c, recurse);
        }
      }
    }

    /// <summary>
    /// Searches the passed control and all child controls for supported subscribable events.
    /// </summary>
    /// <param name="control">
    /// The control hierarchy to search.
    /// </param>
    /// <param name="recurse">
    /// If set to false, only the passed control and its children will be searched, with any grandchildren 
    /// that might be supported ignored.
    /// </param>
    public void WatchControl(ContentControl control, bool recurse=true)
    {
#pragma warning disable CS0219 // Variable is assigned but its value is never used
      bool log = false;
#pragma warning restore CS0219 // Variable is assigned but its value is never used

      foreach (var c in control.GetControls(recurse))
      {
        if (WatchedControls.Contains(c))
          continue;

        if (c is TextBox)
        {
          ((TextBox)c).TextChanged += TextBoxWatcher;
          WatchedControls.Add((Control)c);
          log = true;
        }

        if(c is ComboBox)
        {
          ((ComboBox)c).SelectionChanged += ComboBoxWatcher;
          WatchedControls.Add((Control)c);
          log = true;
        }

        if(c is Button)
        {
          ((Button)c).Click += ButtonWatcher;
          WatchedControls.Add((Control)c);
          log = true;
        }

        //if(log) Log.Debug("Watching " + c.ToString());
      }
    }

    /// <summary>
    /// Invokes the subscribed tabcontroller upon any control update.
    /// </summary>
    /// <param name="c">
    /// The control made dirty.
    /// </param>
    private void SetDirty(Control c)
    {
      //Log.Debug(c.ToString() + " is dirty!");
      OnControlChange?.Invoke(c.Name);
    }

    /// <summary>
    /// The generic wrapper for button Click events.
    /// </summary>
    private void ButtonWatcher(object sender, RoutedEventArgs e)
    {
      SetDirty(sender as Control);
    }

    /// <summary>
    /// The generic wrapper for combobox SelectionChanged events.
    /// </summary>
    private void ComboBoxWatcher(object sender, SelectionChangedEventArgs e)
    {
      SetDirty(sender as Control);
    }

    /// <summary>
    /// The generic wrapper for textbox TextChanged events.
    /// </summary>
    private void TextBoxWatcher(object sender, TextChangedEventArgs e)
    {
      SetDirty(sender as Control);
    }

  }
}
