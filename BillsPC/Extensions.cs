﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BillsPC
{
  public static class Extensions
  {
    //taken from http://stackoverflow.com/questions/12996983/how-to-change-the-position-of-an-element-in-stackpanel
    public static void RepositionChild(this StackPanel stack, UIElement control, int newIndex)
    {
      if (!stack.Children.Contains(control) || newIndex < 0)
        return;

      stack.Children.RemoveAt(stack.Children.IndexOf(control));

      stack.AddChildAt(control, newIndex);
    }

    public static void AddChildAt(this Panel stack, UIElement control, int index)
    {
      if (index < stack.Children.Count)
      {
        stack.Children.Insert(index, control);
      }
      else
      {
        stack.Children.Add(control);
      }
    }
  }
}
