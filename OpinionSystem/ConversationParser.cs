﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace OpinionSystem
{
	public struct PlayerMessage
	{
		public string MessageType;
		public string TypeVariant;
		public IEnumerable<string> Body;
	}

	public class ConversationParser
	{
		public static readonly string QueryKey = "Query";
		public static readonly string DeclareKey = "Declare";
		public static readonly string StopWordKey = "StopWords";

		public Dictionary<string, HashSet<string>> InputCommands { get; protected set; } 
		public HashSet<string> QueryCommands
		{
			get
			{
				return InputCommands[QueryKey];
			}

			protected set
			{
				InputCommands[QueryKey] = value;
			}
		}

		public HashSet<string> DeclareCommands
		{
			get
			{
				return InputCommands[DeclareKey];
			}

			protected set
			{
				InputCommands[DeclareKey] = value;
			}
		}

		public HashSet<string> StopWords
		{
			get
			{
				return InputCommands[StopWordKey];
			}

			protected set
			{
				InputCommands[StopWordKey] = value;
			}
		}

		public string QueryRegex { get; protected set; }
		public string DeclareRegex { get; protected set; }

		public ConversationParser()
		{
			InputCommands = new Dictionary<string, HashSet<string>>();
			QueryCommands = new HashSet<string>();
			DeclareCommands = new HashSet<string>();
			StopWords = new HashSet<string>();
		}

		public PlayerMessage? Parse(string input)
		{
			List<string> list = new List<string>();

			foreach(string s in FilterStopWords(input).Split(' '))
			{
				if(!string.IsNullOrWhiteSpace(s))
					list.Add(s.Trim());
			}

			foreach (string variant in QueryCommands)
			{
				if (variant == list.First())
				{
					return new PlayerMessage() { MessageType = QueryKey, TypeVariant = variant, Body = list.Where((c, i) => i != 0) };
				}
			}

			foreach (string variant in DeclareCommands)
			{
				if (variant == list.First())
				{
					return new PlayerMessage() { MessageType = DeclareKey, TypeVariant = variant, Body = list.Where((c, i) => i != 0) };
				}
			}

			return null;
      //return "I don't know anything about that";
		}

		public string FilterStopWords(string input)
		{
			foreach(string s in StopWords)
			{
				string regex = @"\b" + s + @"\b";
				input = Regex.Replace(input, regex, "");
			}

			return input;
		}

    public string ProcessQuery(List<string> list)
    {
			return "";
    }
    public void ProcessDeclare(List<string> list)
    {

    }

  }
}
