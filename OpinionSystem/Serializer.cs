﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace OpinionSystem
{


	public static class Serializer
	{
		public static void SaveItem(object obj, string filename)
		{
			SaveItem<object>(obj, filename);
		}

		public static void SaveItem<T>(T obj, string filename)
		{
			string output = JsonConvert.SerializeObject(obj, Formatting.Indented);
			WriteJSON(filename, output);
		}

		public static void SaveItemList(object obj, string filename)
		{
			SaveItemList<object>(obj, filename);
		}

		public static void SaveItemList<T>(T obj, string filename)
		{
			List<T> list = new List<T>() { obj };
			string output = JsonConvert.SerializeObject(obj, Formatting.Indented);
			WriteJSON(filename, output);
		}

		public static void SaveItemDictionary(object key, object value, string filename)
		{
			SaveItemDictionary<object, object>(key, value, filename);
		}

		public static void SaveItemDictionary<K, V>(K key, V value, string filename)
		{
			Dictionary<K, V> dict = new Dictionary<K, V>() { { key, value } };
			string output = JsonConvert.SerializeObject(dict, Formatting.Indented);
			WriteJSON(filename, output);
		}

		public static void WriteJSON(string filename, string output)
		{
			File.WriteAllText(filename, output);
		}

		public static T Load<T>(string filename)
		{
			string input = File.ReadAllText(filename);
			return JsonConvert.DeserializeObject<T>(input);
		}

		public static T LoadOrNew<T>(string filename)
			where T : new()
		{
			if (File.Exists(filename))
				return Load<T>(filename);

			return new T();
		}

		public static T LoadOrDefault<T>(string filename)
		{
			if (File.Exists(filename))
				return Load<T>(filename);

			return default(T);
		}


	}
}
