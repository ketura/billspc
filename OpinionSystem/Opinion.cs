﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OpinionSystem
{
  public struct Opinion
  {
		public string Topic;
    public string Value;
    public int Fervor;
    public int Reaction;
    public int Novelty;
		public Dictionary<int, int> Sources;
  }

	public class WordAssociation
	{
		public Dictionary<string, int> Words { get; protected set; } = new Dictionary<string, int>() ;

		public WordAssociation() { }
	}
}
