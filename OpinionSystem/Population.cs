﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpinionSystem
{
  public class Population
  {
    public string Name { get; set; }
    public int StartingPop { get; set; }
    public int Count { get; set; }
    public int Connections { get; set; }
    public float SocialSpread { get; set; } 

    public Dictionary<int, NPC> NPCs { get; protected set; }

    public Population(int pop, int connections=0, float spread=0.5f)
    {
      StartingPop = pop;

      NPCs = new Dictionary<int, NPC>(StartingPop);
      Connections = connections;
      if (Connections == 0)
        Connections = (int)(connections / 2.0);
      SocialSpread = spread;

      Init();
    }

    public void Init()
    {
      NameGenerator namegen = new NameGenerator();
      for(int i = 0; i < StartingPop; i++)
      {
        var name = namegen.GetRandomName();
        NPC npc = new NPC()
        {
          FirstName = name[0],
          LastName = name[1]
        };

        NPCs[npc.ID] = npc;
        Count++;

      }
    }
    public override string ToString()
    {
      return Name + " (pop. " + Count + ")";
    }
  }
}
