﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace OpinionSystem
{
  public class NPC
  {
    public static readonly int ContactBias = 10;
    public static readonly float BoredomFactor = 0.98f;

    public int ID { get; private set; }

    private static int CurrentID;
    private static int NextID()
    {
      return ++CurrentID;
    }

    public string Name
    {
      get { return FirstName + " " + LastName; }
    }

    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Occupation { get; set; }
		public string Greeting { get; protected set; }
		public string Farewell { get; protected set; }

    public string ChatTopic { get; set; }
    public Dictionary<string, Opinion> Opinions { get; protected set; }

    public Dictionary<NPC, int> Contacts { get; protected set; }
    public Dictionary<NPC, int> People { get; protected set; }

    public NPC()
    {
      ID = NextID();

      FirstName = "Joey";
      LastName = "Parker";
      Occupation = "Layabout";

      Contacts = new Dictionary<NPC, int>(150);
      People = new Dictionary<NPC, int>();
      Opinions = new Dictionary<string, Opinion>();
			Opinions["food"] = new Opinion() { Topic = "food", Value = "JOEY DOESN'T SHARE FOOD", Reaction = 0, Fervor = 100, Novelty = 5000 };
    }

		public string GreetingOrDefault()
		{
			if (!string.IsNullOrEmpty(Greeting))
				return Greeting;

			return "Hi, my name is " + FirstName + ".";
		}

		public string FarewellOrDefault()
		{
			if (!string.IsNullOrEmpty(Farewell))
				return Farewell;

			return "Bye!";
		}

		public void HearOpinion(Opinion opinion, NPC source)
    {
      int? bias = GetOpinionOfPerson(source);

      if (!bias.HasValue)
        bias = 100;

      if(Opinions.ContainsKey(opinion.Value))
      {
        Opinion op = Opinions[opinion.Value];
        op.Reaction++;
        

      }

    }

    public int? GetOpinionOfPerson(NPC person)
    {
      return GetPersonalOpinionOfPerson(person) ?? GetNetworkOpinionOfPerson(person);
    }

    public int? GetPersonalOpinionOfPerson(NPC person)
    {
      if (Contacts.ContainsKey(person))
      {
        return Contacts[person] * ContactBias;
      }
      else if (People.ContainsKey(person))
      {
        return People[person];
      }

      return null;
    }

    public int? GetNetworkOpinionOfPerson(NPC person)
    {
      List<int> opinions = new List<int>();
      foreach(NPC contact in Contacts.Keys)
      {
        int? opinion = contact.GetPersonalOpinionOfPerson(person);
        if (opinion.HasValue)
          opinions.Add(opinion.Value);
      }

      if (opinions.Count == 0)
        return null;

      return opinions.Sum() / opinions.Count;
    }

    public override string ToString()
    {
      return ID + ": " + Name;
    }
  }
}
