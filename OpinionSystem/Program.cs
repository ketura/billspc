﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OpinionSystem
{
  class Program
  {
    static void Main(string[] args)
    {
			//Population pop = new Population(10) { Name = "Metropolis", StartingPop = 10 };
			//string output = "";

			//output += pop.ToString() + "\n";

			//foreach(var pair in pop.NPCs)
			//{
			//  output += pair.Value.ToString() + "\n";
			//}

			//File.WriteAllText("output.txt", output.Replace("\n", "\r\n"));

			//Console.WriteLine(output);
			var wa = Serializer.LoadOrDefault<WordAssociation>("Data/words.JSON");
			Serializer.SaveItem(wa, "Data/words.JSON");

			NPC joey = Serializer.LoadOrDefault<NPC>("Data/Joey.JSON");
      //Serializer.SaveItem(joey, "Data/Joey.JSON");

      var cp = Serializer.LoadOrDefault<ConversationParser>("Data/ConversationCommands.JSON");
			Serializer.SaveItem(cp, "Data/ConversationCommands.JSON");

			Console.WriteLine("What is your name?");
			string name = Console.ReadLine();
			int hash = name.GetHashCode();

			Console.WriteLine(joey.GreetingOrDefault());
			Console.Write(">");

			var parseResult = cp.Parse(Console.ReadLine());
			foreach (string s in parseResult.Value.Body)
				Console.WriteLine(s);

			Console.WriteLine(joey.FarewellOrDefault());

			Console.ReadLine();
    }
  }
}
