﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OpinionSystem
{
  public class NameGenerator
  {
    public static string NameListFilename = "Data/names.txt";

    public List<string> FirstNames { get; protected set; }
    public List<string> LastNames { get; protected set; }

    private Random randgen;

    public NameGenerator()
    {
      FirstNames = new List<string>();
      LastNames = new List<string>();
      randgen = new Random();

      using (StreamReader sr = new StreamReader(NameListFilename))
      {
        string names = sr.ReadToEnd();
        foreach(string line in names.Split('\n'))
        {
          string[] fullname = line.Split('\t');
          FirstNames.Add(fullname[0]);
          LastNames.Add(fullname[1]);
        }
      }
    }

    public string[] GetRandomName()
    {
      int int1 = randgen.Next(0, FirstNames.Count - 1);
      int int2 = randgen.Next(0, LastNames.Count - 1);

      return new string[2] { FirstNames[int1], LastNames[int2] };
    }
  }
}
