﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Testbed
{
  public enum Stat
  {
    [Description("Hit Points")]
    HP, Endurance, Attack, Defense,
    [Description("Sp. Atk")]
    SpecialAttack,
    [Description("Sp. Def")]
    SpecialDefense, Resistance, Weakness,
    Speed, Initiative, Accuracy, Evasion, Critical,
    Height, Weight, SoulLink, Respect,
    Experience, Evolution, Gender, Temperament,
    [Description("Catch Rate")]
    CatchRate
  }

  public class StatPage : Dictionary<Stat, int>
  {
    public StatPage(bool fill = true) : base()
    {
      if (fill)
      {
        foreach (Stat stat in new Stat().GetValues())
        {
          Add(stat, 0);
        }
      }
    }
  }

  public class StatModifier : Dictionary<Stat, float>
  {
    public StatModifier()
    {
      foreach (Stat stat in new Stat().GetValues())
      {
        Add(stat, 1.0f);
      }
    }

    public StatModifier(float amount=0.2f) : base()
    {
      foreach (Stat stat in new Stat().GetValues())
      {
        Add(stat, amount);
      }
    }
  }

  public class Stats
  {
    public StatPage Base { get; set; }
    public StatPage IV { get; set; }
    public StatPage EV { get; set; }

    public float IVFactor { get; set; }
    public float EVFactor { get; set; }

    public Stats()
    {
      Base = new StatPage();
      IV = new StatPage();
      EV = new StatPage();
    }

    public int this[Stat stat]
    {
      get { return (int)Math.Floor(Base[stat] + (IVFactor * IV[stat]) + (EVFactor * EV[stat])); }
    }
  }

}