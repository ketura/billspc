﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Testbed
{
  public class PercentStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      float amount = 0;
      if (float.TryParse(value.ToString(), out amount))
      {
        Console.WriteLine("float parsed");
        return amount;
      }

      return 0;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is float)
      {
        Console.WriteLine("Found a float");
        return ((float)value).ToString("##0");
      }

      return "0";
    }
  }
}
