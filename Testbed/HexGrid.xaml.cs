﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Testbed
{
  public partial class HexGrid : UserControl
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    public bool GridMode
    {
      get { return (bool)GetValue(GridModeProperty); }
      set { SetValue(GridModeProperty, value); }
    }
    public static readonly DependencyProperty GridModeProperty = DependencyProperty.RegisterAttached("GridMode",
           typeof(bool), typeof(HexGrid), new PropertyMetadata(true, OnGridModePropertyChanged));

    private static void OnGridModePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      HexGrid h = d as HexGrid;
      h.Redraw();
    }


    public double Size
    {
      get { return (double)GetValue(SizeProperty); }
      set { SetValue(SizeProperty, value); }
    }
    public static readonly DependencyProperty SizeProperty = DependencyProperty.RegisterAttached("Size",
           typeof(double), typeof(HexGrid), new PropertyMetadata(50.0, OnSizePropertyChanged));

    private static void OnSizePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      HexGrid h = d as HexGrid;
      h.Redraw();
    }

    public int Rows
    {
      get { return (int)GetValue(RowsProperty); }
      set { SetValue(RowsProperty, value); }
    }
    public static readonly DependencyProperty RowsProperty = DependencyProperty.RegisterAttached("Rows",
           typeof(int), typeof(HexGrid), new PropertyMetadata(5, OnRowsPropertyChanged));

    private static void OnRowsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      HexGrid h = d as HexGrid;
      h.Redraw();
    }

    public int Columns
    {
      get { return (int)GetValue(ColumnsProperty); }
      set { SetValue(ColumnsProperty, value); Redraw(); }
    }
    public static readonly DependencyProperty ColumnsProperty = DependencyProperty.RegisterAttached("Columns",
           typeof(int), typeof(HexGrid), new PropertyMetadata(5, OnColumnsPropertyChanged));

    private static void OnColumnsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      HexGrid h = d as HexGrid;
      h.Redraw();
    }

    protected void Redraw()
    {
      Log.Debug("Rows: " + Rows + ", Columns: " + Columns);
      HexPanel.Children.Clear();
      for(int i = 0; i < Rows*2; i++)
      {
        HexRow row = new HexRow()
        {
          Count = Columns,
          Size = Size,
          MainColor = Brushes.Crimson,
          OffColor = Brushes.Gray
        };
        double left = 0;
        double top = 0;
        if (i != 0)
          top = Size * -0.5;
        if (i % 2 == 0)
          left = Size * 0.75;

        row.Margin = new Thickness(left, top, 0, 0);
        if (i % 2 == 0)
          row.OffColorTiles = new List<int>() { 3 };
        row.Redraw();
        HexPanel.Children.Add(row);
      }
    }

    public HexGrid()
    {
      InitializeComponent();
      Redraw();
    }
  }
}
