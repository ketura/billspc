﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Testbed
{
  public partial class HexRow : UserControl
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public Brush MainColor { get; set; }
    public Brush OffColor { get; set; }
    public Brush HighlightColor { get; set; }
    public int HighlightSize { get; set; }
    public List<int> OffColorTiles { get; set; }
    public List<int> HighlightTIles { get; set; }

    public double Size
    {
      get { return (double)GetValue(SizeProperty); }
      set { SetValue(SizeProperty, value); }
    }
    public static readonly DependencyProperty SizeProperty = DependencyProperty.RegisterAttached("Size",
           typeof(double), typeof(HexRow), new PropertyMetadata(50.0, OnSizePropertyChanged));

    private static void OnSizePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      HexRow h = d as HexRow;
      h.CountChanged(h.Count);
    }


    public int Count
    {
      get { return (int)GetValue(CountProperty); }
      set { SetValue(CountProperty, value); }
    }
    public static readonly DependencyProperty CountProperty = DependencyProperty.RegisterAttached("Count",
           typeof(int), typeof(HexRow), new PropertyMetadata(5, OnCountPropertyChanged));

    private static void OnCountPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      HexRow h = d as HexRow;
      int? b = e.NewValue as int?;
      h.CountChanged((int)b);
    }
    protected void CountChanged(int size)
    {
      Redraw();
    }

    public void Redraw()
    {
      RowPanel.Children.Clear();

      for (int i = 0; i < Count; i++)
      {
        Hex h = new Hex() { Size = Size, Color = MainColor };
        if(OffColorTiles.Contains(i))
        {
          h.Color = OffColor;
        }
        double margin = Size / 4;
        h.Margin = new Thickness(margin, 0, margin, 0);
        Log.Debug(h.Margin);
        RowPanel.Children.Add(h);
      }
    }

    public HexRow()
    {
      InitializeComponent();
      OffColorTiles = new List<int>();
      HighlightTIles = new List<int>();
    }
  }
}
