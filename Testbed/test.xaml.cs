﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.Defaults;

namespace Testbed
{
  public partial class test : UserControl
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    private int _stat;
    public int stat
    {
      get { return _stat; }
      set
      {
        _stat = value;
        GenerateValues();
      }
    }

    private int _multiplier;
    public int multiplier
    {
      get { return _multiplier; }
      set
      {
        _multiplier = value;
        GenerateValues();
      }
    }

    public autocontext AutoContext { get; set; }

    //private Dictionary<int>
    public test()
    {
      InitializeComponent();

      SeriesCollection = new SeriesCollection
      {
        new LineSeries
        {
            Title = "Max",
            //PointGeometry = null,
            PointGeometrySize = 4,
            Values = new ChartValues<ObservablePoint>(),
            FontFamily = new FontFamily("Segoe UI")
      //DataLabels = true,
      //LabelPoint = point => point.X + ": " + point.Y.ToString("###")
    },
        new LineSeries
        {
            Title = "Min",
            //PointGeometry = null,
            PointGeometrySize = 4,
            Values = new ChartValues<ObservablePoint>(),
            Fill = Brushes.White,
            FontFamily = new FontFamily("Segoe UI")
            //DataLabels = true,
            //LabelPoint = point => point.X + ": " + point.Y.ToString("###")
        },
        new LineSeries
        {
            Title = "Arceus",
            PointGeometry = null,
            Fill = Brushes.Transparent,
            Stroke = System.Windows.Media.Brushes.Gray,
            Values = new ChartValues<ObservablePoint>(),
            DataLabels = true,
            FontFamily = new FontFamily("Segoe UI")
        },
        new LineSeries
        {
            Title = "Arceus",
            PointGeometry = null,
            Fill = Brushes.Transparent,
            Stroke = System.Windows.Media.Brushes.Gray,
            Values = new ChartValues<ObservablePoint>(),
            DataLabels = true,
            FontFamily = new FontFamily("Segoe UI")
        },
        new LineSeries
        {
            Title = "Arceus",
            PointGeometry = null,
            Fill = Brushes.Transparent,
            Stroke = System.Windows.Media.Brushes.Gray,
            Values = new ChartValues<ObservablePoint>(),
            DataLabels = true,
            FontFamily = new FontFamily("Segoe UI")
        }
      };

      ((LineSeries)SeriesCollection[1]).Fill = new SolidColorBrush(Colors.White);
      ((LineSeries)SeriesCollection[1]).Fill.Opacity = 0.75;

      Chart.DisableAnimations = true;
      //Chart.AnimationsSpeed = new TimeSpan(10000);
      Chart.DataTooltip = null;

      //https://en.wikipedia.org/wiki/Logistic_function

      //SeriesCollection[2].Values.Add(new ObservablePoint(0,  5));
      //SeriesCollection[2].Values.Add(new ObservablePoint(50, ((2.0 * 999) * 50) / 100 + 5));
      //SeriesCollection[2].Values.Add(new ObservablePoint(100, (999 * 100) / 100 + 5));

      _stat = 10;

      YFormatter = value => value.ToString("0");
      XFormatter = value => (value * 1).ToString("0");

      AutoContext = new autocontext();
      GenerateValues();

      

      DataContext = this;
    }
    //http://snoopwpf.codeplex.com/
    public void GenerateValues()
    {
      var pokemon = AutoContext.GetClosest(Stat.Attack, _stat);
      ((LineSeries)SeriesCollection[2]).Title = "Bug";
      ((LineSeries)SeriesCollection[3]).Title = "?";
      ((LineSeries)SeriesCollection[4]).Title = "Dragon";
      SeriesCollection[2].LabelPoint = point => point.X == 50 ? ((LineSeries)SeriesCollection[2]).Title : "";
      SeriesCollection[3].LabelPoint = point => point.X == 50 ? ((LineSeries)SeriesCollection[3]).Title : "";
      SeriesCollection[4].LabelPoint = point => point.X == 50 ? ((LineSeries)SeriesCollection[4]).Title : "";

      //add largest var, add to them as generated, then compare to AutoContext
      SeriesCollection[0].Values.Clear();
      SeriesCollection[1].Values.Clear();
      SeriesCollection[2].Values.Clear();
      SeriesCollection[3].Values.Clear();
      SeriesCollection[4].Values.Clear();
      for (int i = 0; i <= 101; i+=10)
      {
        //Log.Debug("upper:" + i);
        //SeriesCollection[0].Values.Add(new ObservablePoint(i, (_multiplier * (_stat * 1.2) / (1 + Math.Pow(Math.E, -0.05 * (i - 50))))));
        //Log.Debug("lower:" + i);
        //SeriesCollection[0].Values.Add(new ObservablePoint(i, (Math.Min((_stat*1.2) / 55 * i, (_stat * 1.2) * .8)) + ((300 + (_stat * 1.2)) / (1 + Math.Pow(Math.E, -0.055 * (i - 45))))));
        SeriesCollection[1].Values.Add(new ObservablePoint(i, (Math.Min((_stat * 0.8) / 55 * i, (_stat * 0.8) * .8)) + ((300 + (_stat * 0.8)) / (1 + Math.Pow(Math.E, -0.055 * (i - 45))))));
        //SeriesCollection[1].Values.Add(new ObservablePoint(i, _stat * i/10 + ((400 + _stat*.1) / (1 + Math.Pow(Math.E, -0.060 * (i - 70))))));

        SeriesCollection[2].Values.Add(new ObservablePoint(i, (Math.Min(_stat / 9.0 * i, _stat)) + ( 2.65 * i )));
        //SeriesCollection[2].Values.Add(new ObservablePoint(i, _stat/3 + (40 * Math.Log(1+i, 3))));
        //SeriesCollection[2].Values.Add(new ObservablePoint(i, _stat/3 + (50 * Math.Pow(i, 0.3))));
        SeriesCollection[3].Values.Add(new ObservablePoint(i, (Math.Min(_stat / 35.0 * i, _stat)) + (Math.Pow(3 + i/ 20, 3)) - Math.Max((i-75)*8, (10 + i/20))));
        SeriesCollection[4].Values.Add(new ObservablePoint(i, (Math.Min(_stat / 40.0 * i, _stat )) + (Math.Pow(i/5.0, 2)) ));
        //SeriesCollection[2].Values.Add(new ObservablePoint(i, (_multiplier * (pokemon.Item1.BaseStats[Stat.Attack]) / (1 + Math.Pow(Math.E, -0.05 * (i - 50))))));
        //SeriesCollection[3].Values.Add(new ObservablePoint(i, (_multiplier * (pokemon.Item2.BaseStats[Stat.Attack]) / (1 + Math.Pow(Math.E, -0.05 * (i - 50))))));
        //SeriesCollection[4].Values.Add(new ObservablePoint(i, (_multiplier * (pokemon.Item3.BaseStats[Stat.Attack]) / (1 + Math.Pow(Math.E, -0.05 * (i - 50))))));
      }

      YAxis.MaxValue = ((ObservablePoint)SeriesCollection[4].Values[10]).Y *1.1;

    }

    private void ChartMouseMove(object sender, MouseEventArgs e)
    {
      var point = Chart.ConvertToChartValues(e.GetPosition(Chart));
      var test = SeriesCollection[3].ActualValues;
      MouseX.FromValue = point.X;
      MouseX.ToValue = point.X;
      MouseY.FromValue = point.Y;
      MouseY.ToValue = point.Y;
    }

    public SeriesCollection SeriesCollection { get; set; }
    public string[] Labels { get; set; }
    public Func<double, string> YFormatter { get; set; }
    public Func<double, string> XFormatter { get; set; }

    public float Zoom { get; set; }

  }
}
