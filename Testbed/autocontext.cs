﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testbed
{
  public class autocontext
  {
    public Stat CurrentStat { get; set; }
    public List<SpeciesDefinition> Examples { get; set; }
    public Dictionary<Stat, SortedDictionary<int, SpeciesDefinition>> Thresholds { get; set; }
    public autocontext()
    {
      #region pokemondef
      SpeciesDefinition arceus = new SpeciesDefinition()
      {
        Name = "Arceus",
        BaseStats = new StatPage(false)
        {
          { Stat.HP, 999 },
          { Stat.Endurance, 999 },
          { Stat.Attack, 999 },
          { Stat.Defense, 999 },
          { Stat.SpecialAttack, 999 },
          { Stat.SpecialDefense, 999 },
          { Stat.Resistance, 999 },
          { Stat.Weakness, 999 },
          { Stat.Speed, 999 },
          { Stat.Initiative, 999 },
          { Stat.Accuracy, 999 },
          { Stat.Evasion, 999 },
          { Stat.Critical, 999 },
          { Stat.Height, 999 },
          { Stat.Weight, 999 },
          { Stat.SoulLink, 999 },
          { Stat.Respect, 999 },
          { Stat.Experience, 999 },
          { Stat.Evolution, 999 },
          { Stat.Gender, 999 },
          { Stat.Temperament, 999 },
          { Stat.CatchRate, 999 }
        }
      };

      SpeciesDefinition rattata = new SpeciesDefinition()
      {
        Name = "Rattata",
        BaseStats = new StatPage(false)
        {
          { Stat.HP, 50 },
          { Stat.Endurance, 50 },
          { Stat.Attack, 50 },
          { Stat.Defense, 50 },
          { Stat.SpecialAttack, 50 },
          { Stat.SpecialDefense, 50 },
          { Stat.Resistance, 50 },
          { Stat.Weakness, 50 },
          { Stat.Speed, 50 },
          { Stat.Initiative, 50 },
          { Stat.Accuracy, 50 },
          { Stat.Evasion, 50 },
          { Stat.Critical, 50 },
          { Stat.Height, 50 },
          { Stat.Weight, 50 },
          { Stat.SoulLink, 50 },
          { Stat.Respect, 50 },
          { Stat.Experience, 50 },
          { Stat.Evolution, 50 },
          { Stat.Gender, 50 },
          { Stat.Temperament, 50 },
          { Stat.CatchRate, 50 }
        }
      };

      SpeciesDefinition magikarp = new SpeciesDefinition()
      {
        Name = "Magikarp",
        BaseStats = new StatPage(false)
        {
          { Stat.HP, 10 },
          { Stat.Endurance, 10 },
          { Stat.Attack, 10 },
          { Stat.Defense, 10 },
          { Stat.SpecialAttack, 10 },
          { Stat.SpecialDefense, 10 },
          { Stat.Resistance, 10 },
          { Stat.Weakness, 10 },
          { Stat.Speed, 10 },
          { Stat.Initiative, 10 },
          { Stat.Accuracy, 10 },
          { Stat.Evasion, 10 },
          { Stat.Critical, 10 },
          { Stat.Height, 10 },
          { Stat.Weight, 10 },
          { Stat.SoulLink, 10 },
          { Stat.Respect, 10 },
          { Stat.Experience, 10 },
          { Stat.Evolution, 10 },
          { Stat.Gender, 10 },
          { Stat.Temperament, 10 },
          { Stat.CatchRate, 10 }
        }
      };

      SpeciesDefinition alakazam = new SpeciesDefinition()
      {
        Name = "Alakazam",
        BaseStats = new StatPage(false)
        {
          { Stat.HP, 200 },
          { Stat.Endurance, 200 },
          { Stat.Attack, 200 },
          { Stat.Defense, 200 },
          { Stat.SpecialAttack, 200 },
          { Stat.SpecialDefense, 200 },
          { Stat.Resistance, 200 },
          { Stat.Weakness, 200 },
          { Stat.Speed, 200 },
          { Stat.Initiative, 200 },
          { Stat.Accuracy, 200 },
          { Stat.Evasion, 200 },
          { Stat.Critical, 200 },
          { Stat.Height, 200 },
          { Stat.Weight, 200 },
          { Stat.SoulLink, 200 },
          { Stat.Respect, 200 },
          { Stat.Experience, 200 },
          { Stat.Evolution, 200 },
          { Stat.Gender, 200 },
          { Stat.Temperament, 200 },
          { Stat.CatchRate, 200 }
        }
      };

      SpeciesDefinition mewtwo = new SpeciesDefinition()
      {
        Name = "Mewtwo",
        BaseStats = new StatPage(false)
        {
          { Stat.HP, 500 },
          { Stat.Endurance, 500 },
          { Stat.Attack, 500 },
          { Stat.Defense, 500 },
          { Stat.SpecialAttack, 500 },
          { Stat.SpecialDefense, 500 },
          { Stat.Resistance, 500 },
          { Stat.Weakness, 500 },
          { Stat.Speed, 500 },
          { Stat.Initiative, 500 },
          { Stat.Accuracy, 500 },
          { Stat.Evasion, 500 },
          { Stat.Critical, 500 },
          { Stat.Height, 500 },
          { Stat.Weight, 500 },
          { Stat.SoulLink, 500 },
          { Stat.Respect, 500 },
          { Stat.Experience, 500 },
          { Stat.Evolution, 500 },
          { Stat.Gender, 500 },
          { Stat.Temperament, 500 },
          { Stat.CatchRate, 500 }
        }
      };
      #endregion

      Examples = new List<SpeciesDefinition>();

      Examples.Add(arceus);
      Examples.Add(rattata);
      Examples.Add(magikarp);
      Examples.Add(alakazam);
      Examples.Add(mewtwo);

      Thresholds = new Dictionary<Stat, SortedDictionary<int, SpeciesDefinition>>();
      RecalculateThresholds();
    }

    public void RecalculateThresholds()
    {
      Thresholds.Clear();
      foreach(Stat stat in new Stat().GetValues())
      {
        Thresholds[stat] = new SortedDictionary<int, SpeciesDefinition>();
        foreach(var pokemon in Examples)
        {
          Thresholds[stat][pokemon.BaseStats[stat]] = pokemon;
        }
      }
    }

    public Tuple<SpeciesDefinition, SpeciesDefinition, SpeciesDefinition> GetClosest(Stat stat, int amount)
    {
      var pokemon = Thresholds[stat];
      int last = 0;
      
      for(int i = 0; i < pokemon.Count; i++)
      {
        last = i;
        if(pokemon.ElementAt(i).Value.BaseStats[stat] < amount /3)
        {
          continue;
        }
        else
        {
          break;
        }
      }

      if (last == pokemon.Count - 1 || last == pokemon.Count - 2)
      {
        return new Tuple<SpeciesDefinition, SpeciesDefinition, SpeciesDefinition>(pokemon.ElementAt(last).Value, pokemon.ElementAt(last - 1).Value, pokemon.ElementAt(last - 2).Value);
      }
      else
      {
        return new Tuple<SpeciesDefinition, SpeciesDefinition, SpeciesDefinition>(pokemon.ElementAt(last).Value, pokemon.ElementAt(last + 1).Value, pokemon.ElementAt(last + 2).Value);
      }
    }

    
  }

  




  public class SpeciesDefinition
  {
    public int Version { get; set; }
    public string Name { get; set; }
    public string Notes { get; set; }
    public float GenderMaleSpread { get; set; }

    public StatPage BaseStats { get; set; }
    public StatModifier StatVariance { get; set; } //IVs
    //public Dictionary<Gender, StatModifier> Dimorphism { get; set; }


    public string EvolvesFrom { get; set; }


    public SpeciesDefinition()
    {
      Version = 1;
      Name = "";

      BaseStats = new StatPage();
      StatVariance = new StatModifier();
    }
  }
}
