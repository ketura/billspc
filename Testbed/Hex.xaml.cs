﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Testbed
{

  public partial class Hex : UserControl
  {

    

    public double Size
    {
      get { return (double)GetValue(SizeProperty); }
      set { SetValue(SizeProperty, value); }
    }
    public static readonly DependencyProperty SizeProperty = DependencyProperty.RegisterAttached("Size",
           typeof(double), typeof(Hex), new PropertyMetadata(100.0, OnSizePropertyChanged));

    private static void OnSizePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      Hex h = d as Hex;
      double? b = e.NewValue as double?;
      h.SizePropChanged((double)b);
    }
    protected void SizePropChanged(double size)
    {
      HexPath.Width = size;
      HexPath.Height = size;
      Width = size;
      Height = size;
    }

    public Brush Color
    {
      get { return (Brush)GetValue(ColorProperty); }
      set { SetValue(ColorProperty, value); }
    }
    public static readonly DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(Brush),
          typeof(Hex), new UIPropertyMetadata(Brushes.Red, OnColorPropertyChanged));

    private static void OnColorPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      Hex h = d as Hex;
      Brush b = e.NewValue as Brush;
      h.ColorChanged(b);
    }
    protected void ColorChanged(Brush color)
    {
      HexPath.Fill = color;
    }


    public Hex()
    {
      InitializeComponent();
    }
  }

 

  }
