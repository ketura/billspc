﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using SOTC_BindingErrorTracer;
using LiveCharts.Defaults;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace Testbed
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>

  [TypeConverter(typeof(EnumDescription))]
  public enum TestEnum
  {
    [Description("Some default value")]
    Value1,

    [Description("Something else")]
    Value2,

    [Description("Some other third thing")]
    Value3

  }

  public partial class MainWindow : Window
  {
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
      (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public Func<double, string> YFormatter { get; set; }
    public Func<double, string> XFormatter { get; set; }

    public TestEnum teste { get; set; }

    //private TestController tc;
    private SeriesCollection SeriesCollection { get; set; }

    public MainWindow()
    {
      //BindingErrorTraceListener.SetTrace();
      InitializeComponent();


      YFormatter = y => y.ToString("0");
      XFormatter = x => x.ToString("0");

      SortedDictionary<int, int> Values = new SortedDictionary<int, int>();

      for(int i = 0; i < 1000; i++)
      {
        int num = (int)Math.Round(GetValue());
        if (Values.ContainsKey(num))
          Values[num]++;
        else
          Values[num] = 1;

      }

      Log.Debug(Values.Count);

      ChartValues<ObservablePoint> curve = new ChartValues<ObservablePoint>();
      foreach(var pair in Values)
      {
        curve.Add(new ObservablePoint(pair.Key, pair.Value));
      }

      SeriesCollection = new SeriesCollection()
      {
        new LineSeries
        {
            Title = "Max",
            PointGeometrySize = 4,
            Values = curve,
            FontFamily = new FontFamily("Segoe UI"),
            FontWeight = FontWeights.Bold,
            Foreground = System.Windows.Media.Brushes.Black,
            DataLabels = false,
        }
      };

      Chart.Series = SeriesCollection;
      Chart.AxisY[0].MinValue = 0;

    }

    Random rand = new Random(); //reuse this if you are generating many

    //Box-Muller
    private double GetValue()
    {
       
      double u1 = rand.NextDouble(); //these are uniform(0,1) random doubles
      double u2 = rand.NextDouble();
      double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
      double randNormal = 120 + ((40/4) * 1) * randStdNormal; //random normal(mean,stdDev^2)

      return randNormal;
    }

  }
}
