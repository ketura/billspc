﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace Testbed
{
  public static class EnumExtensions
  {
    static public bool HasProperty(this Type type, string name)
    {
      return type
          .GetProperties(BindingFlags.Public | BindingFlags.Instance)
          .Any(p => p.Name == name);
    }

    public static string GetDescription<T>(this T value)
    {
      FieldInfo fi = value.GetType().GetField(value.ToString());

      DescriptionAttribute[] attributes = null;

      if (fi != null)
      {
        attributes =
          (DescriptionAttribute[])fi.GetCustomAttributes(
          typeof(DescriptionAttribute),
          false);
      }

      if (attributes != null && attributes.Length > 0)
        return attributes[0].Description;
      else
        return value.ToString();
    }

    public static IEnumerable<T> GetValues<T>(this T value)
    {
      return Enum.GetValues(typeof(T)).Cast<T>();
    }

    public static IEnumerable<T> GetValues<T>(this Enum value)
    {
      return Enum.GetValues(typeof(T)).Cast<T>();
    }

    public static T Parse<T>(this T me, string str)
    {
      if (string.IsNullOrEmpty(str))
        return me.GetValues().First();

      return (T)Enum.Parse(typeof(T), str);
    }
  }
}
