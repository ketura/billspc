﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Testbed
{
  class TestController : INotifyPropertyChanged
  {
    private float _field;
    public float Field1
    {
      get { return _field; }
      set
      {
        if (_field != value)
        {
          _field = value;
          OnPropertyChanged("Field1");
          log();
        }

      }
    }

    public TestController()
    {
      Field1 = 1.2136f;
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public void OnPropertyChanged(string propName)
    {
      if(PropertyChanged != null)
      {
        PropertyChanged(this, new PropertyChangedEventArgs(propName));
      }
    }

    public void log()
    {
      Console.WriteLine(Field1);
    }

  }
}
