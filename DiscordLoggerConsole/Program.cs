﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Discord;

namespace DiscordLoggerConsole
{
	class Program
	{

		static void Main(string[] args) => new Program().Start();

		private DiscordClient _client;

		public void Start()
		{
			Console.WriteLine("started");
			_client = new DiscordClient();

			_client.MessageReceived += async (s, e) =>
						{
							if (e.Message.Text == "test")
								await e.Channel.SendMessage("Message Received");
						};

			_client.Ready += async (s, e) =>
						{
							Channel pokeChannel = _client.GetChannel(230041937984487424);
							Console.WriteLine(pokeChannel.Name);

							await Task.Delay(1000);

							RetrieveMessagesToDate(pokeChannel, DateTime.Parse("12/12/2016"));

							//var response = pokeChannel.DownloadMessages();

							//var list = response.Result;

							//foreach(var m in list)
							//{
							//	string name = m.User == null ? "null" : m.User.Name;
							//	Console.WriteLine( name + "[" + m.Timestamp + "]:\n" + m.Text );
							//}

							//await pokeChannel.SendMessage("testing");

							Console.WriteLine("Complete");

							await _client.Disconnect();
						};

			_client.ExecuteAndWait(async () => {
				await _client.Connect("hyperlink.teltura@gmail.com", "PASSWORDHERE", "MjI0MjUzMDM1Nzg4ODk0MjE4.CvVJqg.4i0jEwEiwy6fk5J7xOca7CKsolQ");


		});
		}

		public void RetrieveMessagesToDate(Channel channel, DateTime date)
		{
			ulong currentPlace = 0;
			//get the most recent message and use this as our anchor.
			Task<Message[]> response = null;
			Dictionary<DateTime, List<Message>> Messages = new Dictionary<DateTime, List<Message>>();

			bool foundEnd = false;

			while (!foundEnd)
			{
				
				if(currentPlace == 0)
				{
					response = channel.DownloadMessages(1);
				}
				else
				{
					response = channel.DownloadMessages(100, currentPlace);
				}

				if(response.Result.Length == 0 )
				{
					Console.WriteLine("No messages returned.  Terminating.");
					break; 
				}

				foreach(var m in response.Result)
				{
					if(m.Timestamp.Date < date)
					{
						Console.WriteLine(string.Format("Found a message with timestamp {0}.  Terminating.", m.Timestamp));
						foundEnd = true;
						break;
					}
					AddMessage(Messages, m);
				}

				currentPlace = response.Result.Last().Id;
			}

			WriteMessages(channel.Name, Messages);
		}

		public void AddMessage(Dictionary<DateTime, List<Message>> Messages, Message message)
		{
			DateTime date = message.Timestamp.Date;
			if (!Messages.ContainsKey(date))
				Messages[date] = new List<Message>();

			Messages[date].Add(message);
		}

		public void WriteMessages(string channelName, Dictionary<DateTime, List<Message>> messages)
		{
			foreach(var pair in messages)
			{
				string currentUser = null;
				string currentTime = "";
				string output = "";

				pair.Value.Reverse();
				foreach (Message m in pair.Value)
				{
					if(m.User.Name != currentUser || m.Timestamp.ToShortTimeString() != currentTime)
					{
						currentUser = m.User.Name;
						currentTime = m.Timestamp.ToShortTimeString();
						output += string.Format("\n[{0}] - {1}\n", currentUser, currentTime);
					}

					output += m.RawText + "\n";
				}
				string filename = channelName + "_" + pair.Key.ToShortDateString().Replace('/', '-') + ".txt";
				File.WriteAllText(filename, output);
			}
		}

//		static void Main(string[] args)
//		{
//			DiscordClient _client = new DiscordClient();
//			//string url = txtURL.Text;
//			//WebRequest wrGETURL = WebRequest.Create(url);
//			//Stream objStream = wrGETURL.GetResponse().GetResponseStream();

//			//StreamReader objReader = new StreamReader(objStream);

//			//string output = "";
//			//string line = "";

//			//while (line != null)
//			//{
//			//	line = objReader.ReadLine();
//			//	if (line != null)
//			//		output += line;
//			//}

//			//txtResult.Text = output;


//			//_client.Connect("hyperlink.teltura@gmail.com", "PASSWORDHERE");


//			_client.MessageReceived += async (s, e) =>
//			{
//				if (!e.Message.Text.StartsWith("|||"))
//					await e.Channel.SendMessage("|||" + e.Message.Text);
//			};

//			_client.Ready += async (s, e) =>
//			{
//				Console.WriteLine( _client.GetChannel(230041937984487424).Name);
//				await _client.Disconnect();
//			};
//			//_client.Connect("hyperlink.teltura@gmail.com", "PASSWORDHERE", "MjI0MjUzMDM1Nzg4ODk0MjE4.CvVJqg.4i0jEwEiwy6fk5J7xOca7CKsolQ");
//			//ThreadExceptionDialog.


//			_client.ExecuteAndWait(async () =>
//			{
//				while (true)
//				{
//					try
//					{
//						await _client.Connect("hyperlink.teltura@gmail.com", "PASSWORDHERE", "MjI0MjUzMDM1Nzg4ODk0MjE4.CvVJqg.4i0jEwEiwy6fk5J7xOca7CKsolQ");
//						Console.WriteLine("complete");
//						break;
//					}
//					catch
//					{
//						await Task.Delay(3000);
//					}
//				}
//				//	//await _client.Connect("MjI0MjUzMDM1Nzg4ODk0MjE4.CvVJqg.4i0jEwEiwy6fk5J7xOca7CKsolQ", TokenType.User);


//			});
//		}
	}
}
